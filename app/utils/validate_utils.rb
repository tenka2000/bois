# encoding: utf-8
# Validationルールの定義
class ValidateUtils
	def initialize
		# エラー結果
		@result = true
		# メッセージリスト
		@messages = {}
		# 初回エラーキー（focus先）
		@firstErrorKey = nil
	end

	# メッセージの取得
	def getMessages(key)
		if !@result

			if !@messages.key?(key)
			return []
			end
			if @messages[key] == nil
			return []
			end
		end
		return @messages[key]
	end

	# 全メッセージの取得
	def getAllMessages()
		return @messages
	end

	# エラーのセット
	def setError(key, message, focus)
		@result = false
		if !@messages.key?(key)
		@messages[key]=[]
		end

		if @firstErrorKey == nil
			@firstErrorKey = focus
		end
		@messages[key] << message
	end

	def getFocus
		return @firstErrorKey
	end

	# エラーの有無を精査
	def hasError

		if @result
		return false
		else
		return true
		end

	end

	# 必須チェック
	# val::チェック対象の値
	# key::チェック対象のキー
	# msg::メッセージ
	def isNotEmpty(val, key=nil, msg=nil, focus=nil)
		ret = true
		if val == nil || val == ''

			if key != nil && msg != nil
				setError(key, msg, focus)
			end

		ret = false
		end
		return ret
	end

	# 文字列長チェック
	# val::チェック対象の値
	# min::最少長
	# max::最大長
	# key::チェック対象のキー
	# msg::メッセージ
	def isLength(val, min, max, key=nil, msg=nil, focus=nil)

		ret=true
		if isNotEmpty(val)
			if val.length < min || val.length > max
				ret = false
				if key != nil && msg != nil
					setError(key, msg, focus)
				end
			end
		end
		return ret
	end

	# 文字列最少長チェック
	# val::チェック対象の値
	# min::最少長
	# key::チェック対象のキー
	# msg::メッセージ
	def isMinLength(val, min, key=nil, msg=nil, focus=nil)

		ret=true
		if isNotEmpty(val)
			if val.length < min
				ret = false
				if key != nil && msg != nil
					setError(key, msg, focus)
				end
			end
		end
		return ret
	end

	# 文字列最大長チェック
	# val::チェック対象の値
	# min::最少長
	# key::チェック対象のキー
	# msg::メッセージ
	def isMaxLength(val, max, key=nil, msg=nil, focus=nil)

		ret=true
		if isNotEmpty(val)
			if val.length > max
				ret = false
				if key != nil && msg != nil
					setError(key, msg, focus)
				end
			end
		end
		return ret
	end

	# 整数チェック
	# val::チェック対象の値
	# decimal::少数の可否
	# minus::マイナスの可否
	# key::チェック対象のキー
	# msg::メッセージ
	def isNumber(val, decimal=false, minus=false, key=nil, msg=nil, focus=nil)
		ret = true

		if isNotEmpty(val)
			# 少数なし・マイナスなし
			if !decimal && !minus

				if val =~ /^[0-9]+$/
				ret = true
				else
					if key != nil && msg != nil
						setError(key, msg, focus)
					end
				ret = false
				end

			# 少数あり・マイナスなし
			elsif decimal && !minus

				if val =~ /^[0-9]+\.[0-9]+$/ || val =~ /^[0-9]+$/
				ret = true
				else
					if key != nil && msg != nil
						setError(key, msg, focus)
					end
				ret = false
				end

			# 少数なし・マイナスあり
			elsif !decimal && minus

				if val =~ /^-[0-9]+$/ || val =~ /^[0-9]+$/
				ret = true
				else
					if key != nil && msg != nil
						setError(key, msg, focus)
					end
				ret = false
				end

			# 少数あり・マイナスあり
			else

				if val =~ /^-[0-9]+\.[0-9]+$/ || val =~ /^[0-9]+\.[0-9]+$/ || val =~ /^-[0-9]+$/ || val =~ /^[0-9]+$/
				ret = true
				else
					if key != nil && msg != nil
						setError(key, msg, focus)
					end
				ret = false
				end

			end
		end

		return ret
	end

	# 数値の範囲
	# val::チェック対象の値
	# min::最少の値
	# max::最大の値
	# key::チェック対象のキー
	# msg::メッセージ
	def isRange(val, min, max, key=nil, msg=nil, focus=nil)

		ret = true
		if isNotEmpty(val)
			if isNumber(val, true, true)

				f = val.to_f

				if f < min
					ret = false
					if key != nil && msg != nil
						setError(key, msg, focus)
					end
				end

				if f > max

					ret = false
					if key != nil && msg != nil
						setError(key, msg, focus)
					end
				end

			else
				ret = false
				if key != nil && msg != nil
					setError(key, msg, focus)
				end

			end

		end

		return ret
	end

	# 数値の最少チェック
	# val::チェック対象の値
	# min::最少の値
	# key::チェック対象のキー
	# msg::メッセージ
	def isMinRange(val, min, key=nil, msg=nil, focus=nil)

		ret = true
		if isNotEmpty(val)
			if isNumber(val, true, true)

				f = val.to_f

				if f < min
					ret = false
					if key != nil && msg != nil
						setError(key, msg, focus)
					end
				end

			else
				ret = false
				if key != nil && msg != nil
					setError(key, msg, focus)
				end
			end

		end
		return ret
	end

	# 数値の範囲
	# val::チェック対象の値
	# max::最大の値
	# key::チェック対象のキー
	# msg::メッセージ
	def isMaxRange(val, max, key=nil, msg=nil, focus=nil)

		ret = true
		if isNotEmpty(val)
			if isNumber(val, true, true)

				f = val.to_f

				if f > max

					ret = false
					if key != nil && msg != nil
						setError(key, msg, focus)
					end
				end

			else
				ret = false
				if key != nil && msg != nil
					setError(key, msg, focus)
				end
			end

		end

		return ret
	end

	# 半角英字チェック
	def isAlpha(val, key=nil, msg=nil, focus=nil)

		ret = true
		if isNotEmpty(val)
			if val =~ /^[A-Za-z]+$/
			ret = true
			else
				if key != nil && msg != nil
					setError(key, msg, focus)
				end
			ret = false
			end
		end
		return ret
	end

	# 半角英数字チェック
	def isAlphaNum(val, key=nil, msg=nil, focus=nil)

		ret = true
		if isNotEmpty(val)
			if val =~ /^[0-9A-Za-z]+$/
			ret = true
			else
				if key != nil && msg != nil
					setError(key, msg, focus)
				end
			ret = false
			end
		end
		return ret
	end

	# 半角英数字チェック
	# 記号は@+-.[]*_<>:!|を使用可能
	def isAlphaNumMark(val, key=nil, msg=nil, focus=nil)

		ret = true
		if isNotEmpty(val)
			if val =~ /^[0-9A-Za-z@\+*\[\]\.\-\?<>:!|]+$/
				ret = true
			else
				if key != nil && msg != nil
					setError(key, msg, focus)
				end
				ret = false
			end
		end
		return ret
	end

	# E-MAILアドレスチェック
	def isEmail(val, key=nil, msg=nil, focus=nil)
		ret = true
		if isNotEmpty(val)
			if val =~ /^[a-zA-Z0-9_\#!$%&`'*+\-{|}~^\/=?\.]+@[a-zA-Z0-9_\#!$%&`'*+\-{|}~^\/=?\.]+$/
				ret = true
			else
				if key != nil && msg != nil
					setError(key, msg, focus)
				end
				ret = false
			end
		end
		return ret
	end

	# 電話番号チェック
	def isTel(val, key=nil, msg=nil, focus=nil)
		ret = true
		if isNotEmpty(val)
			if val =~ /^[0-9\-\(\)\+]+$/
				ret = true
			else
				if key != nil && msg != nil
					setError(key, msg, focus)
				end
				ret = false
			end
		end
		return ret
	end
	
	# 時間チェック
	def isTime(val, key=nil, msg=nil, focus=nil)
		ret = true
		if isNotEmpty(val)
			hour = nil
			min = nil

			# 形式チェック
			fromFormats = Items::FORMAT_CONV_FROM_TIME
			for fromFormat in fromFormats
				re = Regexp.new(fromFormat)
				if re =~ val
					l = val.scan(re)
					hour = l[0][0].to_i
					min = l[0][1].to_i
					break
				end
			end

			if hour == nil || min == nil
				ret = false
			end

			if ret
				
				if hour < 0 || hour > 23
					ret = false
				elsif min < 0 || min > 59
					ret = false
				end	
			end
		end

		if !ret && key != nil && msg != nil
				setError(key, msg, focus)
		end

		return ret

	end


	# 日付チェック
	def isDate(val, key=nil, msg=nil, focus=nil)
		ret = true
		if isNotEmpty(val)
			year = nil
			month = nil
			day = nil

			# 形式チェック
			fromFormats = Items::FORMAT_CONV_FROM_DATE
			for fromFormat in fromFormats
				re = Regexp.new(fromFormat)
				if re =~ val
					l = val.scan(re)
					year = l[0][0].to_i
					month = l[0][1].to_i
					day = l[0][2].to_i
					break
				end
			end

			if year == nil || month == nil || day == nil
				ret = false
			end

			if ret
				if !Date::valid_date?(year, month, day)
					ret = false
				end
			end
		end

		if !ret && key != nil && msg != nil
				setError(key, msg, focus)
		end

		return ret

	end


	# 全角カタカナ
	def isZenKana(val, key=nil, msg=nil, focus=nil)
		ret = true
		if isNotEmpty(val)
			if val =~ /^[\p{katakana}ー－]+$/
				ret = true
			else
				if key != nil && msg != nil
					setError(key, msg, focus)
				end
				ret = false
			end
		end
		return ret
	end
	
end

