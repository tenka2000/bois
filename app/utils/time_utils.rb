# encoding: utf-8
# 日付・時間関連のユーティリティ
class TimeUtils
	
	# 年のリストを返す
	# ex)
	# <%= select_tag(:year, options_for_select(TimeUtils.getYearList, :selected => TimeUtils.getCurrentYear)) %>
	def TimeUtils.getYearList(st=nil, ed=nil)
		
		list=[];
		if st == nil
			st = 1990;
		end
		
		if ed == nil
			day = Date.today
			ed = day.year
		end
		
		for i in st..ed
			list.unshift([i.to_s, :value=>i.to_s])
		end
		return list
	end

	def TimeUtils.getCurrentYear()
		day = Date.today
		return day.year
	end

	def TimeUtils.getMonthList()
		list = []
		for i in 1..12
			list << [i.to_s, :value=>i.to_s]
		end
		return list
		
	end

	def TimeUtils.getCurrentMonth()
		day = Date.today
		return day.month
	end


	def TimeUtils.getDayList(year=nil, month=nil)
			
		list = []
				
		if year==nil || month==nil
			day = Date.today
			year = day.year
			month = day.month
		end

		d = Date.new(year, month, 1)

		while d.month == month
			list << [d.day.to_s, :value=>d.day.to_s]
			d = d + 1
		end
		
		return list
		
	end

	def TimeUtils.getFullDayList()
			
		list = []
				
		for i in 1..31
			list << [i.day.to_s, :value=>i.day.to_s]
		end
		
		return list
		
	end


	# 2013年04月11日（火）の形式で日付リストを返す
	def TimeUtils.getJPYMDWList(year=nil, month=nil, paddingZero=false)
			
		list = []
				
		if year==nil || month==nil
			day = Date.today
			year = day.year
			month = day.month
		end

		d = Date.new(year, month, 1)

		while d.month == month
			
			value = ''
			if paddingZero
				value = format(Items::FORMAT_DAY_FORMAT_01, year, month, d.day, TimeUtils.getJPWdays(d.wday))
			else
				value = format(Items::FORMAT_DAY_FORMAT_02, year, month, d.day, TimeUtils.getJPWdays(d.wday))
			end 
			key = format(Items::FORMAT_DAY_KEY_FORMAT, year, month, d.day);
			
			list << [key, :value=>value]
			d = d + 1
		end

		return list
	end
	
	# 04月11日（火）の形式で日付リストを返す
	def TimeUtils.getJPMDWList(year=nil, month=nil, paddingZero=false)
			
		list = []
				
		if year==nil || month==nil
			day = Date.today
			year = day.year
			month = day.month
		end

		d = Date.new(year, month, 1)

		while d.month == month
			
			value = ''
			if paddingZero
				value = format(Items::FORMAT_DAY_FORMAT_11, month, d.day, TimeUtils.getJPWdays(d.wday))
			else
				value = format(Items::FORMAT_DAY_FORMAT_12, month, d.day, TimeUtils.getJPWdays(d.wday))
			end 
			key = format(Items::FORMAT_DAY_KEY_FORMAT, year, month, d.day);

			list << [key, :value=>value]
			d = d + 1
		end

		return list
	end
	
	
	def TimeUtils.getJPWdays(wday)
		wdays = [Items::LABEL_WEEK_SAN, Items::LABEL_WEEK_MON, Items::LABEL_WEEK_TUE, Items::LABEL_WEEK_WED, Items::LABEL_WEEK_THU, Items::LABEL_WEEK_FRI, Items::LABEL_WEEK_SAT]
		return wdays[wday]
	end

	# 2013年04月11日（火）の形式で日付を返す
	def TimeUtils.getJPYMDW(date, paddingZero=false)
		value = ""
		if paddingZero
			value = format(Items::FORMAT_DAY_FORMAT_01, date.year, date.month, date.day, TimeUtils.getJPWdays(date.wday))
		else
			value = format(Items::FORMAT_DAY_FORMAT_02, date.year, date.month, date.day, TimeUtils.getJPWdays(date.wday))
		end 
		return value
	end

	# 04月11日（火）の形式で日付を返す
	def TimeUtils.getJPMDW(date, paddingZero=false)
		value = ""
		if paddingZero
			value = format(Items::FORMAT_DAY_FORMAT_11, date.month, date.day, TimeUtils.getJPWdays(date.wday))
		else
			value = format(Items::FORMAT_DAY_FORMAT_12, date.month, date.day, TimeUtils.getJPWdays(date.wday))
		end 
		return value
	end


	def TimeUtils.getCurrentDay()
		day = Date.today
		return day.day
	end

	def TimeUtils.getHourList(year=nil, month=nil)
		
		
		
	end

	def TimeUtils.getMinList(year=nil, month=nil)
		
		
		
	end

	def TimeUtils.getSecList(year=nil, month=nil)
		
		
		
	end
	
	# yyyyMMdd形式の日付をパースする
	# delim=trueの場合、年月日の間に1文字入っているとする
	def TimeUtils.parseIntDate(date, delim=false)
		ret={}
		
		if !delim && date.length == 8
				ret[:year] = date.slice(0, 4).to_i
				ret[:month] = date.slice(4, 2).to_i
				ret[:day] = date.slice(6, 2).to_i
		elsif delim && date.length <= 10.to_i
				ret[:year] = date.slice(0, 4).to_i
				ret[:month] = date.slice(5, 2).to_i
				ret[:day] = date.slice(8, 2).to_i
		else
			raise "Date Format Error (" + date + ")"
		end

		return Date.new(ret[:year] , ret[:month], ret[:day])

	end


	# 様々な形式の時間をフォーマットする
	def TimeUtils.formatTime(str, toFormat=Items::FORMAT_CONV_TO_TIME_02)


		if str.class == String
			fromFormats = Items::FORMAT_CONV_FROM_TIME
	
			ret = str
			for fromFormat in fromFormats
				re = Regexp.new(fromFormat)
				if re =~ str
					
					l = str.scan(re)
					
					ret = format(toFormat, l[0][0], l[0][1])
					break
				end
			end
			
		elsif str.class == Time
			ret = format(toFormat, str.hour, str.min)
		end
		
		return ret
	end

	# 様々な形式の時間をフォーマットする
	def TimeUtils.formatDate(str, toFormat=Items::FORMAT_CONV_TO_DATE_02)
		
		if str.class == String
			fromFormats = Items::FORMAT_CONV_FROM_DATE
			ret = str
			for fromFormat in fromFormats
				re = Regexp.new(fromFormat)
				if re =~ str
					
					l = str.scan(re)
					ret = format(toFormat, l[0][0].to_i, l[0][1].to_i, l[0][2].to_i)
					break
				end
			end
		elsif str.class == Time || str.class == Date
			ret = format(toFormat, str.year, str.month, str.day)
		end
		
		return ret
	end
	
	# 様々な形式の時間をパースする
	def TimeUtils.parseTime(str)
		
		fromFormats = Items::FORMAT_CONV_FROM_TIME
		ret = nil
		for fromFormat in fromFormats
			re = Regexp.new(fromFormat)
			if re =~ str
				l = str.scan(re)
				ret = {:hour=>l[0][0].to_i, :min=>l[0][1].to_i}
				break
			end
		end
		return ret
	end
	# 様々な形式の日付をフォーマットする
	def TimeUtils.parseDate(str)
		
		fromFormats = Items::FORMAT_CONV_FROM_DATE
		ret = nil
		for fromFormat in fromFormats
			re = Regexp.new(fromFormat)
			if re =~ str
				l = str.scan(re)
				ret = {:year=>l[0][0].to_i, :month=>l[0][1].to_i, :day=>l[0][2].to_i}
				break
			end
		end
		return ret
	end

	# 様々な形式の年月をフォーマットする
	def TimeUtils.parseYM(str)
		
		fromFormats = Items::FORMAT_CONV_FROM_YM
		ret = nil
		for fromFormat in fromFormats
			re = Regexp.new(fromFormat)
			if re =~ str
				l = str.scan(re)
				ret = {:year=>l[0][0].to_i, :month=>l[0][1].to_i}
				break
			end
		end
		return ret
	end
	
	# 現在の期の開始日時を取得する
	def self.getAccountStartDate(baseMonth)
		
		today = Date.today
		year = today.year
		
		if today.month < baseMonth
			year -=1
		end

		return Date.new(year, baseMonth, 1)
	end
	
	# 現在の期の終了日時を取得する
	def self.getAccountEndDate(baseMonth)
		
		today = Date.today
		year = today.year
		
		if today.month >= baseMonth - 1
			year +=1
		end

		return Date.new(year, baseMonth - 1, 1)
	end
end
