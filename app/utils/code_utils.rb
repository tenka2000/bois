# encoding: utf-8
class CodeUtils

	# 顧客状態のコード解決
	def CodeUtils.resolveCustomerStatusCode(code)
		
		if Codes::CUSTOMER_STATUS_CODE_YET.value == code
			return Codes::CUSTOMER_STATUS_CODE_YET
		end
		if Codes::CUSTOMER_STATUS_CODE_CONTACT.value == code
			return Codes::CUSTOMER_STATUS_CODE_CONTACT
		end
		if Codes::CUSTOMER_STATUS_CODE_AGREE.value == code
			return Codes::CUSTOMER_STATUS_CODE_AGREE
		end
		if Codes::CUSTOMER_STATUS_CODE_END.value == code
			return Codes::CUSTOMER_STATUS_CODE_END
		end
		if Codes::CUSTOMER_STATUS_CODE_WARN.value == code
			return Codes::CUSTOMER_STATUS_CODE_WARN
		end

	end

	# 顧客状態一覧の取得
	def CodeUtils.getCustomerSatusCodeList
		
		ret = []
		ret << Codes::CUSTOMER_STATUS_CODE_YET
		ret << Codes::CUSTOMER_STATUS_CODE_CONTACT
		ret << Codes::CUSTOMER_STATUS_CODE_AGREE
		ret << Codes::CUSTOMER_STATUS_CODE_END
		ret << Codes::CUSTOMER_STATUS_CODE_WARN
		return ret
		
	end

	# 案件状態のコード解決
	def CodeUtils.resolveMatterStatusCode(code)
		
		if Codes::MATTER_STATUS_CODE_SALE.value == code
			return Codes::MATTER_STATUS_CODE_SALE
		end
		if Codes::MATTER_STATUS_CODE_PRE_ORDER.value == code
			return Codes::MATTER_STATUS_CODE_PRE_ORDER
		end
		if Codes::MATTER_STATUS_CODE_ORDER.value == code
			return Codes::MATTER_STATUS_CODE_ORDER
		end
		if Codes::MATTER_STATUS_CODE_COMPLETE.value == code
			return Codes::MATTER_STATUS_CODE_COMPLETE
		end
		if Codes::MATTER_STATUS_CODE_REJECT.value == code
			return Codes::MATTER_STATUS_CODE_REJECT
		end

	end


	# 案件状態一覧の取得
	def CodeUtils.getMatterSatusCodeList
		
		ret = []
		ret << Codes::MATTER_STATUS_CODE_SALE
		ret << Codes::MATTER_STATUS_CODE_PRE_ORDER
		ret << Codes::MATTER_STATUS_CODE_ORDER
		ret << Codes::MATTER_STATUS_CODE_COMPLETE
		ret << Codes::MATTER_STATUS_CODE_REJECT
		return ret
		
	end
	
	# 見積もり状態コード解決
	def CodeUtils.resolveEstimateStatusCode(code)
		if Codes::ESTIMATE_STATUS_ESTIMATE.value == code
			return Codes::ESTIMATE_STATUS_ESTIMATE
		end
		if Codes::ESTIMATE_STATUS_PREORDER.value == code
			return Codes::ESTIMATE_STATUS_PREORDER
		end
		if Codes::ESTIMATE_STATUS_ORDER.value == code
			return Codes::ESTIMATE_STATUS_ORDER
		end
		if Codes::ESTIMATE_STATUS_PART_BILL.value == code
			return Codes::ESTIMATE_STATUS_PART_BILL
		end
		if Codes::ESTIMATE_STATUS_BILL.value == code
			return Codes::ESTIMATE_STATUS_BILL
		end
		if Codes::ESTIMATE_STATUS_REJECT.value == code
			return Codes::ESTIMATE_STATUS_REJECT
		end
		
	end

	# 見積もり状態一覧の取得
	def CodeUtils.getEstimateStatusCodeList
		
		ret = []
		ret << Codes::ESTIMATE_STATUS_ESTIMATE
		ret << Codes::ESTIMATE_STATUS_PREORDER
		ret << Codes::ESTIMATE_STATUS_ORDER
		ret << Codes::ESTIMATE_STATUS_PART_BILL
		ret << Codes::ESTIMATE_STATUS_BILL
		ret << Codes::ESTIMATE_STATUS_REJECT
		return ret
		
	end



	# タスク種別のコード解決
	def CodeUtils.resolveTaskTypeCode(code)
		if Codes::TASK_TYPE_CODE_PROGRESS.value == code
			return Codes::TASK_TYPE_CODE_PROGRESS
		end
		if Codes::TASK_TYPE_CODE_STONE.value == code
			return Codes::TASK_TYPE_CODE_STONE
		end
	end

	# 見積もり状態一覧の取得
	def CodeUtils.getTaskTypeCodeList
		
		ret = []
		ret << Codes::TASK_TYPE_CODE_PROGRESS
		ret << Codes::TASK_TYPE_CODE_STONE
		return ret
		
	end


	# 往復区分のコード解決
	def CodeUtils.resolveCarfareRoundCode(code)
		if Codes::CARFARE_ROUND_CODE_SINGLE.value == code
			return Codes::CARFARE_ROUND_CODE_SINGLE
		end
		if Codes::CARFARE_ROUND_CODE_DOUBLE.value == code
			return Codes::CARFARE_ROUND_CODE_DOUBLE
		end
	end

	# 往復区分一覧の取得
	def CodeUtils.getCarfareRoundCodeList
		
		ret = []
		ret << Codes::CARFARE_ROUND_CODE_SINGLE
		ret << Codes::CARFARE_ROUND_CODE_DOUBLE
		return ret
	end

	# 初期権限のコード解決
	def CodeUtils.resolveDefaultRoleCode(code)
		if Codes::ROLE_DEFAULT_TYPE_ADMIN.value == code
			return Codes::ROLE_DEFAULT_TYPE_ADMIN
		end
		if Codes::ROLE_DEFAULT_TYPE_AGREE.value == code
			return Codes::ROLE_DEFAULT_TYPE_AGREE
		end
		if Codes::ROLE_DEFAULT_TYPE_WORK.value == code
			return Codes::ROLE_DEFAULT_TYPE_WORK
		end
		if Codes::ROLE_DEFAULT_TYPE_NORMAL.value == code
			return Codes::ROLE_DEFAULT_TYPE_NORMAL
		end
	end

	# 初期権限一覧の取得
	def CodeUtils.getDefaultRoleCodeList
		
		ret = []
		ret << Codes::ROLE_DEFAULT_TYPE_ADMIN
		ret << Codes::ROLE_DEFAULT_TYPE_AGREE
		ret << Codes::ROLE_DEFAULT_TYPE_WORK
		ret << Codes::ROLE_DEFAULT_TYPE_NORMAL
		return ret
		
	end


	# 機能コードのコード解決
	def CodeUtils.resolveFunctionCode(code)
		if Codes::FUNCTION_CODE_DAILIES.value == code
			return Codes::FUNCTION_CODE_DAILIES
		end
		if Codes::FUNCTION_CODE_COSTS.value == code
			return Codes::FUNCTION_CODE_COSTS
		end
		if Codes::FUNCTION_CODE_RESULTS.value == code
			return Codes::FUNCTION_CODE_RESULTS
		end
		if Codes::FUNCTION_CODE_DASHBOARDS.value == code
			return Codes::FUNCTION_CODE_DASHBOARDS
		end
		if Codes::FUNCTION_CODE_CUSTOMERS.value == code
			return Codes::FUNCTION_CODE_CUSTOMERS
		end
		if Codes::FUNCTION_CODE_MATTERS.value == code
			return Codes::FUNCTION_CODE_MATTERS
		end
		if Codes::FUNCTION_CODE_ESTIMATES.value == code
			return Codes::FUNCTION_CODE_ESTIMATES
		end
		if Codes::FUNCTION_CODE_TASKS.value == code
			return Codes::FUNCTION_CODE_TASKS
		end
		if Codes::FUNCTION_CODE_ASSIGNS.value == code
			reutrn Codes::FUNCTION_CODE_ASSIGNS
		end
		if Codes::FUNCTION_CODE_REPORT_MATTERS.value == code
			return Codes::FUNCTION_CODE_REPORT_MATTERS
		end
		if Codes::FUNCTION_CODE_REPORT_USERS.value == code
			return Codes::FUNCTION_CODE_REPORT_USERS
		end
		if Codes::FUNCTION_CODE_GANTT_CAHRT.value == code
			return Codes::FUNCTION_CODE_GANTT_CAHRT
		end
		if Codes::FUNCTION_CODE_REPORT_ASSIGN.value == code
			return Codes::FUNCTION_CODE_REPORT_ASSIGN
		end
		if Codes::FUNCTION_CODE_CURRENT_MATTERS.value == code
			return Codes::FUNCTION_CODE_CURRENT_MATTERS
		end
		if Codes::UNCTION_CODE_REPORT_ESTIMATES.value == code
			return Codes::UNCTION_CODE_REPORT_ESTIMATES
		end
		if Codes::FUNCTION_CODE_USERS.value == code
			return Codes::FUNCTION_CODE_USERS
		end
		if Codes::FUNCTION_CODE_ROLES.value == code
			return Codes::FUNCTION_CODE_ROLES
		end
	end

	# 機能コード一覧の取得
	def CodeUtils.getFunctionCodeList

		ret = []
		ret << Codes::FUNCTION_CODE_DAILIES
		ret << Codes::FUNCTION_CODE_COSTS
		ret << Codes::FUNCTION_CODE_RESULTS
		ret << Codes::FUNCTION_CODE_DASHBOARDS
		ret << Codes::FUNCTION_CODE_CUSTOMERS
		ret << Codes::FUNCTION_CODE_MATTERS
		ret << Codes::FUNCTION_CODE_ESTIMATES
		ret << Codes::FUNCTION_CODE_ORDERS
		ret << Codes::FUNCTION_CODE_BILLS
		ret << Codes::FUNCTION_CODE_TASKS
		ret << Codes::FUNCTION_CODE_ASSIGNS
		ret << Codes::FUNCTION_CODE_REPORT_MATTERS
		ret << Codes::FUNCTION_CODE_REPORT_USERS
		ret << Codes::FUNCTION_CODE_GANTT_CAHRT
		ret << Codes::FUNCTION_CODE_REPORT_ASSIGN
		ret << Codes::FUNCTION_CODE_CURRENT_MATTERS
		ret << Codes::FUNCTION_CODE_REPORT_ESTIMATES
		ret << Codes::FUNCTION_CODE_REPORT_ORDERS
		ret << Codes::FUNCTION_CODE_USERS
		ret << Codes::FUNCTION_CODE_ROLES
		ret << Codes::FUNCTION_CODE_WORKPATTERNS
		ret << Codes::FUNCTION_CODE_MAILSERVER
		ret << Codes::FUNCTION_CODE_SYSTEMPROP

		return ret
		
	end


end
