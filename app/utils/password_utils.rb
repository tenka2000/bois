# encoding: utf-8
require 'digest/sha2'
class PasswordUtils
	
	# パスワードをハッシュ値に変換する
	def PasswordUtils.toHash(password)
		return Digest::SHA256.hexdigest(password + Items::PASSWORD_SALT);
	end
	
	# ランダムなパスワードを生成する
	def PasswordUtils.createRandom
		
		ret = ''
		
		random = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','1','2','3','4','5','6','7','8','9','0', '@','+','-','.','[',']','*','_','<','>',':','!','|']
		
		# パスワードの桁数を求める(7～10桁)
		len = rand(3) + 7
		
		# 最初の1文字目は、アルファベット
		
		ret += random[rand(51)]
		
		for i in 0..len-2
			ret += random[rand(random.length - 1)]
		end

		return ret

	end

end
	