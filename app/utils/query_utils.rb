# encoding: utf-8
class QueryUtils

	def initialize(query=nil, items=nil)
		if query != nil
			@query = query
		end
		
		if items != nil
			@items = items
		end
	end


	# offsetを求める
	def QueryUtils.getOffset(pageNo, pageTotal)
		return (pageNo - 1) * pageTotal
	end

	# limitを求める
	def QueryUtils.getLimit(limit)
		return limit
	end


	# offsetを求める
	def getOffset(pageNo, pageTotal)
		return QueryUtils.getOffset(pageNo, pageTotal)
	end

	# limitを求める
	def getLimit(limit)
		return limit
	end
	
	
	
	# 条件を生成する
	# connectorは初回の場合は無視する
	def addCondition(connector, query, *args)

		if @query != nil && @query.length != 0
			@query += ' ' + connector + ' '
		else
			@query = ''
		end
		
		@query += ' ' + query + "\n"

		if @items == nil
			@items = []
		end

		for i in 0..args.length-1
			@items << args[i]
		end
		
	end

	def insertBeforeCondition(connector, query, *args)

		if @query != nil && @query.length != 0
			@query = ' ' + connector + ' ' + @query
		else
			@query = ''
		end
		
		@query = ' ' + query + "\n" + @query

		if @items == nil
			@items = []
		end

		for i in 0..args.length-1
			@items.unshift(args[args.length-1 - i])
		end
		
	end

	def getCondition
		
		ret = []
		if @query != nil && @query != ''
			ret << @query
			for item in @items
				ret << item
			end
		end
		return ret
	end


	def clone
		cloneQuery = @query.to_s
		cloneItems = []
		
		
		if @items != nil
			for i in @items
				cloneItems << i
			end
		end
		return QueryUtils.new(cloneQuery, cloneItems)
	end

	# 条件設定の有無
	def isConditions
		if @query == nil || @query == ''
			return false
		end
		return true
	end
end
