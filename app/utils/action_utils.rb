# encoding: utf-8
class ActionUtils

	#=== アクションのフィルター（非推奨）
	#
	# リクエストメソッドと指定されたパラメータから、処理の振り分けをおこなう。
	# GETまたはfilterNameのパラメータが存在しない場合はfalse(振り分けを行わない)
	# POSTでfileterNameのパラメータが存在する場合はtrue（振り分けしたActionの実行）
	#
	# controller:: コントローラーオブジェクト
	# request:: リクエストオブジェクト
	# filterName:: 振り分け先アクション名が設定されているパラメータ名
	# 戻り値:: false:: 振り分けしない（デフォルトの処理を実行すること）
	#        true:: 振り分けを実行
	def ActionUtils.doFilter(controller, request, params, filterName)
		ret = true
		if request.get? 
			ret = false
		else
			if !params.key?(filterName)
				ret = false
			else	
				methodName = params[filterName]
				method = controller.method(methodName)
				method.call()
				ret = true
			end
		end
		return ret
	end



	#=== アクションのフィルター
	# 
	# リクエストメソッドと指定されたパラメータから、処理の振り分けをおこなう。
	# GETまたはfilterNameのパラメータが存在しない場合はfalse(振り分けを行わない)
	# POSTでfileterNameのパラメータが存在する場合はtrue（振り分けしたActionの実行）
	# paramname.x or paraname.y でもフィルターされる
	#
	# filter = {'paramname', 'action'}
	def ActionUtils.getFilter(filter)
		return ActionFilter.new(filter)
	end


	# パラメータのトリムを行う
	# rainy3用に、パラメータが配列の場合は配列の中もシークする
	# キーが指定されていない場合は全パラメータを変換する
	def ActionUtils.trimParams(params, *keys)
		
		params.each_pair {|key, value|

			if keys==nil || keys.length == 0 ||  searchKey(keys, key)

				if value != nil
					if value.class == Array
						
						value.each_with_index{|elm, i|
							value[i] = elm.strip
						}
					elsif value.class == ActiveSupport::HashWithIndifferentAccess
						
						value.each{|k, v|
							ActionUtils.trimParams(v, *keys)
						}

					elsif value.class == String
						params[key] = value.strip
					end	
				
				end
			end
		}
	end

	def ActionUtils.searchKey(list, key)
		
		ret = false
		for i in list
			if i == key
				ret = true
			end
		end
		return ret
	end
end

class ActionFilter

	def initialize(filter)
		@filter = filter
	end

	
	#=== アクションのフィルター
	#
	# リクエストメソッドと指定されたパラメータから、処理の振り分けをおこなう。
	# GETまたはfilterNameのパラメータが存在しない場合はfalse(振り分けを行わない)
	# POSTでfileterNameのパラメータが存在する場合はtrue（振り分けしたActionの実行）
	#
	# controller:: コントローラーオブジェクト
	# request:: リクエストオブジェクト
	# filterName:: 振り分け先アクション名が設定されているパラメータ名
	# 戻り値:: false:: 振り分けしない（デフォルトの処理を実行すること）
	#        true:: 振り分けを実行
	def doFilter(controller, request, params)

		ret = false

		if request.get? 
			ret = false
		else
			
			@filter.each{|paramname, action|
				
				if params.key?(paramname.to_s) || params.key?(paramname.to_s + '.x') || params.key?(paramname.to_s + '.y') 
					methodName = action
					method = controller.method(methodName)
					method.call()
					ret = true
					break
				end

			}

		end

		return ret

	end

end
