# encoding: utf-8

# グラフユーティリティクラス
class GraphUtils
	
	# グラフの最大幅が100で、マーカーの幅が20
	def self.getGraphDial(maxVal, dialMax=100, markerSpan=20)
		
		ret={}
		ret[:max]=100;
		ret[:marker]=[{:value=>20, :label=>2},{:value=>40, :label=>4},{:value=>60, :label=>6},{:value=>80, :label=>8},{:value=>100, :label=>10}]
		
		if maxVal == nil || maxVal == 0
			return ret;
		end
		
		ret[:max]=100
		ret[:marker]=[]
		
		# 90 以上の場合は100づつ目盛を増やす
		if maxVal >= 90
			x = maxVal / 100
			y = maxVal % 100
			# 表示上の最大値を求める
			if y != 0
				z=(x+1)*100;
			else
				z=x*100;
			end
		# 90 未満の場合は10づつ目盛を増やす
		else
			x = maxVal / 10
			y = maxVal % 10
			# 表示上の最大値を求める
			if y != 0
				z=(x+1)*10;
			else
				z=x*10;
			end
		end
		ret[:label_max]=z

		# 1目盛あたりの増加分を取得
		a=(z.to_f/dialMax.to_f)
		# 最大目盛値までループして、指定スパンでマーカーをセット
		for i in 1..dialMax
			if i % markerSpan == 0
				b = (a * i).to_i
				ret[:marker].push({:value=>i, :label=>b})
			end
		end
		
		return ret
	end
	
end
