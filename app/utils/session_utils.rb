# encoding: utf-8
class SessionUtils
	
	def initialize(session)
		@session = session
	end

	
	def save(key, value)
		@session[key] = value
	end
	
	def load(key)
		return @session[key]
	end

	# ログインユーザー情報の設定	
	def setLoginUser(id, name)
		@session[:dfajiewcjedx] = id
		@session[:dfdjsf1cjedk] = name
	end
	
	# 権限の設定
	def setRole(roleId, parentRoleId)
		@session[:zfjsf1sje8d0] = roleId
		@session[:a3waie0cj1d9] = parentRoleId
	end

	# 利用可能機能の追加
	def addPermission(enableFunctionId)
		if @session[:id3as67cjod1] == nil
			@session[:id3as67cjod1] = []
		end
		@session[:id3as67cjod1] << enableFunctionId
	end


	# ログインユーザーIDの取得
	def getLoginUserId
		return @session[:dfajiewcjedx]
	end

	# ログインユーザー名の取得
	def getLoginUserName
		return @session[:dfdjsf1cjedk]
	end

	# 権限IDの取得
	def getRoleId
		return @session[:zfjsf1sje8d0]
	end
	
	# 継承元権限IDの取得
	def getParentRoleId
		return @session[:a3waie0cj1d9]
	end

	# 利用可能な機能IDの設定	
	def getEnableFunctionIds
		
		if  @session[:id3as67cjod1] == nil
			 @session[:id3as67cjod1] = []
		end
		
		return @session[:id3as67cjod1]
	end

	# セッションのクリア
	def clear
		@session.clear
	end
	
end