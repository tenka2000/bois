# encoding: utf-8

# 暗号化モジュール
class CryptoUtils
	
	# 暗号化
	def self.encode(str)
		enc = OpenSSL::Cipher::Cipher.new("aes-256-cbc")
		enc.encrypt
		enc.pkcs5_keyivgen("a1h832")
		return enc.update(str) + enc.final
	end

	# 複合化
	def self.decode(str)
		dec = OpenSSL::Cipher::Cipher.new("aes-256-cbc")
		dec.decrypt
		dec.pkcs5_keyivgen("a1h832")
		return dec.update(str) + dec.final
	end
end
