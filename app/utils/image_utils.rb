# encoding: utf-8

require "RMagick"
include Magick
class ImageUtils


	def initialize(imageData)

		@imageData = imageData
		@enableFormats = [
			{:format=>'jpeg', :content_type=>'image/jpeg'},
			{:format=>'jpg', :content_type=>'image/jpeg'},
			{:format=>'gif', :content_type=>'image/gif'},
			{:format=>'bmp', :content_type=>'image/bmp'},
			{:format=>'png', :content_type=>'image/png'}
		]

		image = Magick::Image.from_blob(@imageData).first
		fileType = image.format.downcase
		ret = false
		for format in @enableFormats
			if format[:format] == fileType
				@format = format[:format]
				@contentType = format[:content_type]
				ret = true
				break
			end
		end
		if !ret
			raise "format error"
		end
	end

	def getFormat
		return @format
	end

	def getContentType
		return @contentType
	end



	# 縦横の幅を指定サイズ内に収まるようにリサイズする
	def resizeSquare(size, quality)

		image = Magick::Image.from_blob(@imageData).first

		_rate = 0;
		if image.rows > image.columns

			if(image.rows > size)
				_rate = size.to_s.to_f / image.rows
			else
				_rate = 1
			end

		else
			if(image.columns > size)
				_rate = size.to_s.to_f / image.columns
			else
				_rate = 1
			end
		end

		_width = (image.columns * _rate).truncate
		_height = (image.rows * _rate).truncate
		image.resize!(_width,_height)

#		image.quality = quality
		return image.to_blob{
							self.quality = quality
						}
	end

	# 縦幅を指定サイズ内に収まるようにリサイズする
	def resizeWidth(size, quality)
		image = Magick::Image.from_blob(@imageData).first
		
    _rate = 0;

		if(image.columns > size)
			_rate = size.to_s.to_f / image.columns
		else
			_rate = 1
		end
	
		_width = (image.columns * _rate).truncate
		_height = (image.rows * _rate).truncate
	
		image.resize!(_width,_height)
		image.quality = quality
		return image.to_blob{
							self.quality = quality
						}

	end

	# 縦幅を指定サイズ内に収まるようにリサイズする
	def resizeHeight(size, quality)
		image = Magick::Image.from_blob(@imageData).first
		
    _rate = 0;

		if(image.rows > size)
			_rate = size.to_s.to_f / image.rows
		else
			_rate = 1
		end
	
		_width = (image.columns * _rate).truncate
		_height = (image.rows * _rate).truncate
	
		image.resize!(_width,_height)
		return image.to_blob{
							self.quality = quality
						}

	end
	
end