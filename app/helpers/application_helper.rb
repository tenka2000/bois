# encoding: utf-8
module ApplicationHelper
  #　
  def errors_for(object)
    return nil if object.errors.empty?
    render :inline => <<-D, :locals => {:object => object}
    <div class='form-errors'>
      <ul>
        <% object.errors.full_messages.each do |message| %>
          <li><%= message.gsub(/(\s|　)+/, '') %></li>
        <% end %>
      </ul>
    </div>
    D
  end


	def errors(validators, prefix='', suffix='', *keys)
		
		result = ""
		
		if validators == nil
			return ""
		end

		for key in keys
			messages = validators.getMessages(key)
	
			if messages == nil
				return ""
			end
	
			for message in messages
				result = result + prefix + message + suffix
			end
			
		end
		
		return result.html_safe

	end
	
	def focus(validators)
		if validators == nil
			return ""
		end
		
		ret = validators.getFocus
		if ret == nil
			ret = ''
		else
			ret = "$(document).ready(function(){$('*[name=" + ret.to_s +  "]').focus();});"
		end

		return ret.html_safe

	end
	

	# radio_tagのcheked判定
	def is_checked(val, param)
		
		if param == nil
			return false
		end
		
		if param.class == Array
			
			for p in  param
				
				if val.to_s == p.to_s
					return true
				end

			end
			
		else
			if val.to_s == param.to_s
				return true
			end

		end

		return false

	end

	# Hash配列からOption用配列に変換する
	def hashlist_to_options(list, valueKey, labelKey)
		ret = []
		if list != nil
			for data in list
				ret << [data[labelKey],data[valueKey]]
			end
		end
		return ret
	end

	# Hash配列から表示用ラベルを取得する
	def show_from_hashlist(list, valueKey, labelKey, value)
		ret = ''
		if list != nil
			for data in list
				if data[valueKey].to_s == value.to_s
					ret = data[labelKey].to_s
					break
				end
			end
		end
		return ret
	end


	# rainy3用フォーカス先出力ヘルパー
	def r3focus(validators)
		if validators == nil
			return ""
		end
		
		ret = validators.getFocus
		if ret == nil
			ret = ''
		else
			ret = ret.to_s
		end
		return ret
	end

	# rainy3用エラーメッセージ出力ヘルパー
	def r3errors(validators)
		
		result = ""
		
		if validators == nil
			return ""
		end

			messages = validators.getAllMessages()
	
			if messages == nil
				return ""
			end
			
			messages.each_pair {|key,values|
				result += '<message id="' + key.to_s + '">'
				for value in values
					result += '&lt;p&gt;' + value.to_s + '&lt;p&gt;'
				end
				result += '</message>'
			}
		
		return result.html_safe

	end

	def record_id(page, pageSize, id)
		
	end
	
	# 二重エスケープ用
	def hh(val)
		if val == nil
			return ''
		end 
		val = val.gsub('&', '&amp;')
		val = val.gsub('<', '&lt;')
		val = val.gsub('>', '&gt;')
		val = val.gsub('"', '&quot;')
		return val
	end
	
	

	def nl2br(str)
		if str == nil
			return ''
		end
		return str.gsub(/\r\n|\r|\n/, "<br />")
	end
	
	
	def from_to_date(from, to, format)

		ret = ''
		n = Time.now	
		_from = TimeUtils.formatDate(n, format)
		_to = TimeUtils.formatDate(n, format)

		_from = '<span style="visibility:hidden;">' + _from + '</span>';
		_to = '<span style="visibility:hidden;">' + _to + '</span>';

		if from != nil
			_from = TimeUtils.formatDate(from, format)
		end
		if to != nil
			_to = TimeUtils.formatDate(to, format)
		end
		
		if from != nil || to != nil
			
			ret = _from + '～' + _to
			
		end
		
		return ret
		
	end
	
	# 権限の利用可否判定
	def permit_functions(*functionCodes)
		ret=false
		se = SessionUtils.new(session)
		fonctions = se.getEnableFunctionIds
		for fonction in fonctions
			for functionCode in functionCodes
				if functionCode.value == fonction
					ret=true
					break
				end
			end
		end
		return ret
	end
end

