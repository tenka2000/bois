# encoding: utf-8

# メッセージの定義
class Messages

	LOGIN_ERROR =													MessageItem.new("LOGIN IDまたはパスワードが間違っています。")
	LOGIN_INPUT = 												MessageItem.new("セッションが切れています。ログインを行ってください。")

	ROLE_ERROR_TITLE = 										MessageItem.new("権限エラーが発生しました。")
	ROLE_ERROR_BODY = 										MessageItem.new("現在のユーザーでは指定の画面を利用する権限がありません。お手数ですが、ログイン画面から対象の機能を利用する権限をもつユーザーで再ログインを行ってください。")

	# 通常起きえない入力エラー
	FORM_INPUT_ERROR_OTHER_01=						MessageItem.new("%{1}の入力値が不正です。")
	FORM_INPUT_ERROR_OTHER_02=						MessageItem.new("入力値が不正です。")

	FORM_ERROR_INPUT_01=									MessageItem.new("%{1}を入力してください。")
	FORM_ERROR_INPUT_02=									MessageItem.new("入力してください。")
	FORM_ERROR_INPUT_03=									MessageItem.new("[%{1}]　%{2}を入力してください。")

	FORM_ERROR_SELECT_01=									MessageItem.new("%{1}を選択してください。")
	FORM_ERROR_SELECT_02=									MessageItem.new("選択してください。")

	FORM_ERROR_NUMBER_01=									MessageItem.new("%{1}は数値を入力してください。")
	FORM_ERROR_NUMBER_02=									MessageItem.new("数値を入力してください。")
	FORM_ERROR_NUMBER_03=									MessageItem.new("[%{1}]　%{2}は数値を入力してください。")

	FORM_ERROR_RANGE_01=									MessageItem.new("%{1}は%{2}～%{3}までの数値を入力してください。")
	FORM_ERROR_RANGE_02=									MessageItem.new("%{1}～%{2}までの数値を入力してください。")
	FORM_ERROR_RANGE_03=									MessageItem.new("[%{1}]　%{2}は%{3}～%{4}までの数値を入力してください。")

	FORM_ERROR_ALPHA_01=									MessageItem.new("%{1}は半角英字を入力してください。")
	FORM_ERROR_ALPHANUM_01=								MessageItem.new("%{1}は半角英数字を入力してください。")
	FORM_ERROR_ALPHANUMARK_01=						MessageItem.new("%{1}は半角英数字か次の記号を入力してください。（　@+-.[]*_&lt;&gt;:!|　）")

	FORM_ERROR_ZENKANA_01=								MessageItem.new("%{1}は全角カタカナを入力してください。")
	FORM_ERROR_ZENKANA_02=								MessageItem.new("全角カタカナを入力してください。")

	FORM_ERROR_LENGTH_01=									MessageItem.new("%{1}は%{2}～%{3}文字で入力してください。")
	FORM_ERROR_MINLENGTH_01=							MessageItem.new("%{1}は%{2}文字以上で入力してください。")
	FORM_ERROR_MAXLENGTH_01=							MessageItem.new("%{1}は%{2}文字以内で入力してください。")
	FORM_ERROR_MAXLENGTH_02=							MessageItem.new("[%{1}]　%{2}は%{3}文字以内で入力してください。")

	FORM_ERROR_TEL_01=										MessageItem.new("%{1}は電話番号を指定してください。")
	FORM_ERROR_TEL_02=										MessageItem.new("電話番号を指定してください。")

	FORM_ERROR_EMAIL_01=									MessageItem.new("%{1}はメールアドレスを指定してください。")
	FORM_ERROR_EMAIL_02=									MessageItem.new("メールアドレスを指定してください。")


	FORM_ERROR_DATE_01=									MessageItem.new("%{1}は正しい日付を指定してください。")
	FORM_ERROR_DATE_02=									MessageItem.new("正しい日付を指定してください。")
	FORM_ERROR_DATE_RANEG_01=						MessageItem.new("%{1}は%{2}以降の日付を指定してください。")

	FORM_ERROR_TIME_01=									MessageItem.new("%{1}は正しい時間を指定してください。")
	FORM_ERROR_TIME_02=									MessageItem.new("正しい時間を指定してください。")
	FORM_ERROR_TIME_DATE_01=						MessageItem.new("%{1}は%{2}以降の時間を指定してください。")


	FORM_ERROR_DUPLICATE_01=							MessageItem.new("「%{1}」はすでに使用されています。別の%{2}を入力してください。")

	FORM_DUPLICATE_WARN_TITLE=						MessageItem.new("更新確認")
	FORM_DUPLICATE_WARN_DAIRY_MSG=				MessageItem.new("すでに作業実績が登録されています。作業実績を更新してもよろしいですか？")

	FORM_READ_ERROR_TITLE=								MessageItem.new("読み込みエラー")
	FORM_READ_ERROR_NO_RECORD_MESSAGE=		MessageItem.new("指定した%{1}が存在しません。再読み込みを行ってください。")
	FORM_READ_ERROR_COMMON_MESSAGE=				MessageItem.new("データの読み込みに失敗しました。恐れ入りますが、再度登録処理を行ってください。<br />問題が解決しない場合は、システム管理者にお問い合わせください。")

	FORM_CREATE_ERROR_TITLE=							MessageItem.new("登録エラー")
	FORM_CREATE_ERROR_COMMON_MESSAGE=			MessageItem.new("データの登録に失敗しました。恐れ入りますが、再度登録処理を行ってください。<br />問題が解決しない場合は、システム管理者にお問い合わせください。")

	FORM_EDIT_ERROR_TITLE=								MessageItem.new("更新エラー")
	FORM_EDIT_ERROR_COMMON_MESSAGE=				MessageItem.new("データの更新に失敗しました。恐れ入りますが、再度更新処理を行ってください。<br />問題が解決しない場合は、システム管理者にお問い合わせください。")

	FORM_DELETE_ERROR_TITLE=							MessageItem.new("削除エラー")
	FORM_DELETE_ERROR_COMMON_MESSAGE=			MessageItem.new("データの削除に失敗しました。恐れ入りますが、再度削除処理を行ってください。<br />問題が解決しない場合は、システム管理者にお問い合わせください。")


end
