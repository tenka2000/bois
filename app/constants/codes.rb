# encoding: utf-8

class Codes

	RAINY3_RESULT_CODE_OK=						CodeItem.new(1, "正常終了")
	RAINY3_RESULT_CODE_USER_ERROR=		CodeItem.new(2, "ユーザーエラー")
	# !! OK後再リクエストするので、force=trueの場合はエラーチェックをせずに初期を行うこと
	RAINY3_RESULT_CODE_WARNING=				CodeItem.new(3, "警告")	
	RAINY3_RESULT_CODE_ABEND=					CodeItem.new(4, "処理中断")
	RAINY3_RESULT_CODE_CERT_ERROR=		CodeItem.new(5, "認証失敗")
	RAINY3_RESULT_CODE_ROLE_ERROR=		CodeItem.new(6, "アクセス権限なし")
	RAINY3_RESULT_CODE_SYSTEM_ERROR=	CodeItem.new(9, "システムエラー")

end

