# encoding: utf-8
class MessageItem
	def initialize(message)
		@value=message
	end

	def get(*args)
		str = @value
		if args == nil
			args = []
		end
		args.each_with_index{ |value, index| str = str.sub("%{#{ ( index + 1 ).to_s }}", value.to_s)}
		return str
	end

end