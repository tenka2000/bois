# encoding: utf-8

class Items

	# HTMLタグ
	HTMLTAG_ERROR_PREFIX = '<p>'
	HTMLTAG_ERROR_SUFFIX = '</p>'

	# 共通ラベル
	LABEL_COMMON_SYSTEM_NAME = "Bois"



=begin
	# ユーザー関連
	LABEL_USER = 'ユーザー'
	LABEL_USER_LOGIN_ID = 'ログインID'
	LABEL_USER_PASSWORD = 'パスワード'
	LABEL_USER_LAST_NAME = '名前（姓）'
	LABEL_USER_FIRST_NAME = '名前（名）'
	LABEL_USER_NAME = '名前'

	LABEL_USER_LAST_NAME_KANA = '名前（セイ）'
	LABEL_USER_FIRST_NAME_KANA = '名前（メイ）'
	LABEL_USER_NAME_KANA = '名前（カナ）'

	LABEL_USER_TEL = '電話番号'
	LABEL_USER_EMAIL = 'メールアドレス'
	LABEL_USER_ROLE = '権限'
	
	# パスワード関連
	LABEL_PASSWORD_OLD = '現在のパスワード'
	LABEL_PASSWORD_NEW = '新しいパスワード'
	LABEL_PASSWORD_CONF = '新しいパスワード（確認）'
	
	# 実績報告関連
	LABEL_WORK_DATE = '報告日'
	LABEL_WORK_TYPE = '出勤区分'
	LABEL_WORK_PATTERN = '勤務体系'
	LABEL_WORK_START_TIME = '就業時間（開始）'
	LABEL_WORK_END_TIME = '就業時間（終了）'
	LABEL_WORK_TIME = '就業時間'
	LABEL_WORK_EXCEPT_HOURS = '不就労'
	LABEL_WORK_MOVE_HOURS = '移動時間'
	LABEL_WORK_NOTE = '備考'
	LABEL_WORK_PLACE = '主作業場所'	
	LABEL_WORK_RATE = '進捗率'	
	LABEL_WORK_HOURS = '作業時間'	
	LABEL_WORK_DETAIL = '詳細'	


	# 顧客関連
	LABEL_CUSTOMER_PREFIX = '識別情報'
	LABEL_CUSTOMER_NAME = 	'顧客名'
	LABEL_CUSTOMER_NAME_KANA = 	'顧客名（カナ）'
	LABEL_CUSTOMER_CONTACT = 	'連絡先'
	LABEL_CUSTOMER_INFORMATION = 	'その他情報'
	LABEL_CUSTOMER_STATUS = 	'状態'
	LABEL_PERSON_NAME = '担当者名'
	LABEL_PERSON_NAME_KANA = '担当者名（カナ）'
	LABEL_PERSON_DIVISION = '部署名'
	LABEL_PERSON_POST = '役職'
	LABEL_PERSON_CONTACT = '連絡先'

	# 案件関連
	LABEL_MATTER_CUSTOMER = '顧客'
	LABEL_MATTER_TITLE = '案件名'
	LABEL_MATTER_TYPE = '案件種別'
	LABEL_MATTER_OUTLINE = '案件概要'
	LABEL_MATTER_OCCURRENCE_DATE = '案件発生日'
	LABEL_MATTER_START_SCHEDULED_DATE = '開始予定日'
	LABEL_MATTER_END_SCHEDULED_DATE = '終了予定日'
	LABEL_MATTER_STATUS = '案件状態'
	LABEL_MATTER_CANCEL_REASON = '棄却理由'
	LABEL_MATTER_NOTE = '備考'
	LABEL_MATTER_USER = '案件責任者'

	# 見積もり関連
	LABEL_ESTIMATE_MATTER = '案件'
	LABEL_ESTIMATE_NO = '見積番号'
	LABEL_ESTIMATE_TITLE = '件名'
	LABEL_ESTIMATE_DETAIL = '詳細'
	LABEL_ESTIMATE_SINGLE_PRICE = '単価'
	LABEL_ESTIMATE_QUANTITY = '工数・数量'
	LABEL_ESTIMATE_PRICE = '価格（税抜）'
	LABEL_ESTIMATE_PRICE_TAXED = '価格（税込）'
	LABEL_ESTIMATE_DATE = '見積もり日'
	LABEL_ESTIMATE_USER = '見積もり責任者'
	LABEL_ESTIMATE_STATUS = '見積もり状態'
	LABEL_ESTIMATE_CANCEL_REASON = '棄却理由'
	LABEL_ESTIMATE_NOTE = '備考'

	# 受注関連
	LABEL_ORDER_NO = '受注番号'
	LABEL_ORDER_PRICE = '受注金額（税抜）'
	LABEL_ORDER_PRICE_TAXED = '受注金額（税込）'
	LABEL_ORDER_DATE = '受注日'
	LABEL_PAYMENT_DATE = '請求予定日'

	# 請求関連
	LABEL_PAYMENT_NO = '請求番号'
	LABEL_PAYMENT_PRICE = '請求金額（税抜）'
	LABEL_PAYMENT_PRICE_TAXED = '請求金額（税込）'
	LABEL_PART_PAYMENT_DATE = '請求予定日'

	# アサイン関連
	LABEL_ASSIGN_USER = 'ユーザー名'
	LABEL_ASSIGN_START_DATE = '開始日'
	LABEL_ASSIGN_END_DATE = '終了日'

	# タスク関連
	LABEL_TASK_TITLE = 'タスク名'
	LABEL_TASK_TYPE = '種別'
	LABEL_TASK_START_DATE = '開始日'
	LABEL_TASK_END_DATE = '終了日'
	LABEL_TASK_MAIN_USER = '主担当'
	LABEL_TASK_RATE = '進捗率'
	LABEL_TASK_STATUS = '状態'
	LABEL_TASK_DETAIL = '詳細'
	LABEL_TASK_DISPLAY_ORDER = '順番'
	
	# 交通費関連
	LABEL_CARFARE_REPORT_DATE = '支払日'
	LABEL_CARFARE_TARGET = '業務・相手先'
	LABEL_CARFARE_CHARGE_CODE = 'チャージコード'
	LABEL_CARFARE_WAY = '利用路線'
	LABEL_CARFARE_MOVE_FROM = '出発'
	LABEL_CARFARE_MOVE_TO = '到着'
	LABEL_CARFARE_EXPENSE = '単価'
	LABEL_CARFARE_ROUND_CODE = '区分'

	# 経費関連
	LABEL_COST_REPORT_DATE = '支払日'
	LABEL_COST_DETAIL = '経費内容'
	LABEL_COST_PAYEE = '支払先'
	LABEL_COST_CHARGE_CODE = 'チャージコード'
	LABEL_COST_EXPENSE = '費用'
	
	# 権限関連
	LABEL_ROLE_NAME = '権限名'
	LABEL_ROLE_PARENT_ROLE = '継承元権限'
	LABEL_ROLE_FUNCTION = '利用可能機能'

	# 勤務形態関連
	LABEL_WORKPATTERN_CODE = '勤務形態コード'
	LABEL_WORKPATTERN_NAME = '勤務形態名'
	LABEL_WORKPATTERN_SCRIPT = '勤務時間算出スクリプト'
	LABEL_WORKPATTERN_DISPLAY_ORDER = '表示順'
	LABEL_WORKPATTERN_INVALID_FLG = '状態'


	# マイルストーンのラベル
	LABEL_MILESTONE_YET = '未着'
	LABEL_MILESTONE_COMPLETE = '完了'
=end
	
	# パスワードのソルト値
	PASSWORD_SALT="dsfjahan_"
	
	
	# 日本語曜日
	LABEL_WEEK_MON = '月'
	LABEL_WEEK_TUE = '火'
	LABEL_WEEK_WED = '水'
	LABEL_WEEK_THU = '木'
	LABEL_WEEK_FRI = '金'
	LABEL_WEEK_SAT = '土'
	LABEL_WEEK_SAN = '日'

	# 日付キーフォーマット
	FORMAT_DAY_KEY_FORMAT = '%04d%02d%02d'

	# 日本語曜日フォーマット
	FORMAT_DAY_FORMAT_01 = '%04d年%02d月%02d日（%s）'
	FORMAT_DAY_FORMAT_02 = '%d年%d月%d日（%s）'

	FORMAT_DAY_FORMAT_11 = '%02d月%02d日（%s）'
	FORMAT_DAY_FORMAT_12 = '%d月%d日（%s）'
	
	# 変換元時間フォーマット
	FORMAT_CONV_FROM_TIME = [
														Regexp.new('(^[0-2]{1}[0-9]{1})([0-5]{1}[0-9]{1})$'),
														Regexp.new('(^[0-2]{0,1}[0-9]{1}):([0-5]{0,1}[0-9]{1})$'),
														Regexp.new('(^[0-2]{0,1}[0-9]{1})時([0-5]{0,1}[0-9]{1})分$'),
													];
	# 変換先時間フォーマット
	FORMAT_CONV_TO_TIME_01 = '%02d:%02d'
	FORMAT_CONV_TO_TIME_02 = '%d:%02d'
	FORMAT_CONV_TO_TIME_03 = '%02d時%02d分'
	FORMAT_CONV_TO_TIME_04 = '%d時%02d分'

	# 変換元日付フォーマット
	FORMAT_CONV_FROM_DATE = [
														Regexp.new('(^[0-9]{4})([0-1]{1}[0-9]{1})([0-3]{1}[0-9]{1})$'),
														Regexp.new('(^[0-9]{4})/([0-1]{0,1}[0-9]{1})/([0-3]{0,1}[0-9]{1})$'),
														Regexp.new('(^[0-9]{4})-([0-1]{0,1}[0-9]{1})-([0-3]{0,1}[0-9]{1})$'),
														Regexp.new('(^[0-9]{4})年([0-1]{0,1}[0-9]{1})月([0-3]{0,1}[0-9]{1})日$'),
													];
	# 変換先日付フォーマット
	FORMAT_CONV_TO_DATE_01 = '%04d/%02d/%02d'
	FORMAT_CONV_TO_DATE_02 = '%04d/%d/%d'
	FORMAT_CONV_TO_DATE_03 = '%04d年%02d月%02d日'
	FORMAT_CONV_TO_DATE_04 = '%04d年%d月%d日'

	# 変換元年月フォーマット
	FORMAT_CONV_FROM_YM = [
														Regexp.new('(^[0-9]{4})([0-1]{1}[0-9]{1})$'),
														Regexp.new('(^[0-9]{4})/([0-1]{0,1}[0-9]{1})$'),
														Regexp.new('(^[0-9]{4})-([0-1]{0,1}[0-9]{1})$'),
														Regexp.new('(^[0-9]{4})年([0-1]{0,1}[0-9]{1})月$'),
													];

	# 変換先日付フォーマット
	FORMAT_CONV_TO_YM_01 = '%04d/%02d'
	FORMAT_CONV_TO_YM_02 = '%04d/%d'
	FORMAT_CONV_TO_YM_03 = '%04d年%02d月'
	FORMAT_CONV_TO_YM_04 = '%04d年%d月'

	# チャージコード下桁数
	#FORMAT_CHARGE_CODE = '%d%03d'
	FORMAT_CHARGE_CODE_DOWN = 4
	
	# 無期限設定の最終日
	INDEFINITE_END_DATE = Date.new(9999, 12, 31)
	INITIALIZE_START_DATE = Date.new(1900, 1, 1)


end