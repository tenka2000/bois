# encoding: utf-8
class IllegalMultiParamsException < StandardError
	# 複数入力パラメーター例外
	
	def initialize(errors)
		@errors = errors
	end
	
	def getErrors
		return @errors
	end
	
end
