# encoding: utf-8
class RecordRequireException < StandardError
	# レコードが必要な場合にレコードが存在しない
	# たとえば、管理者権限が必ず１件必要な場合など
end
