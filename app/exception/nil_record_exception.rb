# encoding: utf-8
class NilRecordException < StandardError
	# レコードが存在しない
end
