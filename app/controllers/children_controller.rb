# encoding: utf-8
class ChildrenController < ApplicationBaseController

=begin
	before_filter :only => [:index] do |controller|
		controller.beforePageFilter(Codes::FUNCTION_CODE_USERS)
	end
	before_filter :beforeAjaxFilter, :except => [:index]
=end

	# メイン画面
	def index
		# 権限の取得
		respond_to do |format|
			format.html # index.html.erb
		end
	end

	# 一覧
	def list
		@result = Codes::RAINY3_RESULT_CODE_OK.value
		@title = ''
		@message = ''
		@page={:page=>params[:page], :page_size=>0, :total=>0, :results=>[]}
		
		begin
#			@page = User.getPageList(params)
		rescue => ex
			Rails.logger.error(ex.backtrace.join(", "))
			@result = Codes::RAINY3_RESULT_CODE_ABEND.value
			@title=Messages::FORM_READ_ERROR_TITLE.get
			@message = Messages::FORM_READ_ERROR_COMMON_MESSAGE.get
		ensure
			respond_to do |format|
				format.xml { render :layout => false }
			end
		end
	end

	# 一覧
	def list
		@result = Codes::RAINY3_RESULT_CODE_OK.value
		@title = ''
		@message = ''
		@page={:page=>params[:page], :page_size=>0, :total=>0, :results=>[]}

		begin

			@dummy = []

			@dummy << 1
			@dummy << 2
			@dummy << 3
			@dummy << 4
			@dummy << 5
			@dummy << 6
			@dummy << 7
			@dummy << 8
			@dummy << 9
			@dummy << 10

		rescue => ex
			Rails.logger.error(ex.backtrace.join(", "))
			@result = Codes::RAINY3_RESULT_CODE_ABEND.value
			@title=Messages::FORM_READ_ERROR_TITLE.get
			@message = Messages::FORM_READ_ERROR_COMMON_MESSAGE.get
		ensure
			respond_to do |format|
				format.xml { render :layout => false }
			end
		end
	end

	# フォーム
	def form

		@result = Codes::RAINY3_RESULT_CODE_OK.value
		@title=''
		@message = ''
		@focus = 'customer_name'
		@validators = Validators.new
		@record = nil

		begin
=begin
			@record = Estimate.getRecord(params[:id].to_i)
			
			if @record == nil
				raise NilRecordException
			end
			# 請求数を取得
			@paymentCount = Payment.where(:del_flg=>false, :estimate_id=>params[:id].to_i).count
			# 関連請求を取得
			@relations = Payment.getRelationalPayments(@record[:id], nil)
=end
		rescue NilRecordException => e1
			@result = Codes::RAINY3_RESULT_CODE_ABEND.value
			@title=Messages::FORM_READ_ERROR_TITLE.get
			@message = Messages::FORM_READ_ERROR_COMMON_MESSAGE.get
		rescue => ex
			Rails.logger.error(ex)
			Rails.logger.error(ex.backtrace.join(", "))
			@result = Codes::RAINY3_RESULT_CODE_ABEND.value
			@title=Messages::FORM_READ_ERROR_TITLE.get
			@message = Messages::FORM_READ_ERROR_COMMON_MESSAGE.get
		ensure
			respond_to do |format|
				format.xml { render :layout => false }
			end
		end
	end


	# 新規登録
	def create

		@result = Codes::RAINY3_RESULT_CODE_OK.value
		@title=''
		@message = ''
		@validators = Validators.new
		@record = nil

		begin
		# 入力値の整形
			ActionUtils.trimParams(params)

=begin
			# 入力チェック（Error:IllegalParamsException）
			@validators.validateUser(params, :create)

			# レコードの生成
			# パスワードをSHA256に変換
			@record = User.create!(
											:last_name=>params[:last_name][0],
											:first_name=>params[:first_name][0],
											:last_name_kana=>params[:last_name_kana][0],
											:first_name_kana=>params[:first_name_kana][0],
											:login_id=>params[:login_id][0],
											:password=>PasswordUtils.toHash(params[:password][0]),
											:role_id=>params[:role_id][0].to_i,
											:tel=>params[:tel][0],
											:email=>params[:email][0],
											:del_flg=>false,
											:invalid_flg=>params[:invalid_flg][0]
										)

			# 成功時メール通知が必要な場合
			if params[:notice][0] == 'true'
				# メールを送信する（Error:MailTransferException）
				MailUtils.deliver(
						SystemMailer.createUserNotify(
																	params[:email][0],
																	params[:last_name][0] + " " + params[:first_name][0], 
																	params[:login_id][0], 
																	params[:password][0]
																	))
			end
=end
		rescue IllegalParamsException => e1
			@result = Codes::RAINY3_RESULT_CODE_USER_ERROR.value
		rescue => ex
			Rails.logger.error(ex)
			Rails.logger.error(ex.backtrace.join(", "))
			# 登録失敗時、画面にエラーを表示し、再入力を促す
			@result = Codes::RAINY3_RESULT_CODE_ABEND.value
			@title=Messages::FORM_CREATE_ERROR_TITLE.get
			@message = Messages::FORM_CREATE_ERROR_COMMON_MESSAGE.get
		ensure
			respond_to do |format|
				format.xml { render :layout => false, :template => 'children/form'} # form.html.erb
			end

		end

	end

end
