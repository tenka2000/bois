# encoding: utf-8
class RequestController < ApplicationBaseController

=begin
	before_filter :only => [:index] do |controller|
		controller.beforePageFilter(Codes::FUNCTION_CODE_USERS)
	end
	before_filter :beforeAjaxFilter, :except => [:index]
=end

	# メイン画面
	def index

		# パラメーターとアクションのマッピング
		filter = ActionUtils.getFilter({
			:confirm=>'confirm',
			:compleat=>'compleat',
		})

		if !filter.doFilter(self, request, params)
			respond_to do |format|
				format.html
			end
		end

	end
	
	# 確認画面
	def confirm
		template ='request/confirm'
		respond_to do |format|
			format.html{render :template => template}
		end
	end

	# 完了画面
	def compleat
		template ='request/compleat'
		respond_to do |format|
			format.html{render :template => template}
		end
	end

end

