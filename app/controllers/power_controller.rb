# encoding: utf-8
class PowerController < ApplicationBaseController

=begin
	before_filter :only => [:index] do |controller|
		controller.beforePageFilter(Codes::FUNCTION_CODE_USERS)
	end
	before_filter :beforeAjaxFilter, :except => [:index]
=end

	# メイン画面
	def index
		respond_to do |format|
			format.html
		end
	end

	# 一覧
	def list

=begin
入力：params[:rack_no][n] ... ラック番号の配列
		params[:page] ... ページ番号
		params[:sort] ... ソートカラム（customer_name,site,floor,ruck...）
		params[:direction] ... ソート方向（asc,desc）
		
出力：power.html.erb
		<page_num></page_num> ... ページ番号
		<page_size></page_size> ... 該当ページの表示件数
		<total_size></total_size> ... 全データ数
		<rows>〜</rows> ... 行データ、レコード数分返してください。<row id="x">のxの値は行毎にユニークになる値を指定してください。
=end


		@result = Codes::RAINY3_RESULT_CODE_OK.value
		@title = ''
		@message = ''
		@page={:page=>params[:page], :page_size=>0, :total=>0, :results=>[]}

		begin

			@dummy = []
			if params[:search][0] != 'false'

				@dummy << 1
				@dummy << 2
				@dummy << 3
				@dummy << 4
				@dummy << 5
				@dummy << 6
				@dummy << 7
				@dummy << 8
				@dummy << 9
				@dummy << 10
			end

		rescue => ex
			Rails.logger.error(ex.backtrace.join(", "))
			@result = Codes::RAINY3_RESULT_CODE_ABEND.value
			@title=Messages::FORM_READ_ERROR_TITLE.get
			@message = Messages::FORM_READ_ERROR_COMMON_MESSAGE.get
		ensure
			respond_to do |format|
#				response.headers['Content-Type'] = 'application/xml; charset=utf-8'
				format.xml { render :layout => false }
			end
		end
	end
	
	# グラフ表示
	def graph
		
=begin
入力：params[:id] ... list.xml.erbの<row id="x">で指定したxの値
		
出力：画像ファイル
=end
		
		#初期化する(Lineは折れ線グラフ)
		#コンストラクタにnew('高さx横幅')の形で渡せばそのサイズになる
		g = Gruff::Line.new

		#タイトルと日本語フォントの指定
		#今回はこのファイルと同じところにフォントファイルを置いたが、絶対パス指定でもおｋ
		g.title = ""
		#g.font = "/System/Library/Fonts/HelveticaNeueDeskUI.ttc" 
		#g.font = "/usr/share/fonts/dejavu/DejaVuSans.ttf" 
		g.font = "/home/devguest/HelveticaNeueDeskUI.ttc" 

		#X軸のラベルサイズ
		g.marker_font_size = 12

		#Y軸の最大値と最小値を設定
		#設定しないと自動
		#最大値や最小値だけを設定するとエラーが出る。

		#g.maximum_value = 105
		#g.minimum_value = 95

		#背景色
		#色を2色設定しているので上から下へ青から白にグラデーションがかかる
		g.theme = {:background_colors => %w[#eee #ddd]}

		g.hide_title = false # タイトルなし
		g.legend_box_size=12	# 凡例のサイズ
		g.legend_font_size=12  # 凡例のフォントサイズ
		g.hide_dots = true  # グラフの点の削除
		g.line_width=2			# 線の太さ

		#描画するデータ設定(今回は1種類のデータ)
		#データの個数分以下の形式で追加していく
		#g.data("名前", [データ,データ,・・・], "色")
		g.data("M1", [10, 21, 18, 31, 29, 35, 15, 17, 11, 15, 35, 15, 17, 11, 15, 15, 15, 35, 15, 17, 11, 15, 15], "#cc6699")
		g.data("M2", [13, 22, 38, 12, 21, 11, 18, 39, 20, 29, 21, 11, 18, 39, 20, 29, 21, 11, 18, 39, 20, 29, 21], "#9966cc")
		g.data("M3", [13, 23, 34, 13, 23, 14, 28, 19, 10, 49, 51, 21, 28, 11, 39, 20, 29, 21, 11, 39, 20, 29, 21], "#99cc66")

		#X軸のラベル
		g.labels = {0 => '11',
								1 => '12',
								2 => '13',
								3 => '14',
								4 => '15',
								5 => '16',
								6 => '17',
								7 => '18',
								8 => '19',
								9 => '20',
								10 => '21',
								11 => '22',
								12 => '23',
								13 => '00',
								14 => '01',
								15 => '02',
								16 => '03',
								17 => '04',
								18 => '05',
								19 => '06',
								20 => '07',
								21 => '09',
								22 => '10',
							}


		g.write('/home/devguest/gruff.png')


		a = g.to_blob('PNG')

		send_data(a, :disposition => "inline", :type =>'image/png')
		
	end

end

