# encoding: utf-8
class ApplicationBaseController < ApplicationController
	
	# 画面全体に対するフィルター
	def beforePageFilter(functionCode=nil)

		se = SessionUtils.new(session)
		# セッションの有無をチェック
		if se.getLoginUserId == nil

			login = false
			# セッションがない場合、自動ログインの有効をチェックする
			if cookies[:auto_login] != nil && cookies[:auto_login] != ''
				# データを複合化
				ret = CryptoUtils.decode(cookies[:auto_login])
				re = Regexp.new("jsdg([0-9]+)ksds")
				if re.match(ret)
					userId = $1.to_i
					user = User.checkAutoLogin(userId)
					if user != nil
						# ログイン処理
						se.clear
						se.setLoginUser(user[:id], user[:last_name] + "　" + user[:first_name]);
						# 権限の取得
						role = Role.where(:id=>user[:role_id]).first
						se.setRole(role[:id], role[:parent_role_id])
						rolefunctions = Rolefunction.where(:role_id=>role[:id])
						for rolefunction in rolefunctions
							se.addPermission(rolefunction[:function_code])
						end

						login = true
					end
				end
			end

			# ログインしていない場合はログイン画面を表示する
			if !login
				@message=Messages::LOGIN_INPUT.get
				se.clear
				# ログインエラー
				respond_to do |format|
					format.html { render :template => 'login/index'}
				end
				return
			end
		end

		# 権限チェック
		if functionCode != nil
			ret=false
			se = SessionUtils.new(session)
			fonctions = se.getEnableFunctionIds
			for fonction in fonctions
				if functionCode.value == fonction
					ret=true
					break
				end
			end
			
			if !ret
				# 権限エラー
				respond_to do |format|
					
					@pageError={:title=>Messages::ROLE_ERROR_TITLE.get, :body=>Messages::ROLE_ERROR_BODY.get, :button_login=>true}
					format.html { render :template => 'common/error'}
				end
				return
			end
		end
	end
	
	# Ajaxリクエストに対するフィルター
	def beforeAjaxFilter
	
		se = SessionUtils.new(session)


		# セッションの有無をチェック
		if se.getLoginUserId == nil

			login = false
			# セッションがない場合、自動ログインの有効をチェックする
			if cookies[:auto_login] != nil && cookies[:auto_login] != ''
				# データを複合化
				ret = CryptoUtils.decode(cookies[:auto_login])
				re = Regexp.new("jsdg([0-9]+)ksds")
				if re.match(ret)
					userId = $1.to_i
					user = User.checkAutoLogin(userId)
					if user != nil
						se.setLoginUser(user[:id], user[:last_name] + "　" + user[:first_name]);
						login = true
					end
				end
			end

			if !login
				se.clear
				# ログインエラー
				respond_to do |format|
					format.html { render :template => 'login/ajaxError', :layout => false}
				end
			end
		end
	end
	
end
