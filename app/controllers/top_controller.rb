# encoding: utf-8
class TopController < ApplicationBaseController

=begin
	before_filter :only => [:index] do |controller|
		controller.beforePageFilter(Codes::FUNCTION_CODE_USERS)
	end
	before_filter :beforeAjaxFilter, :except => [:index]
=end

	# メイン画面
	def index
		
		@results=[]
		for i in 1..10
			@results << {:title=>'7/15　第1サイト　3Fにて空調フィルター交換作業がございます', :category=>'メンテナンス', :id=>1}
			@results << {:title=>'6/28 17:30　東京電力サービス内にて停電が発生しました', :category=>'障害', :id=>2}
			@results << {:title=>'省エネにご協力ください', :category=>'お知らせ', :id=>3}
		end
		
		
		respond_to do |format|
			format.html
		end
	end

	# 詳細画面
	def topic
		
		
		respond_to do |format|
			format.html
		end
	end

end

