var timePicker=new function(){
	
	var _self=this;
	this.target=null;
	this.showTarget=null;
	this.top=null;
	this.left=null;
	this.pos=null;
	this.frame=null;

	this.top=null;
	this.body=null;
	this.bottom=null;

	this.hour=null;
	this.min=null;
	this.baseYear=null;
	this.baseMonth=null;
	this.imagePath="../img/time.picker/";
	this.dayBoxIdPrefix="aoa6udf8i_";
	this.selectedHour=null;
	this.selectedMin=null;
	
	this.clickHour=null;
	this.clickMin=null;
	this.jqClickHour=null;
	this.jqClickMin=null;
	
			
	$(function(){
		_self.pos = $('<div class="time_picker_pos" style=""><div>').prependTo('body');
		_self.frame = $('<div class="time_frame"></div>').appendTo(_self.pos);


		_self.top = $('<div class="time_top"></div>').appendTo(_self.frame);
		_self.body = $('<div class="time_body"></div>').appendTo(_self.frame);
		_self.bottom = $('<div class="time_bottom"></div>').appendTo(_self.frame);

		_self.table = $('<table cellspacing="0" cellpadding="0" border="0"><tr></tr></table>').appendTo(_self.body);
		_self.hourTd = $('<td style="vertical-align:middle;color:#fff;"></td>').appendTo(_self.table);
		_self.delimTd = $('<td style="vertical-align:middle;color:#fff;width:13x;">・<br/>・</td>').appendTo(_self.table);
		_self.minTd = $('<td style="vertical-align:middle;color:#fff;"></td>').appendTo(_self.table);

		_self.hour = $('<div class="time_hours"></div>').appendTo(_self.hourTd);
		_self.min = $('<div class="time_mins"></div>').appendTo(_self.minTd);

		rainy3.bindDocumentMousedown("hareknfdsa", function(e){
			var parent = app.getParentJQueryObjectByClassName($(e.target), 'time_picker_pos');
			if(parent == null){
				_self.hide();
			}
		});
	});

	this.show=function(obj,showTarget, top, left){
		var _self=this;
		_self.frame.hide();
		/* 初期化 */
		_self.top=null;
		_self.left=null;
		_self.frame.css('top', "0px");
		_self.frame.css('left', "0px");
		_self.selectedHour=null;
		_self.selectedMin=null;
		_self.clickHour=null;
		_self.clickMin=null;
		_self.jqClickHour=null;
		_self.jqClickMin=null;
	
		_self.showTarget=null;
		var year=null;
		var month=null;
		_self.target=$(obj);
		if(_self.target.val() != null && _self.target.val() != ""){
			var date = app.parseTimeString(_self.target.val());
			if(date != null){
				var _hour = date.getHours();
				var _min = date.getMinutes();
				_self.selectedHour=_hour;
				_self.selectedMin=_min;
			}
		}
		if(showTarget == null){
			_self.showTarget=_self.target;
		}
		else{
			_self.showTarget=$(showTarget);
		}
		_self.top=top;
		_self.left=left;
		_self._show(year, month);
		_self.showTarget.after(_self.pos);
		if(_self.top != null){
			_self.frame.css('top', _self.top + "px");
		}
		if(_self.left != null){
			_self.frame.css('left', _self.left + "px");
		}
		_self.target.keydown(function(e){
			if(e.keyCode == KEY.TAB){
				_self.hide();
			}
		})
		_self.frame.fadeIn('fast');
	}

	this._show=function(){
		var _self=this;
		var now = new Date();
		var nowHour = now.getHours();
		var nowMin = now.getMinutes();

		_self.hour.empty();
		_self.min.empty();

		var hourList = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
		var minList = [0,5,10,15,20,25,30,35,40,45,50,55];
		var minRange = [
					{min:0,max:4},
					{min:5,max:9},
					{min:10,max:14},
					{min:15,max:19},
					{min:20,max:24},
					{min:25,max:29},
					{min:30,max:34},
					{min:35,max:39},
					{min:40,max:44},
					{min:45,max:49},
					{min:50,max:54},
					{min:55,max:59}
			];

		var hourPrefix = "hfaushf3ufsdha_";
		var minPrefix = "fdna93sfdofaoi_";

		var html = '';
		for(var i=0; i<hourList.length; i++){
			
			var id=hourPrefix + i;
			html += '<a href="javascript:void(0);" onClick="timePicker.setHour(' + "'" + id + "'" + ', ' + hourList[i] + ');" ondblclick="timePicker.setForceHour(' + "'" + id + "'" + ', ' + hourList[i] + ');"><div id="' + id + '" class="';
			
			if(_self.selectedHour != null && _self.selectedHour == hourList[i]){
				html += 'box box_selected';
			}
			else if(nowHour == hourList[i]){
				html += 'box box_current';
			}
			else{
				html += 'box box_sel';
			}
			html += '">' + hourList[i] + '</div></a>';
		}
		_self.hour.append(html);

		html = '';
		for(var i=0; i<minList.length; i++){

			var id=minPrefix + i;
			html += '<a href="javascript:void(0);" onClick="timePicker.setMin(' + "'" + id + "'" + ', ' + minList[i] + ');" ondblclick="timePicker.setForceMin(' + "'" + id + "'" + ',' + minList[i] + ');"><div id="' + id + '" class="';
			
			if(_self.selectedMin != null && _self.selectedMin >= minRange[i]['min'] && _self.selectedMin <= minRange[i]['max']){
				html += 'box box_selected';
			}
			else if(nowMin >= minRange[i]['min'] && nowMin <= minRange[i]['max']){
				html += 'box box_current';
			}
			else{
				html += 'box box_sel';
			}
			html += '">' + minList[i] + '</div></a>';
		}
		_self.min.append(html);
	}
	
	this.hide=function(obj){
		var _self=this;
		if(_self.target!=null){
			_self.target.unbind('keydown');
		}
		_self.frame.fadeOut('fast');
		_self.target=null;
	}
	
	this.setHour=function(id, hour){
		var _self=this;

		_self.clickHour=hour;
		
		if(_self.jqClickHour != null && _self.jqClickHour.length != 0){
			_self.jqClickHour.removeClass('box_sel_selected');
			_self.jqClickHour.removeClass('box_current_selected');
			_self.jqClickHour.removeClass('box_selected_selected');
		}
		_self.jqClickHour = $('#' + id);
		if(_self.jqClickHour.length != 0){
			if(_self.jqClickHour.hasClass('box_sel')){
				_self.jqClickHour.addClass('box_sel_selected');
			}
			else if(_self.jqClickHour.hasClass('box_current')){
				_self.jqClickHour.addClass('box_current_selected');
			}
			else if(_self.jqClickHour.hasClass('box_selected')){
				_self.jqClickHour.addClass('box_selected_selected');
			}
		}
		
		if(_self.clickMin != null){
			var val = ("0"+ _self.clickHour).slice(-2) + ":" + ("0"+ _self.clickMin).slice(-2);
			if(_self.target != null){
				_self.target.val(val);
			}
			_self.hide();
		}
	}

	this.setMin=function(id, min){
		var _self=this;
		_self.clickMin=min;
		_self.jqClickMin = $('#' + id);
		if(_self.jqClickMin.length != 0){
			if(_self.jqClickMin.hasClass('box_sel')){
				_self.jqClickMin.addClass('box_sel_selected');
			}
			else if(_self.jqClickMin.hasClass('box_current')){
				_self.jqClickMin.addClass('box_current_selected');
			}
			else if(_self.jqClickMin.hasClass('box_selected')){
				_self.jqClickMin.addClass('box_selected_selected');
			}
		}

		if(_self.clickHour != null){
			var val = ("0"+ _self.clickHour).slice(-2) + ":" + ("0"+ _self.clickMin).slice(-2)
			if(_self.target != null){
				_self.target.val(val);
			}
			_self.hide();
		}
	}

	this.setForceHour=function(id, hour){
		var _self=this;

		_self.clickHour=hour;
		_self.jqClickHour = $('#' + id);
		if(_self.jqClickHour.length != 0){
			if(_self.jqClickHour.hasClass('box_sel')){
				_self.jqClickHour.addClass('box_sel_selected');
			}
			else if(_self.jqClickHour.hasClass('box_current')){
				_self.jqClickHour.addClass('box_current_selected');
			}
			else if(_self.jqClickHour.hasClass('box_selected')){
				_self.jqClickHour.addClass('box_selected_selected');
			}
		}

		var _min = null;
		if(_self.clickMin != null){
			_min = _self.clickMin;
		}
		else if(_self.selectedMin != null){
			_min = _self.selectedMin;
		}
		if(_min != null){
			var val = ("0"+ _self.clickHour).slice(-2) + ":" + ("0"+ _min).slice(-2)
			if(_self.target != null){
				_self.target.val(val);
			}
			_self.hide();
		}
	}

	this.setForceMin=function(id, min){
		var _self=this;
		_self.clickMin=min;
		if(_self.jqClickMin.length != 0){
			if(_self.jqClickMin.hasClass('box_sel')){
				_self.jqClickMin.addClass('box_sel_selected');
			}
			else if(_self.jqClickMin.hasClass('box_current')){
				_self.jqClickMin.addClass('box_current_selected');
			}
			else if(_self.jqClickMin.hasClass('box_selected')){
				_self.jqClickMin.addClass('box_selected_selected');
			}
		}

		var _hour = null;
		if(_self.clickHour != null){
			_hour = _self.clickHour;
		}
		else if(_self.selectedHour != null){
			_hour = _self.selectedHour;
		}
		
		if(_hour != null){
			var val = ("0"+ _hour).slice(-2) + ":" + ("0"+ _self.clickMin).slice(-2)
			if(_self.target != null){
				_self.target.val(val);
			}
			_self.hide();
		}
	}
}
