/****************************************************************
 * ガントチャートオブジェクト
 ****************************************************************/
var Rainy3Ganttchart = function(id){

	var GANTTCHART_PREFIX = "a1pj1ods_";
	var CANVAS_PREFIX="bcp9aoad1_";
	this.ganttchartData=null;
	this.loadurl=null;
	this.loadaction=null;
	this.unloadaction=null;
	this.target=null;
	this.taskColor={
		// その他
		0:'#bc8f8f',
		// 未着手
		1:'#2b8854',
		// 進行中
		2:'#66e',
		// 保留
		3:'#bc8f8f',
		// 中断
		4:'#bc8f8f',
		// 完了
		5:'#ffa500',
		// クローズ
		6:'#888'
	}

	////////////////////////////////////////////////
	// 初期化
	this.init(this, 'ganntchart');

	////////////////////////////////////////////////
	// イメージの描画
	this.addContent=function(id, jqTarget, contentInfo){
		var ganttchartData = null;
		eval(contentInfo);

		if(ganttchart == null){
			alert('Please define "var ganttchart={}"');
		}
		else{
			this.ganttchartData = ganttchart;
			this.loadurl = this.ganttchartData['url'];
			this.loadaction = this.ganttchartData['loadaction'];
			this.unloadaction = this.ganttchartData['unloadaction'];
			// 初期検索条件
			if(this.ganttchartData['initcondition'] != null){
				this.condition=this.ganttchartData['initcondition'];
			}
			this.target=jqTarget;
		}
	}

	////////////////////////////////////////////////
	// ガントチャートの読み込み
	this.load=function(){
		var _self = this;
		var _url = this.loadurl;
		var first=true;
		var dataMap = {};
		if(this.condition != null){
			for(var i in this.condition){
				dataMap[i] = this.condition[i];
			}
		}

		// HTTPリクエスト
		rainy3.request(this.loadurl, dataMap, false, function(head, body){
			
			// 成功時のアクション
			var jsonData={};

			jsonData['start_date']=body.find('task_start').text();
			jsonData['end_date']=body.find('task_end').text();
			jsonData['tasks']=[];
			var tasks = body.find('tasks');
			tasks.children('task').each(
				function(i){
					var data={};
					data['task_id'] = parseInt($(this).attr('id'));
					data['charge_code'] = $(this).find('charge_code').text();
					data['task_title'] = $(this).find('task_title').text();
					data['start_date'] = $(this).find('start_date').text();
					data['end_date'] = $(this).find('end_date').text();
					data['type'] = $(this).find('type').text();
					data['status'] = parseInt($(this).find('status').text());

					data['rate'] = [];
					data['base_rate'] =[];
					var rates = $(this).find('rates');
					rates.children('rate').each(
						function(j){
							var rate = $(this).text();
							if(rate != null && rate != ''){
								data['rate'].push(parseInt(rate));
							}
							else{
								data['rate'].push(null);
							}
						}
					);
					jsonData['tasks'].push(data);
				}
			);
			_self.draw(jsonData);
			
			if(_self.loadaction!=null){
				new _self.loadaction();
			}
		},
		function(){},
		function(){
			_self.protection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, true)
		},
		function(head, body){
			_self.releaseProtection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, _self.jqMessageBox, _self.jqMessageBoxShadow);
		});
	}

	// ガントチャートの描画
	this.draw=function(jsonData){
		var _self=this;
		var today = new Date();
		var topLabel = this.createCalendar(jsonData['start_date'], jsonData['end_date']);
		var currentDate = new Date();
		var moveCurrent=false;

		var dayWidth=20;
		var chargeCodeWidth=70;
		var taskTitleWidth=130;
		var baseRateWidth=0;
		var rateWidth=0;
		var leftLabelWidth=chargeCodeWidth+1+taskTitleWidth+1 // +baseRateWidth+1+rateWidth+1;
		var totalTableWidth=leftLabelWidth+(this.getTotalDayCount(topLabel)* dayWidth) + this.getTotalDayCount(topLabel);
		var leftLabelHeight=20;
		var leftLabelTotalHeight=leftLabelHeight;
		var barHeight=10;
		var topLabelHeight=20;
		var topLabelTotal1 = topLabelHeight * 2 + 2 - 1;
		var topLabelTotal2 = topLabelHeight * 3 + 3 - 1;


		var jqDiv = $('<div></div>').appendTo(_self.target);
		var jqTable = $('<table cellspacing="0" cellpadding="0" border="0" width="' + (totalTableWidth) +  '" class="ganttchart_table"></table>').appendTo(jqDiv);
		var topHtml = '<table cellspacing="0" cellpadding="0" border="0" width="' + (totalTableWidth) +  '" class="ganttchart_table">';
		var leftHtml = '<table cellspacing="0" cellpadding="0" border="0" width="' + (leftLabelWidth) +  '" class="ganttchart_table">'

		var html ='<tr><td id="fix_scale" rowspan="3" width="' + (leftLabelWidth - 1) + '" align="left" vAlign="top" style="height:' + topLabelTotal2 + 'px;padding:0px;"><div class="floating_pos"><div class="left"></div><div class="top"></div><div class="fix head"></div></div></td>';
		topHtml += '<tr><td rowspan="3" width="' + (leftLabelWidth - 1) + '" align="left" vAlign="top" style="height:' + topLabelTotal2 + 'px;padding:0px;"></td>';

		leftHtml +='<tr>';
		leftHtml +='<td id="left_fix_top" width="' + (chargeCodeWidth) + '" align="left" vAlign="top" style="padding:0px;"></td>';
		leftHtml +='<td width="' + (taskTitleWidth) + '" align="left" vAlign="top" style="padding:0px;"></td>';

		for(var i=0; i<topLabel.length; i++){
			html += '<td colspan="' + this.getYearDayCount(topLabel, topLabel[i]['year']) + '" width="' + (this.getYearDayCount(topLabel, topLabel[i]['year']) * dayWidth + this.getYearDayCount(topLabel, topLabel[i]['year']) - 1) + '" style="padding:0px;height:' + topLabelHeight + 'px;">' + topLabel[i]['year'] + '年</td>'
			topHtml += '<td class="head" colspan="' + this.getYearDayCount(topLabel, topLabel[i]['year']) + '" width="' + (this.getYearDayCount(topLabel, topLabel[i]['year']) * dayWidth + this.getYearDayCount(topLabel, topLabel[i]['year']) - 1) + '" style="padding:0px;height:' + topLabelHeight + 'px;">' + topLabel[i]['year'] + '年</td>';
		}
		html += '</tr>'
		topHtml += '</tr>';
		leftHtml += '</tr>';

		html += '<tr>'
		topHtml += '<tr>';
		for(var i=0; i<topLabel.length; i++){
			for(var j=0; j<topLabel[i]['months'].length; j++){
				html += '<td colspan="' + this.getMonthDayCount(topLabel, topLabel[i]['year'], topLabel[i]['months'][j]['month']) + '" width="' + (this.getMonthDayCount(topLabel, topLabel[i]['year'], topLabel[i]['months'][j]['month']) * dayWidth + this.getMonthDayCount(topLabel, topLabel[i]['year'], topLabel[i]['months'][j]['month'])-1) + '" style="padding:0px;height:' + topLabelHeight + 'px;">' + topLabel[i]['months'][j]['month'] + '月</td>'
				topHtml += '<td class="head" colspan="' + this.getMonthDayCount(topLabel, topLabel[i]['year'], topLabel[i]['months'][j]['month']) + '" width="' + (this.getMonthDayCount(topLabel, topLabel[i]['year'], topLabel[i]['months'][j]['month']) * dayWidth + this.getMonthDayCount(topLabel, topLabel[i]['year'], topLabel[i]['months'][j]['month'])-1) + '" style="padding:0px;height:' + topLabelHeight + 'px;">' + topLabel[i]['months'][j]['month'] + '月</td>';
			}
		}
		html += '</tr>'
		topHtml += '</tr>';

		html += '<tr>'
		topHtml += '<tr>';
		for(var i=0; i<topLabel.length; i++){
			for(var j=0; j<topLabel[i]['months'].length; j++){
				
				if(currentDate.getFullYear() == topLabel[i]['year'] && (currentDate.getMonth() + 1) == topLabel[i]['months'][j]['month']){
					moveCurrent=true;
				}
				
				for(var k=0; k<topLabel[i]['months'][j]['days'].length; k++){
					html += '<td class="';
					topHtml += '<td class="';
					
					if(today.getFullYear() == topLabel[i]['year'] && today.getMonth() + 1 == topLabel[i]['months'][j]['month'] && today.getDate() == topLabel[i]['months'][j]['days'][k]['day']){
							html +='current_day ';
							topHtml += 'current_day';
					}
					else if(topLabel[i]['months'][j]['days'][k]['day_of_week'] == 0){
						html +='sunday';
						topHtml += 'sunday';
					}
					else if(topLabel[i]['months'][j]['days'][k]['day_of_week'] == 6){
						html +='saturday';
						topHtml += 'saturday';
					}
					else{
						topHtml += 'head';
					}
					

					html += '"width="' + dayWidth + '" style="padding:0px;height:' + topLabelHeight + 'px;">' + topLabel[i]['months'][j]['days'][k]['day'] + '</td>';
					topHtml += '"width="' + dayWidth + '" style="padding:0px;height:' + topLabelHeight + 'px;">' + topLabel[i]['months'][j]['days'][k]['day'] + '</td>';
				}
			}
		}
		html += '</tr>'
		topHtml += '</tr>';
		topHtml += '</table>';

		// タスク描画領域の描画
		var count=0;
		for(var h=0; h<jsonData['tasks'].length; h++){
			
			html += '<tr>'
			html += '<td style="height:' + leftLabelTotalHeight + 'px;padding:0px;" class="day_box_under'
			if(jsonData['tasks'].length-1 == h){
				html +=' table_end';
			}
			html += '">';
			html += '</td>';

			leftHtml += '<tr>';
			leftHtml += '<td style="height:' + leftLabelTotalHeight + 'px;padding:0px;" height="' + leftLabelTotalHeight + '" class="day_box_under';
			if(jsonData['tasks'].length-1 == h){
				leftHtml +=' table_end';
			}
			leftHtml += '">';

			leftHtml += '<div style="width:' + (chargeCodeWidth) + 'px;line-height:' + leftLabelTotalHeight + 'px;height:100%;overflow:hidden;">';
			leftHtml += app.htmlEscape(jsonData['tasks'][h]['charge_code'])
			leftHtml += '</div>';
			leftHtml += '</td>';

			leftHtml += '<td align="left" style="text-align:left;height:' + leftLabelTotalHeight + 'px;padding:0px;" height="' + leftLabelTotalHeight + '" class="day_box_under'
			if(jsonData['tasks'].length-1 == h){
				leftHtml +=' table_end';
			}
			leftHtml += '">';

			leftHtml += '<div style="padding-left:2px;width:' + (taskTitleWidth - 2) + 'px;line-height:' + leftLabelTotalHeight + 'px;height:100%;overflow:hidden;">';
			leftHtml += app.htmlEscape(jsonData['tasks'][h]['task_title'])
			leftHtml += '</div>';
			leftHtml += '</td>';

			for(var i=0; i<topLabel.length; i++){
				for(var j=0; j<topLabel[i]['months'].length; j++){
					for(var k=0; k<topLabel[i]['months'][j]['days'].length; k++){
						html += '<td valign="top" align="left" class="day_box day_box_under'

						if(topLabel[i]['months'][j]['days'][k]['day_of_week'] == 0){
							html +='sunday day_box_under';
						}
						else if(topLabel[i]['months'][j]['days'][k]['day_of_week'] == 6){
							html +='saturday day_box_under ';
						}
						
						if(today.getFullYear() == topLabel[i]['year'] && today.getMonth() + 1 == topLabel[i]['months'][j]['month'] && today.getDate() == topLabel[i]['months'][j]['days'][k]['day']){
							html +=' current_day ';
						}

						if(jsonData['tasks'].length-1 == h){
							html +='table_end';
						}

						
						html += '"width="' + dayWidth + '" id="' + _self.getDayBoxId(topLabel[i]['year'], topLabel[i]['months'][j]['month'], topLabel[i]['months'][j]['days'][k]['day'], jsonData['tasks'][h]['task_id']) + '">'
						
						if(today.getFullYear() > topLabel[i]['year'] || 
								(today.getFullYear() == topLabel[i]['year'] && (today.getMonth() + 1) > topLabel[i]['months'][j]['month']) ||
								(today.getFullYear() == topLabel[i]['year'] && (today.getMonth() + 1) ==  topLabel[i]['months'][j]['month'] && today.getDate() >= topLabel[i]['months'][j]['days'][k]['day'])){


							if(h==0 && topLabel[i]['months'][j]['days'][k]['day_of_week'] == 5){
								var canvasStyle = this.getCanvasStyle(topLabel[i]['year'], topLabel[i]['months'][j]['month'], topLabel[i]['months'][j]['days'][k]['day'], count, topLabel, jsonData, dayWidth)
								html += '<div class="bar_pos">'
								html += '<canvas class="canvas" id="' + this.getCanvasId(topLabel[i]['year'], topLabel[i]['months'][j]['month'], topLabel[i]['months'][j]['days'][k]['day']) + '" ';
								html += 'width="' + canvasStyle['width']+ '" ';
								html += 'height="' + (leftLabelTotalHeight*jsonData['tasks'].length) + '" ';
	
								html += 'style="left:' + canvasStyle['left'] + 'px;">';
	
								html += '</canvas>'
								html += '</div>'
	
								topLabel[i]['months'][j]['days'][k]['canvas_style']=canvasStyle;
								count++;
							}
							else{
								html += ''
							}
						}
						html += '</td>'
					}
				}
			}
			html += '</tr>'
			leftHtml += '</tr>';
		}
		leftHtml+='</table>';
		
		
		$(html).appendTo(jqTable);

		// 固定部分の描画
		var jqFixScale = $('#fix_scale');

		// 左側
		var leftMenu = $('div.floating_pos div.left');
		leftMenu.width(jqFixScale.width() + 1);
		
		leftMenu.height(leftLabelTotalHeight*jsonData['tasks'].length  + 1 +  (topLabelTotal2 + 1))
		leftMenu.append(leftHtml);
		
		$('#left_fix_top').height((jqFixScale.height()));

		// 上側
		var topMenu = $('div.floating_pos div.top');
		topMenu.height(topLabelTotal2 + 1);
		topMenu.width(totalTableWidth+1);
		topMenu.append(topHtml);

		// 左上
		var fixMenu = $('div.floating_pos div.fix');
		fixMenu.height(topLabelTotal2);
		fixMenu.width(jqFixScale.width());

		var html = '<div><p><span style="color:'+ this.taskColor[1] + '">■</span>未着手</p>';
		html += '<p><span style="color:'+ this.taskColor[2] + '">■</span>進行中</p>'
		html += '<p><span style="color:'+ this.taskColor[3] + '">■</span>保留・中断</p></div>'
		html += '<div><p><span style="color:'+ this.taskColor[5] + '">■</span>完了</p>'
		html += '<p><span style="color:'+ this.taskColor[6] + '">■</span>クローズ</p></div>'
		fixMenu.html(html);

		// スクロール固定
		_self.target.scroll(function(e){
			leftMenu.css('top', e.target.scrollTop + 'px')
			leftMenu.css('left', e.target.scrollLeft + 'px')
			topMenu.css('top', e.target.scrollTop + 'px')
			fixMenu.css('left', e.target.scrollLeft + 'px')
			fixMenu.css('top', e.target.scrollTop + 'px')
			
		})
		// タスクの描画
		for(var i=0; i<jsonData['tasks'].length; i++){
			var start = jsonData['tasks'][i]['start_date'];
			var end = jsonData['tasks'][i]['end_date'];
			var startYear = parseInt(start.substring(0, 4));
			var startMonth = parseInt(start.substring(4, 6));
			var startDay = parseInt(start.substring(6, 8));
			var endYear = parseInt(end.substring(0, 4));
			var endMonth = parseInt(end.substring(4, 6));
			var endDay = parseInt(end.substring(6, 8)) + 1;
			
			var calStartYear = topLabel[0]['year'];
			var calStartMonth = topLabel[0]['months'][0]['month'];
			var calStartDay = topLabel[0]['months'][0]['days'][0]['day'];

			var calEndYear = topLabel[topLabel.length-1]['year'];
			var calEndMonth = topLabel[topLabel.length-1]['months'][topLabel[topLabel.length-1]['months'].length-1]['month'];
			var calEndDay = topLabel[topLabel.length-1]['months'][topLabel[topLabel.length-1]['months'].length-1]['days'][topLabel[topLabel.length-1]['months'][topLabel[topLabel.length-1]['months'].length-1]['days'].length-1]['day'];

			// 表示上の開始日より開始日が前の場合
			if((calStartYear > startYear) || (calStartYear == startYear && calStartMonth > startMonth) || (calStartYear == startYear && calStartMonth == startMonth && calStartDay > startDay)){
					// 開始日を表示上の開始日に設定
					startYear = calStartYear;
					startMonth = calStartMonth;
					startDay = calStartDay;
			}
			// 開始日が表示上の終了日より後の場合
			else if((calEndYear < startYear) || (calEndYear == startYear && calEndMonth < startMonth) || (calEndYear == startYear && calEndMonth == startMonth && calEndDay < startDay)){
					// 表示しない
					continue;
			}
			startDate = new Date(startYear, startMonth - 1, startDay);

			// 終了日が表示上の開始日より前の場合
			if((calStartYear > endYear) || (calStartYear == endYear && calStartMonth > endMonth) || (calStartYear == endYear && calStartMonth == endMonth && calStartDay > endDay)){
				// 表示しない
				continue;
			}
			// 終了日が表示上の終了日より後の場合
			else if((calEndYear < endYear) || (calEndYear == endYear && calEndMonth < endMonth) || (calEndYear == endYear && calEndMonth == endMonth && calEndDay < endDay)){
				// 表示上の終了日を終了日に設定
				endYear = calEndYear;
				endMonth = calEndMonth;
				endDay = calEndDay+1;
			}
			endDate = new Date(endYear, endMonth - 1, endDay);

			// 色の決定
			var color = this.taskColor[jsonData['tasks'][i]['status']];
			if(color == null){
				color = this.taskColor[0];
			}

			// チャートの場合
			if(jsonData['tasks'][i]['type'] == 1){
				var diffDay=(endDate.getTime()-startDate.getTime())/(1000*60*60*24);
				var boxId = this.getDayBoxId(startYear, startMonth, startDay, jsonData['tasks'][i]['task_id']);
				$('#' + boxId).append('<div class="bar_pos"><div class="bar" style="background-color:' + color + ';width:' + (diffDay * dayWidth + diffDay) + 'px;height:' + barHeight + 'px;"></div></div>')
			}
			// マイルの場合
			else{
				var diffDay=(endDate.getTime()-startDate.getTime())/(1000*60*60*24);
				var boxId = this.getDayBoxId(startYear, startMonth, startDay, jsonData['tasks'][i]['task_id']);
				$('#' + boxId).append('<div class="bar_pos"><div class="stone" style="color:' + color + ';width:' + dayWidth + 'px;height:' + leftLabelTotalHeight + 'px;line-height:' + leftLabelTotalHeight + 'px;">◆</div></div>')
			}
		}

		// 線の描画
		var count=0;
		for(var i=0; i<topLabel.length; i++){
			for(var j=0; j<topLabel[i]['months'].length; j++){
				for(var k=0; k<topLabel[i]['months'][j]['days'].length; k++){
					if(topLabel[i]['months'][j]['days'][k]['day_of_week'] == 5){
						
						// app.debug("@" + topLabel[i]['months'][j]['month'] + "|" + topLabel[i]['months'][j]['days'][k]['day'])
						
						var canvas=document.getElementById(this.getCanvasId(topLabel[i]['year'], topLabel[i]['months'][j]['month'], topLabel[i]['months'][j]['days'][k]['day']));
						if(canvas != null && canvas.getContext){
							
							// app.debug("-----------")
							
							var cs = canvas.getContext('2d');
							cs.lineWidth = 1; 
							cs.strokeStyle="#b22";  
							
							var basePosX = (topLabel[i]['months'][j]['days'][k]['canvas_style']['left']) * -1 + (dayWidth / 2);
							
							cs.moveTo(basePosX,0);

							var yPos=0;
							for(var l=0; l<jsonData['tasks'].length; l++){
								
								yPos += (leftLabelHeight - barHeight) / 2;
								cs.lineTo(basePosX,yPos - 2);
								xPos = basePosX;

								if(jsonData['tasks'][l]['rate'][count] != null){
									xPos = basePosX + ((jsonData['tasks'][l]['rate'][count] - jsonData['tasks'][l]['base_rate'][count]) * jsonData['tasks'][l]['par_px'])
									xPos += (dayWidth / 2);
								}
								yPos += (barHeight / 2);
								cs.lineTo(xPos,yPos);

								yPos += (barHeight / 2);
								cs.lineTo(basePosX,yPos + 2);

								yPos +=  (leftLabelHeight - barHeight) / 2;
								cs.lineTo(basePosX,yPos);
							}
							cs.stroke(); 
						}
						count++;
					}
				}
			}
		}
		
		// 表示範囲内に当月が含まれていた場合、スクロールバーを移動する
		// 含まれていない場合、先頭に移動する
		if(moveCurrent){
			// カレンダー開始日を求める
			var calStartYear = topLabel[0]['year'];
			var calStartMonth = topLabel[0]['months'][0]['month'];
			var calStartDay = topLabel[0]['months'][0]['days'][0]['day'];
			var calStartDate = new Date(calStartYear, calStartMonth-1, calStartDay);
			// 当月先頭日を求める
			var workDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1)

			// 差を求める
			var diffDate = (workDate.getTime()-calStartDate.getTime())/(1000*60*60*24);
			this.target.get(0).scrollLeft=(diffDate * (dayWidth + 1))
		}
		else{
			this.target.get(0).scrollLeft=0;
		}


	}

	// キャンパスのスタイルを取得
	this.getCanvasStyle=function(year, month, day, index, topLabel, jsonData, dayWidth){
		var ret={width:20, left:-10};
		// 線の描画

		// 基準日を求める
		var baseDate = new Date(year, month-1, day);

		// カレンダー開始日を求める
		var calStartYear = topLabel[0]['year'];
		var calStartMonth = topLabel[0]['months'][0]['month'];
		var calStartDay = topLabel[0]['months'][0]['days'][0]['day'];
		var calStartDate = new Date(calStartYear, calStartMonth-1, calStartDay);

		// カレンダー終了日を求める
		var calEndYear = topLabel[topLabel.length-1]['year'];
		var calEndMonth = topLabel[topLabel.length-1]['months'][topLabel[topLabel.length-1]['months'].length-1]['month'];
		var calEndDay = topLabel[topLabel.length-1]['months'][topLabel[topLabel.length-1]['months'].length-1]['days'][topLabel[topLabel.length-1]['months'][topLabel[topLabel.length-1]['months'].length-1]['days'].length-1]['day'];
		var calEndDate = new Date(calEndYear, calEndMonth-1, calEndDay);

		// 最大の開始位置を求める
		var diffDate = (baseDate.getTime()-calStartDate.getTime())/(1000*60*60*24) * -1;
		var maxStartPx = diffDate * (dayWidth + 1)
		
		// 最大の終了位置を求める
		var diffDate = (calEndDate.getTime()-baseDate.getTime())/(1000*60*60*24);
		var maxEndPx = (diffDate + 1) * (dayWidth + 1)

		// 全進捗状況を参照して、最少と最大の幅を算出する
		// キャンパスのサイズ補正を行いスタート地点を求める
		var minPxPos=0;	// 最少の位置
		var maxPxPos=0;	// 最大の位置
		for(var l=0; l<jsonData['tasks'].length; l++){

			// マイルで開始日と終了日が有効なデータだけ線の描画を行う
			if(jsonData['tasks'][l]['type']==1 && 
					jsonData['tasks'][l]['start_date'] != null && jsonData['tasks'][l]['start_date'] != '' && 
					jsonData['tasks'][l]['end_date'] != null && jsonData['tasks'][l]['end_date'] != ''){

				var startYear = parseInt(jsonData['tasks'][l]['start_date'].substring(0, 4));
				var startMonth = parseInt(jsonData['tasks'][l]['start_date'].substring(4, 6));
				var startDay = parseInt(jsonData['tasks'][l]['start_date'].substring(6, 8));
				var startDate = new Date(startYear, (startMonth - 1), startDay);
				var endYear = parseInt(jsonData['tasks'][l]['end_date'].substring(0, 4));
				var endMonth = parseInt(jsonData['tasks'][l]['end_date'].substring(4, 6));
				var endDay = parseInt(jsonData['tasks'][l]['end_date'].substring(6, 8));
				var endDate = new Date(endYear, (endMonth-1), endDay);
				var rate = jsonData['tasks'][l]['rate'][index];

				// 全体の日数
				var totalDiffDate = (endDate.getTime()-startDate.getTime())/(1000*60*60*24) + 1;
				// 開始日から基準日までの日数
				var baseDiffDate = (baseDate.getTime()-startDate.getTime())/(1000*60*60*24) + 1;


				// 基準日時点の進捗率
				var baseRate = baseDiffDate/totalDiffDate*100


				// 1%あたりのpx数(保持)
				var parPx = ((totalDiffDate/100)*(dayWidth+1)) 
				jsonData['tasks'][l]['par_px']=parPx;
				jsonData['tasks'][l]['base_rate'][index]=baseRate;
	
				var pxPos=0;

				if(rate != null){
					var diffRate = jsonData['tasks'][l]['rate'][index]- baseRate;
					pxPos = parseInt(diffRate*parPx);
				}

	
				if(minPxPos>pxPos){
					minPxPos=pxPos;
				}
				if(maxPxPos<pxPos){
					maxPxPos=pxPos;
				}
			}
		}
		
		minPxPos = minPxPos - 20;
		maxPxPos = maxPxPos + 20;
		
		if(minPxPos <= maxStartPx){
			minPxPos = maxStartPx;
		}
		
		if(maxPxPos >= maxEndPx){
			maxPxPos = maxEndPx;
		}

		width=maxPxPos-minPxPos;


		
		ret={width:width, left:minPxPos};
		return ret;
	}


	this.getDayBoxId=function(year, month, day, lineId){
		return GANTTCHART_PREFIX + '_' + (year + ("0"+(month)).slice(-2) + ("0"+(day)).slice(-2)) + '_' + lineId;
	}

	this.getCanvasId=function(year, month, day){
		return CANVAS_PREFIX + '_' + (year + ("0"+(month)).slice(-2) + ("0"+(day)).slice(-2));
	}
	this.createCalendar=function(start, end){

		startYear = parseInt(start.substring(0, 4));
		startMonth = parseInt(start.substring(4, 6));
		endYear = parseInt(end.substring(0, 4));
		endMonth = parseInt(end.substring(4, 6));
		
		monthCount=0;
		topLabel=[];
		for(var i=startYear; i<=endYear; i++){
			var _j = {year:i,months:[]}
			topLabel.push(_j);
			
			if(startYear==endYear){
				for(var j=startMonth; j<=endMonth; j++){
					this.createCalendarDate(_j, i, j);
				}
			}
			else if(i==endYear){
				for(var j=1; j<=endMonth; j++){
					this.createCalendarDate(_j, i, j);
				}
			}
			else if(i==startYear){
				for(var j=startMonth; j<=12; j++){
					this.createCalendarDate(_j, i, j);
				}
			}
			else {
				for(var j=1; j<=12; j++){
					this.createCalendarDate(_j, i, j);
				}
			}
		}

		return topLabel;
	}
	this.createCalendarDate=function(jyear, year, month){
		var _j = {month:month,days:[]}
		jyear.months.push(_j);
		
		var date = new Date(year, month - 1, 1);
		for(var i=0;;i++){
			var time = date.getTime() + (i*86400000);
			var _data = new Date();
			_data.setTime(time);
			if((_data.getMonth()+1) != month){
					break;
			}
			else{
				 _j.days.push({day:_data.getDate(), day_of_week:_data.getDay()});
			}
		}
	}
	this.getTotalDayCount=function(topLabel){
		var count=0
		for(var i=0; i<topLabel.length; i++){
			for(var j=0; j<topLabel[i].months.length; j++){
				for(var k=0; k<topLabel[i].months[j].days.length; k++){
					count++;
				}
			}
		}
		return count;
	}
	
		this.getYearDayCount=function(topLabel, year){
		var count=0
		for(var i=0; i<topLabel.length; i++){
			if(topLabel[i].year == year){
				for(var j=0; j<topLabel[i].months.length; j++){
					for(var k=0; k<topLabel[i].months[j].days.length; k++){
						count++;
					}
				}
			}
		}
		return count;
	}

		this.getMonthDayCount=function(topLabel, year, month){
		var count=0
		for(var i=0; i<topLabel.length; i++){
			if(topLabel[i].year == year){
				for(var j=0; j<topLabel[i].months.length; j++){
					if(topLabel[i].months[j].month == month){
						for(var k=0; k<topLabel[i].months[j].days.length; k++){
							count++;
						}
					}
				}
			}
		}
		return count;
	}

	////////////////////////////////////////////////
	// 画像を削除する
	this.reset=function(){
		var _self=this;
		if(_self.unloadaction != null){
			new _self.unloadaction();
		}
		this.target.empty();
	}

	////////////////////////////////////////////////
	// データ取得条件の設定
	this.setCondition=function(condition){
		this.condition = condition;
	}
	
		////////////////////////////////////////////////
	// 検索条件の取得
	this.getCondition=function(condition){
		return this.condition;
	}
}
Rainy3Ganttchart.prototype = new Rainy3Base();
rainy3.rainy3ObjectList.push({key:'r3ganttchart',	_class:Rainy3Ganttchart});


