/****************************************************************
 * フォームウィンドウオブジェクト
 ****************************************************************/
var Rainy3BoisChildForm = function(id){

	var FORM_ID_SUFFIX = "_dfsjasds";
	this.loadurl = null;
	this.loadaction = null;
	this.unloadaction = null;
	this.jqTable = null;
	this.condition = {};
	this.defaultData = {};
	this.form=null;

	////////////////////////////////////////////////
	// 初期化
	this.init(this, 'form');

	////////////////////////////////////////////////
	// formの描画
	this.addContent=function(id, jqTarget, contentInfo){

		var _self=this;
		var form = null;
		eval(contentInfo);


		if(form == null){
			alert('Please define "var form={}"');
		}
		else{
			this.formData = form;
			this.loadurl = form['url'];
			this.loadaction = form['loadaction'];
			this.unloadaction = form['unloadaction'];

			// 初期検索条件
			if(this.formData['initcondition'] != null){
				_self.condition=this.formData['initcondition'];
			}
			var totalWidth = parseInt(form['label_width']) + parseInt(form['data_width']) + parseInt(form['example_width']);

			this.jqForm = $('<form name="' + this.id + FORM_ID_SUFFIX + '" id="' + this.id + FORM_ID_SUFFIX + '"></form>').appendTo(jqTarget);
			this.jqTable = $('<table cellspacing="0" cellpadding="0" border="0" width="' + (totalWidth * 2) + '" class="form_table" style="float:left;"></table>').appendTo(this.jqForm);


			var html = '';

			for(var i=0; i<form['forms'].length; i++){
				if(form['forms'][i]['plural'] != null && form['forms'][i]['plural']){
					if(form['forms'][i]['groupname'] == null || form['forms'][i]['groupname'] == ''){
						alert("Please specify groupname.");
						return;
					}
				}

				html = '<tr id=' + this.getObjectId(form['forms'][i]['id']);
				if(i == 0){
					html += ' class="ruck_block" ';
				}
				html += '>';
				html += '</tr>';
				var trObj = $(html).appendTo(this.jqTable);

				html = '<th width="' + form['label_width'] + '">';
				html += form['forms'][i]['label'];
				html += '</th>';
				var thObj = $(html).appendTo(trObj);


				html = '<td width="' + form['data_width'] + '">';
				html += '</td>';
				var tdObj = $(html).appendTo(trObj);

				html = '';
				html += '<span title="' + form['forms'][i]['id'] + '" class="r3form_message"></span>';
				html += form['forms'][i]['html'];
				var inputObj = $(html).appendTo(tdObj);
				
				if(i == 0){
					var thObj = $('<th class="ruck_block" rowspan="' + form['forms'].length + '" width="' + form['label_width'] + '">ラック選択</th>').appendTo(trObj);
					var tdObj = $('<td class="ruck_block" rowspan="' + form['forms'].length + '" width="' + form['data_width'] + '" style="vertical-align:top;"></td>').appendTo(trObj);
					$('<span title="ruck" class="r3form_message"></span>').appendTo(tdObj);
					var html = '<table cellspacing="0" cellpadding="0" border="0" class="power_condition" width="280">'

/*
					for(var i=0; i< 1; i++){
					


					html += '<tr>';
					html += '<th width="80">';
					html += '第一サイト';
					html += '</th>';
					html += '<td>';
					html += '<ul class="li_width75">';
					html += '<li><label><input type="checkbox" name="floor1" value="3" class="ruck_input"><span>3F</span></label></li>';
					html += '<li><label><input type="checkbox" name="floor1" value="4" class="ruck_input"><span>4F</span></label></li>';
					html += '<li><label><input type="checkbox" name="floor1" value="5" class="ruck_input"><span>5F</span></label></li>';
					html += '<li><label><input type="checkbox" name="floor1" value="15" class="ruck_input"><span>15F</span></label></li>';
					html += '</ul>';
					html += '</td>';
					html += '</tr>';
					}
*/
					html += '</table>';
					
					html += '<div class="power_select" style="height:200px;overflow:auto;">';
					html += '<ul>';
					html += '<li><label><input type="checkbox" name="power" value="1"><span>Ad-05</span></label></li>';
					html += '<li><label><input type="checkbox" name="power" value="2"><span>Ad-05</span></label></li>';
					html += '<li><label><input type="checkbox" name="power" value="3"><span>Ad-05</span></label></li>';
					html += '<li><label><input type="checkbox" name="power" value="4"><span>Ad-05</span></label></li>';
					html += '</ul>';
					html += '</div>';
					html += '<a href="javascript:void(0);" class="power_select_2">SELECT ALL</a>';

					
					$(html).appendTo(tdObj);
				}
			}
			this.bindFocusAction();
			this.defaultData = app._formParam2jsonData(this.jqForm, true);
		}
	}

	////////////////////////////////////////////////
	// フォームデータの読み込み
	this.load=function(){
		var _self = this;

		var dataMap = {};
		if(this.condition != null){
			for(var i in this.condition){
				dataMap[i] = this.condition[i];
			}
		}

		// HTTPリクエスト
		rainy3.request(this.loadurl, dataMap, false, function(head, body){
			// 成功時のアクション

			// エラーメッセージをクリア
			$('.r3form_message p').remove();

			// 追加した要素を削除
			_self.resetPluralFields();

			// xmlで帰ってきた値をjsonデータに変換してフォームにセット
			var pluralList = [];
			var jsonData = _self.xml2formData(body, pluralList);

			// 複数要素がある場合には、数文要素を増やす
			_self.createPluralFields(pluralList);

			// 返ってきた値をセット
			app._jsonData2formParam(jsonData, _self.jqForm);

			// ロードアクションを実行
			if(_self.loadaction != null){
				new _self.loadaction();
			}
		},
		function(){},
		function(){
			_self.protection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, true)
		},
		function(head, body){
			_self.releaseProtection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, _self.jqMessageBox, _self.jqMessageBoxShadow);
		});
	}

	////////////////////////////////////////////////
	// 複数入力フィールドを指定数分増やす
	this.createPluralFields=function(pluralList){
		var _self = this;

		for(var i in pluralList){ 

			for(var j=0; j<_self.formData['forms'].length; j++){
				if(i==_self.formData['forms'][j]['groupname']){
					
					for(var k=1; k<pluralList[i]; k++){
						_self.addInputFields(_self.formData['forms'][j]['id'])
					}
				}
			}
		}
	}

	////////////////////////////////////////////////
	// 複数入力フィールドをリセット
	this.resetPluralFields=function(){
		this.jqForm.find('.pluralboxnext').remove();
		for(var i=0; i<this.formData['forms'].length; i++){
			if(this.formData['forms'][i]['pluralcount'] != null){
				this.formData['forms'][i]['pluralcount'] = 1;
			}
		}

		// 先頭のフィールドがない場合は追加
		for(var j=0; j<this.formData['forms'].length; j++){
			if(this.formData['forms'][j]['groupname'] != null){
				// 対象のフィールドが0件の場合は、デフォルトを追加する
				if(this.formData['forms'][j]['pluralhandle'].parent().children('.pluralbox').length == 0){
					var ret = this.addFirstInputFields(this.formData['forms'][j]['id'])
					this.formData['forms'][j]['pluralbox'] = ret;
				}
			}
		}
	}


	////////////////////////////////////////////////
	// 入力を初期状態に戻す
	this.reset=function(){

		// エラーメッセージをクリア
		$('.r3form_message p').remove();

		// 追加した要素を削除
		this.resetPluralFields();


		// デフォルト値をセット
		app._jsonData2formParam(this.defaultData, this.jqForm);

		if(this.unloadaction != null){
			new this.unloadaction();
		}
	}

	////////////////////////////////////////////////
	// データ取得条件の設定
	this.setCondition=function(condition){
		this.condition = condition;
	}

	////////////////////////////////////////////////
	// 検索条件の取得
	this.getCondition=function(condition){
		return this.condition;
	}

	////////////////////////////////////////////////
	// リクエストURLの設定
	this.setUrl=function(url){
		this.loadurl = url;
	}

	////////////////////////////////////////////////
	// データの送信
	this.send=function(url, afterFunction){
		var _self = this;

		// フォームデータを取得
		var dataMap = app._formParam2jsonData(this.jqForm, false);
		// HTTPリクエスト
		rainy3.request(url, dataMap, false, function(head, body){
			// 成功

			// メッセージをクリア
			$('.r3form_message p').remove();

			// 追加した要素を削除
			_self.resetPluralFields();

			// xmlで帰ってきた値をjsonデータに変換してフォームにセット
			var pluralList = [];
			var jsonData = _self.xml2formData(body, pluralList);

			// 複数要素がある場合には、数文要素を増やす
			_self.createPluralFields(pluralList);

			// 返ってきた値をセット（エラーの場合はセットしない）
			app._jsonData2formParam(jsonData, _self.jqForm);

			// 正常終了のメッセージを表示（1秒くらい）
			
			
			// 処理後のメソッド実行
			if(afterFunction != null){
				new afterFunction()
			}
		},
		function(head, body){
			// ユーザーエラー

			// メッセージをクリア
			$('.r3form_message p').remove();

			// エラーメッセージを表示
			var jsonData = _self.xml2messageData(body);

			// 返ってきた値をセット
			_self.jsonData2message($('#' + _self.id + FORM_ID_SUFFIX), jsonData);

			// フォーカス
			_self.messageFocus(body);

			// xmlで帰ってきた値をjsonデータに変換してフォームにセット
//			var jsonData = _self.xml2formData(body);

			// 返ってきた値をセット（エラーの場合はセットしない）
//			app._jsonData2formParam(jsonData, _self.jqForm);

		},
		function(){
			_self.protection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, true)
		},
		function(head, body){

			var result = head.find("result").text();
			var message = head.find("message").text();
			if(message == null || message == ''){
				message = RAINY3MESSAGE.MESSAGE_COMPLATE;
			}
			if(result == '1'){
				_self.showCompleateMessage( _self.jqContentLoading, _self.jqMessageBox, _self.jqMessageBoxShadow, message);


				setTimeout(function(){
					_self.releaseProtection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, _self.jqMessageBox, _self.jqMessageBoxShadow);
				}, 1000);

			}
			else{
				_self.releaseProtection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, _self.jqMessageBox, _self.jqMessageBoxShadow);
			}
		});
	}


	////////////////////////////////////////////////
	// 返却されたXMLからjsonデータ（入力）を生成する
	this.xml2formData=function(body, pluralList){
		var _self = this;
		var jsonData = {};
		var fields = body.find('fields');
		fields.children('field').each(
			function(i){
				var field = $(this);
				var id = field.attr('id');
				field.find('value').each(function(j){
					var value = $(this);
					_self.setJsonData(jsonData, id, value.text());
				});
			}
		);

		// 複数要素の処理を行う
		fields.children('plural').each(
			function(i){
				var plural = $(this);
				var groupname = plural.attr('id');

				plural.children('datablock').each(
					function(j){
						var datablock = $(this);

						if(pluralList[groupname] == null){
							pluralList[groupname] = 1;
						}
						else{
							pluralList[groupname]++;
						}

						datablock.children('field').each(
							function(k){
								var field = $(this);
								var id = field.attr('id');

								if(rainy3.RAILS){
									id = groupname + "[" + j + "]" + id;
								}
								else{
									id = groupname + "[" + j + "]." + id;
								}
								

								field.find('value').each(function(l){
									var value = $(this);
									_self.setJsonData(jsonData, id, value.text());
								});
							}
						)
					}
				);
			}
		);

		return jsonData;
	}
	this.setJsonData=function(jsonData, id, value){
		if(jsonData[id] == null){
			jsonData[id]=value;
		}
		else{
			if($.type(jsonData[id]) == 'array'){
				jsonData[id].push(value);
			}
			else{
				var bk = jsonData[id];
				jsonData[id] = [];
				jsonData[id].push(bk);
				jsonData[id].push(value);
			}
		}
	}

	////////////////////////////////////////////////
	// 返却されたXMLからjsonデータ（メッセージ）を生成する
	this.xml2messageData=function(body){
		var jsonData = {};
		body.find('message').each(
			function(i){
				var message = $(this);
				var id = message.attr('id');
				var message = message.text();
				jsonData[id]=message;
			}
		);
		return jsonData;
	}

	////////////////////////////////////////////////
	// メッセージをセットする
	this.jsonData2message=function(jqObj, jsonData){
		if(jqObj[0].nodeName.toUpperCase() == 'SPAN' && jqObj.hasClass('r3form_message')){
			var message = jsonData[jqObj.attr('title')];
			if(message != null && message != ''){
				jqObj.html('<p>' + message + '</p>');
			}
		}
		else{
			var children = jqObj.children();
			for(var i=0; i<children.length; i++){
				this.jsonData2message($(children[i]), jsonData);
			}
		}
	}

	////////////////////////////////////////////////
	// フォーカスする
	this.messageFocus=function(body){
		var _self = this;
		var focus = body.find('focus');
		if(focus.length == 1){
			_self._messageFocus($('#' + _self.id + FORM_ID_SUFFIX), focus.text());
		}
	}
	this._messageFocus=function(jqObj, focusName){
		if(jqObj[0].nodeName.toUpperCase() == 'INPUT' || jqObj[0].nodeName.toUpperCase() == 'SELECT' || jqObj[0].nodeName.toUpperCase() == 'TEXTAREA'){
			if(jqObj.attr('name') == focusName){
				jqObj.focus();
			}
		}
		else{
			var children = jqObj.children();
			for(var i=0; i<children.length; i++){
				this._messageFocus($(children[i]), focusName);
			}
		}
	}

	////////////////////////////////////////////////
	// 入力フィールドにフォーカスした場合に、背景を変更する
	this.bindFocusAction=function(){

		var target = '#' + this.id + FORM_ID_SUFFIX + ' input';
		target += ',';
		target += '#' + this.id + FORM_ID_SUFFIX + ' select'
		target += ',';
		target += '#' + this.id + FORM_ID_SUFFIX + ' textarea'

		$(target).focus(function(e){
			
			if(!$(e.target).hasClass('ruck_input')){
				
			var parentObj = app.getParentJQueryObjectByTagName($(e.target), 'tr');
			if(parentObj != null){
				if(parentObj.hasClass('ruck_block')){

					if(e.target.name == 'bois_id'){
						var chlist = parentObj.children('td,th');
						for(var i=0; i<chlist.length; i++){
							var ch = chlist.eq(i);
							if(!ch.hasClass('ruck_block')){
								ch.addClass("selected");
							}
						}
					}
					else{
/*
						var chlist = parentObj.children('td,th');
						for(var i=0; i<chlist.length; i++){
							var ch = chlist.eq(i);
							if(ch.hasClass('ruck_block')){
								ch.addClass("selected");
							}
						}
*/
					}

				}
				else{
					parentObj.children('td,th').addClass("selected");
				}
			}
				
				
				
				
			}
			
			
		});

		$(target).blur(function(e){
			var parentObj = app.getParentJQueryObjectByTagName($(e.target), 'tr');
			if(parentObj != null){
				parentObj.children('td,th').removeClass("selected");
			}
		});
	}

	////////////////////////////////////////////////
	// 入力フィールドにフォーカスした場合に、背景を変更する（Jqueryオブジェクト指定）
	this.bindFocusAction2=function(jqObj){

		var target = 'input';
		target += ',';
		target += 'select'
		target += ',';
		target += 'textarea'

		jqObj.find(target).focus(function(e){
			var parentObj = app.getParentJQueryObjectByTagName($(e.target), 'tr');
			if(parentObj != null){
				parentObj.children('td,th').addClass("selected");
			}
		});

		jqObj.find(target).blur(function(e){
			var parentObj = app.getParentJQueryObjectByTagName($(e.target), 'tr');
			if(parentObj != null){
				parentObj.children('td,th').removeClass("selected");
			}
		});
	}

	////////////////////////////////////////////////
	// ロード後の処理を定義する
	this.setLoadAction=function(){
	}

	////////////////////////////////////////////////
	// ロード直前の処理を定義する
	this.setPreLoadAction=function(){
	}
}
Rainy3BoisChildForm.prototype = new Rainy3Base();
rainy3.rainy3ObjectList.unshift({key:'r3form', _class:Rainy3BoisChildForm});