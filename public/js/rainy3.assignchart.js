/****************************************************************
 * アサイン状況オブジェクト
 ****************************************************************/
var Rainy3Assignchart = function(id){

	var GANTTCHART_PREFIX = "b21d0odi_";
	var CANVAS_PREFIX="ucip8aoad1_";
	this.ganttchartData=null;
	this.loadurl=null;
	this.loadaction=null;
	this.unloadaction=null;
	this.target=null;
	this.matterColor={
		// その他
		0:'#fff',
		// 営業中
		1:'#bc8f8f',
		// 仮受注
		2:'#2b8854',
		// 受注
		3:'#66e',
		// 完了
		4:'#ffa500',
		// 棄却
		5:'#888'
	}


	////////////////////////////////////////////////
	// 初期化
	this.init(this, 'assignchart');

	////////////////////////////////////////////////
	// イメージの描画
	this.addContent=function(id, jqTarget, contentInfo){
		var _self=this;

		eval(contentInfo);

		if(assignchart == null){
			alert('Please define "var assignchart={}"');
		}
		else{
			this.assignchartData = assignchart;
			this.loadurl = this.assignchartData['url'];
			this.loadaction = this.assignchartData['loadaction'];
			this.unloadaction = this.assignchartData['unloadaction'];
			// 初期検索条件
			if(this.assignchartData['initcondition'] != null){
				this.condition=this.assignchartData['initcondition'];
			}
			this.target=jqTarget;
			_self.load();
		}
	}

	////////////////////////////////////////////////
	// アサイン情報の読み込み
	this.load=function(){
		var _self = this;
		var _url = this.loadurl;
		var first=true;
		var dataMap = {};
		this.direction='asc'

		// 検索条件にpage,sort,directionは利用不可
		dataMap={
				page:_self.page,
				sort:_self.sort,
				direction:_self.direction
		}

		if(this.condition != null){
			for(var i in this.condition){
				dataMap[i] = this.condition[i];
			}
		}

		this.target.empty();
		// HTTPリクエスト
		rainy3.request(this.loadurl, dataMap, false, function(head, body){
			
			// 成功時のアクション
			var jsonData={};

			jsonData['start_date']=body.find('term_start').text();
			jsonData['end_date']=body.find('term_end').text();
			jsonData['users']=[];
			var users = body.find('rows');
			users.children('row').each(
				function(i){
					var data={};
					data['user_id'] = parseInt($(this).attr('id'));
					data['user_name'] = $(this).find('name').text();
					var matters = data['matters'] = [];
					jsonData['users'].push(data);

					var _matters = $(this).find('matters');
					_matters.children('matter').each(
						function(j){
							var data2={}
							data2['matter_id'] = parseInt($(this).find('matter_id').text());
							data2['matter_title'] = $(this).find('matter_title').text();
							data2['matter_status'] = parseInt($(this).find('matter_status').text());
							var terms = data2['term'] = [];
							matters.push(data2);
	
							var _terms = $(this).find('terms');
							_terms.children('term').each(
								function(k){
									var data3={}
									data3['start_date'] = $(this).find('start_date').text();
									data3['end_date'] = $(this).find('end_date').text();
									terms.push(data3);	
								}
							);
						}
					);
				}
			);
			_self.draw(jsonData);

			// ページ情報の取得
			var pageNum = parseInt(head.find('page_num').text());
			var pageSize = parseInt(head.find('page_size').text());
			var totalSize = parseInt(head.find('total_size').text());

			var dataCount = ((pageNum - 1) * pageSize) + 1;

			// ページ番号を更新
			_self.page = parseInt(pageNum);
			if(_self.page == 0){
				_self.page = 1;
			}
			// フッターがRainy3PagingFooterの場合、ページング表示を行う
			if(_self.footer != null && _self.footer.type=='paging_footer'){
				_self.footer.pageNum = pageNum;
				_self.footer.pageSize = pageSize;
				_self.footer.totalSize = totalSize;
				_self.footer.direction = _self.direction;

				_self.footer.clickAction=function(pageNum){
					_self.page=pageNum;
					_self.load();
				}
				_self.footer.showPageInfo();
			}

			if(_self.loadaction!=null){
				new _self.loadaction();
			}
		},
		function(){},
		function(){
			_self.protection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, true)
		},
		function(head, body){
			_self.releaseProtection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, _self.jqMessageBox, _self.jqMessageBoxShadow);
		});
	}

	// アサイン情報の描画
	this.draw=function(jsonData){
		var _self=this;
		var today = new Date();
		var topLabel = this.createCalendar(jsonData['start_date'], jsonData['end_date']);
		var currentDate = new Date();
		var moveCurrent=false;

		var dayWidth=5;
		var userNameWidth=100;
		var matterInfoWidth=180;
		var rateWidth=0;
		var leftLabelWidth=userNameWidth+1+matterInfoWidth+1 // +baseRateWidth+1+rateWidth+1;
		var totalTableWidth=leftLabelWidth+(this.getTotalDayCount(topLabel)* dayWidth) + this.getTotalDayCount(topLabel);
		var leftLabelHeight=40;
		var leftLabelTotalHeight=leftLabelHeight;
		var barHeight=10;
		var topLabelHeight=20;
		var topLabelTotal1 = topLabelHeight * 2 + 2 - 1;
		var topLabelTotal2 = topLabelHeight * 3 + 3 - 1;
		var jqDiv = $('<div></div>').appendTo(_self.target);

		var jqTable = $('<table cellspacing="0" cellpadding="0" border="0" width="' + (totalTableWidth) +  '" class="ganttchart_table"></table>').appendTo(jqDiv);
		var topHtml = '<table cellspacing="0" cellpadding="0" border="0" width="' + (totalTableWidth) +  '" class="ganttchart_table">';
		var leftHtml = '<table cellspacing="0" cellpadding="0" border="0" width="' + (leftLabelWidth) +  '" class="ganttchart_table">'

		var html ='<tr><td id="fix_scale" rowspan="2" width="' + (leftLabelWidth - 1) + '" align="left" vAlign="top" style="height:' + topLabelTotal1 + 'px;padding:0px;"><div class="floating_pos"><div class="left"></div><div class="top"></div><div class="fix head"></div></div></td>';
		topHtml += '<tr><td rowspan="3" width="' + (leftLabelWidth - 1) + '" align="left" vAlign="top" style="height:' + topLabelTotal2 + 'px;padding:0px;"></td>';

		leftHtml +='<tr>';
		leftHtml +='<td id="left_fix_top" width="' + (userNameWidth) + '" align="left" vAlign="top" style="padding:0px;"></td>';
		leftHtml +='<td width="' + (matterInfoWidth) + '" align="left" vAlign="top" style="padding:0px;"></td>';

		for(var i=0; i<topLabel.length; i++){
			html += '<td colspan="' + this.getYearDayCount(topLabel, topLabel[i]['year']) + '" width="' + (this.getYearDayCount(topLabel, topLabel[i]['year']) * dayWidth + this.getYearDayCount(topLabel, topLabel[i]['year']) - 1) + '" style="height:' + topLabelHeight + 'px;padding:0px;">' + topLabel[i]['year'] + '年</td>'
			topHtml += '<td class="head"  width="' + (this.getYearDayCount(topLabel, topLabel[i]['year']) * dayWidth + this.getYearDayCount(topLabel, topLabel[i]['year']) - 1) + '" style="height:' + topLabelHeight + 'px;padding:0px;" colspan="' + this.getYearDayCount(topLabel, topLabel[i]['year']) + '">' + topLabel[i]['year'] + '年</td>';
		}
		html += '</tr>'
		topHtml += '</tr>';

		leftHtml += '</tr>';

		html += '<tr>'
		topHtml += '<tr>';
		for(var i=0; i<topLabel.length; i++){
			for(var j=0; j<topLabel[i]['months'].length; j++){
				html += '<td colspan="' + this.getMonthDayCount(topLabel, topLabel[i]['year'], topLabel[i]['months'][j]['month']) + '" width="' + (this.getMonthDayCount(topLabel, topLabel[i]['year'], topLabel[i]['months'][j]['month']) * dayWidth + this.getMonthDayCount(topLabel, topLabel[i]['year'], topLabel[i]['months'][j]['month']) - 1) + '" style="height:' + topLabelHeight + 'px;padding:0px;">' + topLabel[i]['months'][j]['month'] + '月</td>'
				topHtml += '<td class="head" style="height:' + topLabelHeight + 'px;padding:0px;" width="' + (this.getMonthDayCount(topLabel, topLabel[i]['year'], topLabel[i]['months'][j]['month']) * dayWidth + this.getMonthDayCount(topLabel, topLabel[i]['year'], topLabel[i]['months'][j]['month']) - 1) + '" colspan="' + this.getMonthDayCount(topLabel, topLabel[i]['year'], topLabel[i]['months'][j]['month']) + '" width="' + (this.getMonthDayCount(topLabel, topLabel[i]['year'], topLabel[i]['months'][j]['month']) * dayWidth + this.getMonthDayCount(topLabel, topLabel[i]['year'], topLabel[i]['months'][j]['month']) ) + '">' + topLabel[i]['months'][j]['month'] + '月</td>';
			}
		}

		html += '</tr>'
		topHtml += '</tr>';

		topHtml += '<tr>';
		for(var i=0; i<topLabel.length; i++){
			for(var j=0; j<topLabel[i]['months'].length; j++){
				
				if(currentDate.getFullYear() == topLabel[i]['year'] && (currentDate.getMonth() + 1) == topLabel[i]['months'][j]['month']){
					moveCurrent=true;
				}
				
				for(var k=0; k<topLabel[i]['months'][j]['days'].length; k++){
					topHtml += '<td class="';
					
					if(today.getFullYear() == topLabel[i]['year'] && today.getMonth() + 1 == topLabel[i]['months'][j]['month'] && today.getDate() == topLabel[i]['months'][j]['days'][k]['day']){
							topHtml += 'current_day';
					}
					else if(topLabel[i]['months'][j]['days'][k]['day_of_week'] == 0){
						topHtml += 'sunday';
					}
					else if(topLabel[i]['months'][j]['days'][k]['day_of_week'] == 6){
						topHtml += 'saturday';
					}
					else{
						topHtml += 'head';
					}
					topHtml += '"width="' + dayWidth + '" style="height:' + topLabelHeight + 'px;padding:0px;">' + topLabel[i]['months'][j]['days'][k]['day'] + '</td>';
				}
			}
		}
		topHtml += '</tr>';
		topHtml += '</table>';

		// ユーザー領域の描画
		for(var i=0; i<jsonData['users'].length; i++){
			for(var j=0; j<jsonData['users'][i]['matters'].length; j++){
				html += '<tr>';
				html += '<td style="height:' + leftLabelTotalHeight + 'px;padding:0px;" height="' + leftLabelTotalHeight + '" class="day_box_under'
				if(jsonData['users'][i]['matters'].length-1 == j){
					html +=' table_end';
				}
				html += '">';
				html += '</td>';


			leftHtml += '<tr>';
			
			if(j ==0){
				leftHtml += '<td rowspan="' + jsonData['users'][i]['matters'].length + '" style="text-align:left;padding:0px;" class="day_box_under table_end">';
				leftHtml += '<div style="padding-left:2px;width:' + (userNameWidth -3) + 'px;line-height:' + leftLabelTotalHeight + 'px;height:100%;overflow:hidden;">';
				leftHtml += app.htmlEscape(jsonData['users'][i]['user_name'])
				leftHtml += '</div>';
				leftHtml += '</td>';
			}


			leftHtml += '<td align="left" vAlign="top" style="vertical-align:top;text-align:left;height:' + leftLabelTotalHeight + 'px;padding:0px;" height="' + leftLabelTotalHeight + '" class="day_box_under'
			if(jsonData['users'][i]['matters'].length-1 == j){
				leftHtml +=' table_end';
			}
			leftHtml += '">';

			leftHtml += '<div style="white-space: nowrap;padding-top:0px;padding-left:2px;width:' + (matterInfoWidth - 3) + 'px;line-height:' + (leftLabelTotalHeight/2) + 'px;height:100%;overflow:hidden;">';

			if(jsonData['users'][i]['matters'][j]['matter_id'] != 0){
				leftHtml += app.htmlEscape(jsonData['users'][i]['matters'][j]['matter_title'])
				var startDt=null;
				var endDt=null;
				if(jsonData['users'][i]['matters'][j]['term'].length > 0){
					for(var k=0; k<jsonData['users'][i]['matters'][j]['term'].length; k++){
						if(startDt == null || startDt > jsonData['users'][i]['matters'][j]['term'][k]['start_date']){
							startDt = jsonData['users'][i]['matters'][j]['term'][k]['start_date']
						}
						if(endDt == null || endDt < jsonData['users'][i]['matters'][j]['term'][k]['end_date']){
							endDt = jsonData['users'][i]['matters'][j]['term'][k]['end_date']
						}
					}
					var d = app.parseDateString(startDt);
					startDt = d.getFullYear() + "/" + ("0"+(d.getMonth()+1)).slice(-2) + "/" + ("0"+d.getDate()).slice(-2);

					if(endDt != '99991231'){
						var d = app.parseDateString(endDt);
						endDt = d.getFullYear() + "/" + ("0"+(d.getMonth()+1)).slice(-2) + "/" + ("0"+d.getDate()).slice(-2);
					}
					else{
						endDt='';
					}
				}
				leftHtml += '<p style="font-size:70%;">(' + startDt + '〜' + endDt + ')</p>';
			}

			leftHtml += '</div>';
			leftHtml += '</td>';

			for(var k=0; k<topLabel.length; k++){
				for(var l=0; l<topLabel[k]['months'].length; l++){
					for(var m=0; m<topLabel[k]['months'][l]['days'].length; m++){
							html += '<td valign="top" align="left" class="day_box day_box_under '

							if(topLabel[k]['months'][l]['days'][m]['day_of_week'] == 0){
								html +='sunday day_box_under ';
							}
							else if(topLabel[k]['months'][l]['days'][m]['day_of_week'] == 6){
								html +='saturday day_box_under ';
							}
						
							if(today.getFullYear() == topLabel[k]['year'] && today.getMonth() + 1 == topLabel[k]['months'][l]['month'] && today.getDate() == topLabel[k]['months'][l]['days'][m]['day']){
								html +=' current_day ';
							}

							if(jsonData['users'][i]['matters'].length-1 == j){
								html +='table_end';
							}
							html += '"width="' + dayWidth + '" id="' + _self.getDayBoxId(topLabel[k]['year'], topLabel[k]['months'][l]['month'], topLabel[k]['months'][l]['days'][m]['day'], jsonData['users'][i]['user_id'], jsonData['users'][i]['matters'][j]['matter_id']) + '">'
							html += '</td>'
						}
					}
				}
				html += '</tr>';
				leftHtml += '</tr>';
			}
		}
		leftHtml+='</table>';

		$(html).appendTo(jqTable);

		// 固定部分の描画
		var jqFixScale = $('#fix_scale');

		// 左側


		var leftMenu = $('div.floating_pos div.left');
		leftMenu.width(jqFixScale.width() + 1);
		leftMenu.height(leftLabelTotalHeight*4 + jsonData['users'].length + (topLabelTotal1 + 1))

		leftMenu.css('background-color', '#fff');
		
		leftMenu.append(leftHtml);
		
		$('#left_fix_top').height(topLabelTotal1);

		// 上側
		var topMenu = $('div.floating_pos div.top');

		topMenu.height(topLabelTotal1 + 1);
		topMenu.width(totalTableWidth+1);
		topMenu.append(topHtml);

		// 左上
		var fixMenu = $('div.floating_pos div.fix');
		fixMenu.height(topLabelTotal1);
		fixMenu.width(jqFixScale.width());

		var html = '<div><p><span style="color:'+ this.matterColor[1] + '">■</span>営業中</p>';
		html += '<p><span style="color:'+ this.matterColor[2] + '">■</span>仮受注</p></div>'

		html += '<div><p><span style="color:'+ this.matterColor[3] + '">■</span>受注</p>'
		html += '<p><span style="color:'+ this.matterColor[4] + '">■</span>完了</p></div>'
		html += '<div><p><span style="color:'+ this.matterColor[5] + '">■</span>棄却</p></div>'
		fixMenu.html(html);


		// スクロール固定
		_self.target.scroll(function(e){
			leftMenu.css('top', e.target.scrollTop + 'px')
			leftMenu.css('left', e.target.scrollLeft + 'px')
			topMenu.css('top', e.target.scrollTop + 'px')
			fixMenu.css('left', e.target.scrollLeft + 'px')
			fixMenu.css('top', e.target.scrollTop + 'px')
		})

		for(var i=0; i<jsonData['users'].length; i++){
			for(var j=0; j<jsonData['users'][i]['matters'].length; j++){
				if(jsonData['users'][i]['matters'][j]['matter_id'] != 0){

					for(var k=0; k<jsonData['users'][i]['matters'][j]['term'].length; k++){


						var start = jsonData['users'][i]['matters'][j]['term'][k]['start_date'];
						var end = jsonData['users'][i]['matters'][j]['term'][k]['end_date'];
						var startYear = parseInt(start.substring(0, 4));
						var startMonth = parseInt(start.substring(4, 6));
						var startDay = parseInt(start.substring(6, 8));
						var endYear = parseInt(end.substring(0, 4));
						var endMonth = parseInt(end.substring(4, 6));
						var endDay = parseInt(end.substring(6, 8)) + 1;

						var calStartYear = topLabel[0]['year'];
						var calStartMonth = topLabel[0]['months'][0]['month'];
						var calStartDay = topLabel[0]['months'][0]['days'][0]['day'];

						var calEndYear = topLabel[topLabel.length-1]['year'];
						var calEndMonth = topLabel[topLabel.length-1]['months'][topLabel[topLabel.length-1]['months'].length-1]['month'];
						var calEndDay = topLabel[topLabel.length-1]['months'][topLabel[topLabel.length-1]['months'].length-1]['days'][topLabel[topLabel.length-1]['months'][topLabel[topLabel.length-1]['months'].length-1]['days'].length-1]['day'];

						// 表示上の開始日より開始日が前の場合
						if((calStartYear > startYear) || (calStartYear == startYear && calStartMonth > startMonth) || (calStartYear == startYear && calStartMonth == startMonth && calStartDay > startDay)){
								// 開始日を表示上の開始日に設定
								startYear = calStartYear;
								startMonth = calStartMonth;
								startDay = calStartDay;
						}
						// 開始日が表示上の終了日より後の場合
						else if((calEndYear < startYear) || (calEndYear == startYear && calEndMonth < startMonth) || (calEndYear == startYear && calEndMonth == startMonth && calEndDay < startDay)){
								// 表示しない
								continue;
						}
						startDate = new Date(startYear, startMonth - 1, startDay);
	
						// 終了日が表示上の開始日より前の場合
						if((calStartYear > endYear) || (calStartYear == endYear && calStartMonth > endMonth) || (calStartYear == endYear && calStartMonth == endMonth && calStartDay > endDay)){
							// 表示しない
							continue;
						}
						// 終了日が表示上の終了日より後の場合
						else if((calEndYear < endYear) || (calEndYear == endYear && calEndMonth < endMonth) || (calEndYear == endYear && calEndMonth == endMonth && calEndDay < endDay)){
							// 表示上の終了日を終了日に設定
							endYear = calEndYear;
							endMonth = calEndMonth;
							endDay = calEndDay+1;
						}
						endDate = new Date(endYear, endMonth - 1, endDay);

						var color=_self.matterColor[jsonData['users'][i]['matters'][j]['matter_status']]
						var diffDay=(endDate.getTime()-startDate.getTime())/(1000*60*60*24);


						var boxId = this.getDayBoxId(startYear, startMonth, startDay, jsonData['users'][i]['user_id'], jsonData['users'][i]['matters'][j]['matter_id']);
						$('#' + boxId).append('<div class="bar_pos"><div class="bar" style="background-color:' + color + ';width:' + (diffDay * dayWidth + diffDay) + 'px;height:' + barHeight + 'px;"></div></div>')
					}
				}
			}
		}

		// 表示範囲内に当月が含まれていた場合、スクロールバーを移動する
		// 含まれていない場合、先頭に移動する
		// if(moveCurrent){
			// // カレンダー開始日を求める
			// var calStartYear = topLabel[0]['year'];
			// var calStartMonth = topLabel[0]['months'][0]['month'];
			// var calStartDay = topLabel[0]['months'][0]['days'][0]['day'];
			// var calStartDate = new Date(calStartYear, calStartMonth-1, calStartDay);
			// // 当月先頭日を求める
			// var workDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1)
// 
			// // 差を求める
			// var diffDate = (workDate.getTime()-calStartDate.getTime())/(1000*60*60*24);
			// this.target.get(0).scrollLeft=(diffDate * (dayWidth + 1))
		// }
		// else{
			this.target.get(0).scrollLeft=0;
//		}
	}

	this.getDayBoxId=function(year, month, day, userId, lineId){
		return GANTTCHART_PREFIX + '_' + (year + ("0"+(month)).slice(-2) + ("0"+(day)).slice(-2)) + '_' + userId + '_' + lineId;
	}

	this.createCalendar=function(start, end){

		startYear = parseInt(start.substring(0, 4));
		startMonth = parseInt(start.substring(4, 6));
		endYear = parseInt(end.substring(0, 4));
		endMonth = parseInt(end.substring(4, 6));
		
		monthCount=0;
		topLabel=[];
		for(var i=startYear; i<=endYear; i++){
			var _j = {year:i,months:[]}
			topLabel.push(_j);
			
			if(startYear==endYear){
				for(var j=startMonth; j<=endMonth; j++){
					this.createCalendarDate(_j, i, j);
				}
			}
			else if(i==endYear){
				for(var j=1; j<=endMonth; j++){
					this.createCalendarDate(_j, i, j);
				}
			}
			else if(i==startYear){
				for(var j=startMonth; j<=12; j++){
					this.createCalendarDate(_j, i, j);
				}
			}
			else {
				for(var j=1; j<=12; j++){
					this.createCalendarDate(_j, i, j);
				}
			}
		}

		return topLabel;
	}
	this.createCalendarDate=function(jyear, year, month){
		var _j = {month:month,days:[]}
		jyear.months.push(_j);
		
		var date = new Date(year, month - 1, 1);
		for(var i=0;;i++){
			var time = date.getTime() + (i*86400000);
			var _data = new Date();
			_data.setTime(time);
			if((_data.getMonth()+1) != month){
					break;
			}
			else{
				 _j.days.push({day:_data.getDate(), day_of_week:_data.getDay()});
			}
		}
	}
	this.getTotalDayCount=function(topLabel){
		var count=0
		for(var i=0; i<topLabel.length; i++){
			for(var j=0; j<topLabel[i].months.length; j++){
				for(var k=0; k<topLabel[i].months[j].days.length; k++){
					count++;
				}
			}
		}
		return count;
	}
	
	this.getYearDayCount=function(topLabel, year){
		var count=0
		for(var i=0; i<topLabel.length; i++){
			if(topLabel[i].year == year){
				for(var j=0; j<topLabel[i].months.length; j++){
					for(var k=0; k<topLabel[i].months[j].days.length; k++){
						count++;
					}
				}
			}
		}
		return count;
	}

		this.getMonthDayCount=function(topLabel, year, month){
		var count=0
		for(var i=0; i<topLabel.length; i++){
			if(topLabel[i].year == year){
				for(var j=0; j<topLabel[i].months.length; j++){
					if(topLabel[i].months[j].month == month){
						for(var k=0; k<topLabel[i].months[j].days.length; k++){
							count++;
						}
					}
				}
			}
		}
		return count;
	}

	////////////////////////////////////////////////
	// 画像を削除する
	this.reset=function(){
		var _self=this;
		if(_self.unloadaction != null){
			new _self.unloadaction();
		}
		this.target.empty();
	}

	////////////////////////////////////////////////
	// データ取得条件の設定
	this.setCondition=function(condition){
		this.condition = condition;
	}
	
		////////////////////////////////////////////////
	// 検索条件の取得
	this.getCondition=function(condition){
		return this.condition;
	}
}
Rainy3Assignchart.prototype = new Rainy3Base();
rainy3.rainy3ObjectList.push({key:'r3assignchart',	_class:Rainy3Assignchart});


