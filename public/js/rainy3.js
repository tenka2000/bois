/****************************************************************
 * Rainy3 Client Web Interface Framework
 ****************************************************************/
var RAINY3MESSAGE=new function(){

	this.TITLE_CONFIRM='確認';
	this.MESSAGE_CONFIRM='警告があります。OKボタンをクリックすれば、再リクエストします。<br />CANCELボタンをクリックすれば、処理を中断します。';

	this.TITLE_ALERT='アラート';
	this.MESSAGE_ALERT='アラートが発生しました。';

	this.TITLE_SESSION_TIME_OUT='セッションタイムアウト';
	this.MESSAGE_SESSION_TIME_OUT='一定時間操作がありませんでしたので、安全のために接続を切りました。<br />恐れ入りますが、ログインからやり直してください。';

	this.TITLE_PARMIT='権限がありません';

	this.TITLE_SYSTEM='システムトラブルが発生しました';
	this.MESSAGE_SYSTEM='大変申し訳ございません。只今、システムトラブルが発生しております。</br/>お手数をお掛けしますが、しばらく時間をおいてから、再度アクセスをお願いします。</br/>この問題が解決されない場合は、システム管理者までご連絡ください。';

	this.TITLE_DOWN='システムトラブルが発生しました';
	this.MESSAGE_DOWN='大変申し訳ございません。只今、システムトラブルが発生しております。</br/>お手数をお掛けしますが、システム管理者までご連絡ください。';

	this.MESSAGE_COMPLATE = '処理が完了しました';
	this.FORM_WINDOW_ADD_FIELD_MESSAGE = '＋入力フィールドを追加する';

}

var rainy3=new function(){

	var _self = this;
	var debug = true;
	// rails用フラグ
	this.RAILS=true	


	////////////////////////////////////////////////
	// 定数定義
	var CONTENTS_MIN_WIDTH=1000;
	var CONTENTS_MIN_HEIGHT=400;
	var DELAY_TIMEOUT = 10;
	var SUBMENU_MARGIN =10;

	var CREATE_OBJECT_ID_PREFIX="72hdshdure_";
	var CREATE_OBJECT_ID_SUFFIX_XCONTENT_DELIM="_jdahureqe";
	var CREATE_OBJECT_ID_SUFFIX_YCONTENT_DELIM="_jfajikfed";
	var CREATE_OBJECT_ID_SUFFIX_CONTENT_FOOTER="_ksjieekmw"

	var SUBSEARCH_TR_ID_SUFFIX = "_asi05edmw"

	////////////////////////////////////////////////
	// 変数定義
	_self.ie=false;
	_self.jqBody = null;
	_self.displayProtection = null;
	_self.lobjDisplayLoding = null;
	_self.lobjContents = null;
	_self.lobjHeader = null;
	_self.lobjFooter = null;
	_self.jqTabStop = null;
	_self.layoutList = [];
	_self.cacheProtectionEvent=[];
	_self.cacheProtectionStyle=[];
	_self.contentsYSpaceSingle = null;
	_self.contentsYSpace = null;
	_self.jqXContentsResizeBorder = null;
	_self.jqYContentsResizeBorder = null;
	_self.documentMouseupEvent=[];
	_self.documentMousedownEvent=[];
	_self.windows=[]

	_self.jqMessageWindow = null;
	_self.jqMessageWindowTitle=null;
	_self.jqMessageWindowMessage = null;
	_self.jqMessageWindowOK = null;
	_self.jqMessageWindowCANCEL = null;
	_self.jqMessageWindowLT = null;
	_self.jqMessageWindowLL = null;
	_self.jqMessageWindowLB = null;
	_self.jqMessageWindowTT = null;
	_self.jqMessageWindowBB = null;
	_self.jqMessageWindowRT = null;
	_self.jqMessageWindowRR = null;
	_self.jqMessageWindowRB = null;
	_self.messageWindowPadding = null;
	this.resizeFunction=null;

	// サブウィンドウ検索用ページ番号
	_self.subSearchPage=1;
	// サブウィンドウ検索用ソートキー
	_self.subSearchSort=null;
	// サブウィンドウ検索用ソート方向
	_self.subSearchDirection=null;
	// サブウィンドウ検索用検索条件
	_self.subSearchCondition={};

	// 現在表示しているサブメニュー
	_self.currentSubmenu=null;

	// フラグ関連
	_self.flgProtection = false;	// 画面保護フラグ

	////////////////////////////////////////////////
	// 初期化処理
	_self.init = function(){

		// ie判定
		//if(!_self.ie){
		//	var userAgent = window.navigator.userAgent.toLowerCase();
		//	if(userAgent.indexOf('msie') != -1){
		//		ie = true;
		//	}
		//}
		
		// 基本オブジェクトの取得
		_self.displayProtection = $('#display_protection');
		_self.lobjDisplayLoding = app.getLayoutObject('#display_loading');
		_self.jqBody = $('body');
		_self.lobjContents = app.getLayoutObject('#contents');
		_self.lobjHeader = app.getLayoutObject('#header');
		_self.lobjFooter = app.getLayoutObject('#footer');
		_self.jqTabStop = $('input.tab_stop');
		_self.jqXContentsResizeBorder = $('#x_contents_resize_border');
		_self.jqYContentsResizeBorder = $('#y_contents_resize_border');

		_self.jqMessageWindow = $('#message_window');
		_self.jqMessageWindowTitle = $('#message_window .title');
		_self.jqMessageWindowMessage = $('#message_window .message');
		_self.jqMessageWindowOK = $('#message_window .control_ok');
		_self.jqMessageWindowCANCEL = $('#message_window .control_cancel');
		_self.jqMessageWindowLT = $('#message_window .lt');
		_self.jqMessageWindowLL = $('#message_window .ll');
		_self.jqMessageWindowLB = $('#message_window .lb');
		_self.jqMessageWindowTT = $('#message_window .tt');
		_self.jqMessageWindowBB = $('#message_window .bb');
		_self.jqMessageWindowRT = $('#message_window .rt');
		_self.jqMessageWindowRR = $('#message_window .rr');
		_self.jqMessageWindowRB = $('#message_window .rb');
		_self.messageWindowPadding = app.px2num(_self.jqMessageWindow.css('padding-left'));
		_self.messageWindowWidth = 0;
		_self.messageWindowHeigh = 0;

		_self.jqSubmenu = $('#submenu');
		_self.jqSubmenuContents = $('#submenu .contents');
		_self.jqSubmenuLT = $('#submenu .lt');
		_self.jqSubmenuLL = $('#submenu .ll');
		_self.jqSubmenuLB = $('#submenu .lb');
		_self.jqSubmenuTT = $('#submenu .tt');
		_self.jqSubmenuBB = $('#submenu .bb');
		_self.jqSubmenuRT = $('#submenu .rt');
		_self.jqSubmenuRR = $('#submenu .rr');
		_self.jqSubmenuRB = $('#submenu .rb');
		_self.submenuPadding = app.px2num(_self.jqSubmenu.css('padding-left'));
		_self.submenuContentsPadding = app.px2num(_self.jqSubmenuContents.css('padding-left'));


		_self.contentsYSpaceSingle = $('.contents_y_space').height();
		_self.contentsYSpace = $('.contents_y_space').height() * 2;

		// 画面保護
		_self.loading('white');

		// フッターの描画
		_self.lobjFooter.jquery.show();

		// 横方向割付
		var xContents = $('.x_content');
		// リサイズ可能オブジェクトの出現フラグ
		var resizeableXContetnt = false;
		// 最終比率オブジェクトの出現フラグ
		var lastRateXContent = false;

		for(var i=xContents.length-1; i>=0; i--){

			var xContent = xContents.eq(i);
			// widthが10以下の場合は初期値に比率を設定する
			// widthが11以上の場合は初期値に固定値を設定する
			// 比率の部分は、画面幅-固定幅-区切りの残りを比率で幅を割出する
			// 比率指定の場合はリサイズの場合に自動的に幅を広げる（比率の合計は10になるように設定する）
			// 固定値はリサイズしても幅は変更しない
			var raitoflg = false;
			var raito = 0;
			var lastRate=false;
			if(xContent.width() <= 10){
				raitoflg = true;
				raito = xContent.width();
				if(!lastRateXContent){
					lastRate=true;
				}
				lastRateXContent=true;
			}

			// サイズの固定or稼働を取得する
			var fixed = false;
			if(xContent.css('position') == 'fixed'){
				fixed = true;
			}
			xContent.css('position', 'static');

			// border部の自動追加
			var delim = null;
			if(i < xContents.length-1){
				if(!fixed && resizeableXContetnt){
					delim = $('<div class="x_content_delim" id="' + CREATE_OBJECT_ID_PREFIX + xContent.attr('id') + CREATE_OBJECT_ID_SUFFIX_XCONTENT_DELIM + '"></div>').insertAfter(xContent);
				}
				else{
					delim = $('<div class="x_content_delim_fixed" id="' + CREATE_OBJECT_ID_PREFIX + xContent.attr('id') + CREATE_OBJECT_ID_SUFFIX_XCONTENT_DELIM + '"></div>').insertAfter(xContent);
				}
			}

			if(!fixed){
				resizeableXContetnt=true;
			}
			var yContentList = [];

			_self.layoutList.unshift(
				{
					xid:xContent.attr('id'),	// 対象のオブジェクトのＩＤ
					jquery:xContent,			// 対象のJQueryオブジェクト
					raitoflg:raitoflg,			// 比率フラグ
					raito:raito,				// 比率
					delim:delim,				// 区切りのオブジェクト
					fixed:fixed,				// サイズ固定フラグ
					lastRate:lastRate,			// 最終比率コンテンツ
					yContents:yContentList		// yContents
				}
			);

			// 縦方向割付
			var yContents = xContent.children('.y_content');
			// リサイズ可能オブジェクトの出現フラグ
			var resizeableYContetnt = false;
			// 最終比率オブジェクトの出現フラグ
			var lastRateYContent = false;

			for(var j=yContents.length-1; j>=0; j--){


				var yContent = yContents.eq(j);
				// heightが10以下の場合は初期値に比率を設定する
				// heightが11以上の場合は初期値に固定値を設定する
				// 比率の部分は、画面幅-固定幅-区切りの残りを比率で幅を割出する
				// 比率指定の場合はリサイズの場合に自動的に幅を広げる（比率の合計は10になるように設定する）
				// 固定値はリサイズしても幅は変更しない
				var raitoflg = false;
				var raito = 0;
				var lastRate=false;
				if(yContent.height() <= 10){
					raitoflg = true;
					raito = yContent.height();
					if(!lastRateYContent){
						lastRate=true;
					}
					lastRateYContent=true;
				}

				// サイズの固定or稼働を取得する
				var fixed = false;
				if(yContent.css('position') == 'fixed'){
					fixed = true;
				}
				yContent.css('position', 'static');

				var delim = null;
				if(j < yContents.length-1){
					if(!fixed && resizeableYContetnt){
						delim = $('<div class="y_content_delim" id="' + CREATE_OBJECT_ID_PREFIX + yContent.attr('id') + CREATE_OBJECT_ID_SUFFIX_YCONTENT_DELIM + '"></div>').insertAfter(yContent);
					}
					else{
						delim = $('<div class="y_content_delim_fixed" id="' + CREATE_OBJECT_ID_PREFIX + yContent.attr('id') + CREATE_OBJECT_ID_SUFFIX_YCONTENT_DELIM + '"></div>').insertAfter(yContent);
					}
				}

				if(!fixed){
					resizeableYContetnt=true;
				}
				yContentList.unshift(
					{
						yid:yContent.attr('id'),	// 対象のオブジェクトのＩＤ
						jquery:yContent,			// 対象のJQueryオブジェクト
						raitoflg:raitoflg,			// 比率フラグ
						raito:raito,				// 比率
						delim:delim,				// 区切りのオブジェクト
						fixed:fixed,				// サイズ固定フラグ
						lastRate:lastRate,			// 最終比率コンテンツ
					}
				);
				
				// ウィンドウの初期化
				_self.initWindow(yContent);

			}
		}

		// ウィンドウの親フレームとマッピング
		_self.mappingWindows();

		// オーバーレイ時のtab stop処理
		_self.setTabStop();

		// xContentsリサイズイベントバインド
		_self.bindResizeXContents();

		// yContentsリサイズイベントバインド
		_self.bindResizeYContents();

		// ドキュメントオブジェクトのイベントセット
		_self.setDocumentMouseupEvent();
		_self.setDocumentMousedownEvent();

		// ツールチップの設定
		// $('.r3menu_link').powerTip({followMouse:true, smartPlacement:true, offset:15, fadeInTime:300});

		// メッセージボタンのセット
		_self.setButtonAction('message_window_ok');
		_self.setButtonAction('message_window_cancel');


		// 再描画処理
		_self.resize();
		// 画面リサイズイベントにバインド
		$(window).resize(_self.resize);
	}
	
	////////////////////////////////////////////////
	// 画面理リサイズ処理
	_self.resize = function(){

		// サブメニューが表示されていた場合は非表示にする
		if(_self.currentSubmenu != null){
			_self.hideSubmenu();
		}

		var winWidth = app.getWindowWidth();
		var winHeight = app.getWindowHeight();

		if(winWidth < CONTENTS_MIN_WIDTH){
			winWidth = CONTENTS_MIN_WIDTH;
			_self.jqBody.css("overflow-x", "auto");
		}
		else{
			_self.jqBody.css("overflow-x", "hidden");
		}

		if(winHeight < CONTENTS_MIN_HEIGHT){
			winHeight = CONTENTS_MIN_HEIGHT;
			_self.jqBody.css("overflow-y", "auto");
		}
		else{
			_self.jqBody.css("overflow-y", "hidden");
		}

		_self.lobjHeader.jquery.width(winWidth);
		_self.lobjFooter.jquery.width(winWidth);


		// ※1
		// IEの場合は、両サイド20px分の余白をとる
		// IE以外の場合は、左サイド20pxの余白をとる
		var contentsWidth = winWidth - _self.lobjContents.marginLeft;
		var contentsHeight = winHeight - _self.lobjHeader.totalHeight - _self.lobjFooter.totalHeight - _self.contentsYSpace;
		if(_self.ie){
			contentsWidth -= _self.lobjContents.marginLeft;
		}

	
		// .contentsの横・高を設定
		_self.lobjContents.jquery.width(contentsWidth);
		_self.lobjContents.jquery.height(contentsHeight);

		// 固定値の合算を割り出す
		var contentsFixedWidth = 0;
		for(var i=0; i<_self.layoutList.length; i++){
			if(!_self.layoutList[i].raitoflg){
				contentsFixedWidth += _self.layoutList[i].jquery.width();
			}
			if(_self.layoutList[i].delim != null){
				contentsFixedWidth += _self.layoutList[i].delim.width();
			}
		}

		// コンテンツの高さと幅の設定
		var contentWidth = 0;
		// ※1
		// IEの場合は、両サイド20px分の余白を考慮し、20pxの減算はしない
		// IE以外の場合は、右サイド20px分の予約を設定する
		var lastContentWidth = contentsWidth - _self.lobjContents.marginLeft - contentsFixedWidth;
		if(_self.ie){
			lastContentWidth += _self.lobjContents.marginLeft;
		}
		
		// yContents
		for(var i=0; i<_self.layoutList.length; i++){
			_self.layoutList[i].jquery.height(contentsHeight);
			if(_self.layoutList[i].delim != null){
				_self.layoutList[i].delim.height(contentsHeight);
			}
			if(_self.layoutList[i].raito){
				// 比率の中で最後の場合は、すべて残った余りを設定する
				// それ以外は比率を求めて設定する
				if(!_self.layoutList[i].lastRate){
					contentWidth = ((contentsWidth - contentsFixedWidth) / 10) * _self.layoutList[i].raito;
					lastContentWidth -= contentWidth;
					_self.layoutList[i].jquery.width(contentWidth);
				}
				else{
					contentWidth = lastContentWidth;
					_self.layoutList[i].jquery.width(Math.floor(contentWidth));
				}
			}

			// xContents
			var contentsFixedHeight = 0;
			for(var j=0; j<_self.layoutList[i].yContents.length; j++){
				if(!_self.layoutList[i].yContents[j].raitoflg){
					contentsFixedHeight += _self.layoutList[i].yContents[j].jquery.height();
				}
				if(_self.layoutList[i].yContents[j].delim != null){
					contentsFixedHeight += _self.layoutList[i].yContents[j].delim.height();
				}
			}

			var contentHeight = null;
			var lastContentHeight = contentsHeight - contentsFixedHeight;
			var yContentsWidth = _self.layoutList[i].jquery.width();

			for(var j=0; j<_self.layoutList[i].yContents.length; j++){
				var yContent = _self.layoutList[i].yContents[j];

				yContent.jquery.width(yContentsWidth);

				if(yContent.delim != null){
					yContent.delim.width(yContentsWidth);

					yContent.delim.removeClass('y_content_delim_80');
					yContent.delim.removeClass('y_content_delim_25');
					if(yContentsWidth < 160 && yContentsWidth >= 80){
						yContent.delim.addClass('y_content_delim_80');
					}
					else if(yContentsWidth < 80){
						yContent.delim.addClass('y_content_delim_25');
					}
				}

				if(yContent.raito){
					// 比率の中で最後の場合は、すべて残った余りを設定する
					// それ以外は比率を求めて設定する
					if(!yContent.lastRate){
						contentHeight = ((contentsHeight - contentsFixedHeight) / 10) * yContent.raito ;
						lastContentHeight -= contentHeight;
						yContent.jquery.height(contentHeight);
					}
					else{
						contentHeight = Math.floor(lastContentHeight);
						yContent.jquery.height(contentHeight);
					}
				}

				// ウィンドウの描画
				var win = _self.getWindow(yContent['yid']);
				if(win != null){
					win._resize(yContentsWidth, contentHeight);
				}
			}
		}

		// 画面保護のリサイズ
		if(_self.flgProtection){
			var tmp = _self.jqBody.height();
			if(winHeight > tmp){
				tmp = winHeight;
			}
			_self.displayProtection.width(winWidth);
			_self.displayProtection.height(tmp);
		}

		// メッセージウィンドウの位置変更
		_self.resizeMessageWindow(winWidth, winHeight);
	}

	_self.resizeMessageWindow=function(winWidth, winHeight){
		if(winWidth == null){
			winWidth = app.getWindowWidth();
			if(winWidth < CONTENTS_MIN_WIDTH){
				winWidth = CONTENTS_MIN_WIDTH;
				_self.jqBody.css("overflow-x", "auto");
			}
			else{
				_self.jqBody.css("overflow-x", "hidden");
			}
		}
		if(winHeight == null){
			winHeight = app.getWindowHeight();
			if(winHeight < CONTENTS_MIN_HEIGHT){
				winHeight = CONTENTS_MIN_HEIGHT;
				_self.jqBody.css("overflow-y", "auto");
			}
			else{
				_self.jqBody.css("overflow-y", "hidden");
			}
		}
		// メッセージウィンドウの位置変更
		if(_self.flgMessageWindow){
			_self.jqMessageWindow.css('left', app.num2px((winWidth - (_self.messageWindowWidth + _self.messageWindowPadding * 2))/2));
			_self.jqMessageWindow.css('top', app.num2px((winHeight - (_self.messageWindowHeight + _self.messageWindowPadding * 2))/2));
		}
	}

	////////////////////////////////////////////////
	// 
	_self.windowSize = function(width, height){
		CONTENTS_MIN_WIDTH=width;
		CONTENTS_MIN_HEIGHT=height;
	}

	////////////////////////////////////////////////
	// 画面保護
	_self.protection = function(_class, focus, afterFunction){

		if(!_self.flgProtection){
			_self.flgProtection = true;
			_self.displayProtection.attr('class', _class);
			_self.displayProtection.css('display', 'block');
			_self.displayProtection.width(app.getWindowWidth());
			_self.displayProtection.height(app.getWindowHeight());
			_self.flgProtection = true;
			// focusがtrueの場合、tab_stopエリアにフォーカスし、tabで背面にフォーカスが廻らないようにする
			if(focus){
				_self.jqTabStop.css('display', 'block');
				$('input[name=tab_stop_2]').focus();
			}

			if(afterFunction != null){
				new afterFunction();
			}
		}
	}

	////////////////////////////////////////////////
	// 画面保護解除
	_self.releaseProtection = function(){
		for(var i=_self.cacheProtectionEvent.length - 1; i >= 0; i--){
			_self.displayProtection.unbind(_self.cacheProtectionEvent[i]);
			_self.cacheProtectionEvent.splice(i, 1);
		}
		for(var i=_self.cacheProtectionStyle.length - 1; i >= 0; i--){
			_self.displayProtection.css(_self.cacheProtectionStyle[i]['property'], _self.cacheProtectionStyle[i]['value']);
			_self.cacheProtectionStyle.splice(i, 1);
		}

		_self.displayProtection.css('display', 'none');
		_self.flgProtection = false;
		_self.jqTabStop.css('display', 'none');
		_self.flgProtection = false;
	}

	////////////////////////////////////////////////
	// 画面読み込み中
	_self.loading=function(_class){
	
		var winWidth = app.getWindowWidth();
		var winHeight = app.getWindowHeight();
		var top = (winHeight / 2) - (_self.lobjDisplayLoding.totalHeight / 2);
		var left = (winWidth / 2) - (_self.lobjDisplayLoding.totalWidth / 2);

		_self.lobjDisplayLoding.jquery.css('display', 'block');
		_self.lobjDisplayLoding.jquery.css('top', top);
		_self.lobjDisplayLoding.jquery.css('left', left);
		
		_self.protection(_class, true, null);
	}
	
		////////////////////////////////////////////////
	// 画面読み込み中
	_self.releaseLoading=function(_class){
		_self.lobjDisplayLoding.jquery.css('display', 'none');
		_self.releaseProtection();
	}


	////////////////////////////////////////////////
	// 画面保護画面のイベントバインド
	_self.bindProtection = function(event, _function){
		_self.displayProtection.bind(event, _function);
		// イベントをキャッシュしておいて、オーバーレイ非表示時にイベントをunbindする
		_self.cacheProtectionEvent.push(event);
	}

	////////////////////////////////////////////////
	// 画面保護画面のスタイル変更
	_self.styleProtection = function(prop, _value){

		var orgValue = _self.displayProtection.css(prop);
		_self.displayProtection.css(prop, _value);
		// スタイルをキャッシュしておいて、オーバーレイ非表示時にスタイルを戻す
		_self.cacheProtectionStyle.push({property:prop, value:orgValue});
	}

	////////////////////////////////////////////////
	// タブストップ処理
	_self.setTabStop = function(){
		_self.jqTabStop.keydown(
			function(e){
				if(e.keyCode == KEY.TAB){
					return false;
				}
			}
		);
		_self.jqTabStop.keypress(
			function(e){
				if(e.keyCode == KEY.TAB){
					return false;
				}
			}
		);
	}

	////////////////////////////////////////////////
	// xContentsリサイズイベントバインド
	_self.bindResizeXContents = function(){

		// マウスダウンで画面保護
		$('.x_content_delim').mousedown(function(e){

			// サブメニューが表示されていた場合は非表示にする
			if(_self.currentSubmenu != null){
				_self.hideSubmenu();
			}

			var delimId = e.target.id;
			var initPos = app.getMousePosition(e);
			var winHeight = app.getWindowHeight();

			// マウスカーソルの変更
			_self.styleProtection('cursor', 'col-resize');
			// リサイズ時に実行する処理がある場合は実行
			if(_self.resizeFunction != null){
				new _self.resizeFunction();
			}

			// マウス移動で枠線移動
			_self.bindProtection('mousemove', function(e){
				var movePos = app.getMousePosition(e);
				_self.jqXContentsResizeBorder.css('left', app.num2px(movePos.x - 2));
				return false;
			});

			// マウスアップイベントで、x-contentsリサイズ＆画面保護解除
			_self.bindDocumentMouseup('jfdafdaf', function(e){
				_self.execResizeXContents(delimId, initPos, app.getMousePosition(e));
				_self.releaseProtection();

				// 枠線の非表示
				_self.jqXContentsResizeBorder.css('display', 'none');

				// バインドの解除
				_self.unbindDocumentMouseup('jfdafdaf');
			});

			// 画面保護
			_self.protection("clear", false, function(){

				// 枠線の表示
				// topをヘッダー分にする
				// heightをコンテンツ＋区切りの高さにする
				// x位置をマウスの押されてた位置-2pxにする
				_self.jqXContentsResizeBorder.css('display', 'block');
				_self.jqXContentsResizeBorder.css('top', app.num2px(_self.lobjHeader.totalHeight));
				var barHeight = winHeight - _self.lobjHeader.totalHeight - _self.lobjFooter.totalHeight;
				_self.jqXContentsResizeBorder.height(barHeight);
				_self.jqXContentsResizeBorder.css('left', app.num2px(initPos.x - 2));
			});
			return false;
		});
	}

	////////////////////////////////////////////////
	// yContentsリサイズイベントバインド
	_self.bindResizeYContents = function(){

		// マウスダウンで画面保護
		$('.y_content_delim').mousedown(function(e){

			// サブメニューが表示されていた場合は非表示にする
			if(_self.currentSubmenu != null){
				_self.hideSubmenu();
			}

			var delimId = e.target.id;
			var initPos = app.getMousePosition(e);
			var winHeight = app.getWindowHeight();

			// xContentのIDを求める
			var parentId = $('#' + delimId).parent().attr('id');

			// マウスカーソルの変更
			_self.styleProtection('cursor', 'row-resize');
			// リサイズ時に実行する処理がある場合は実行
			if(_self.resizeFunction != null){
				new _self.resizeFunction();
			}

			// マウス移動で枠線移動
			_self.bindProtection('mousemove', function(e){
				var movePos = app.getMousePosition(e);
				_self.jqYContentsResizeBorder.css('top', app.num2px(movePos.y - 2));
				return false;
			});

			// マウスアップイベントで、y-contentsリサイズ＆画面保護解除
			_self.bindDocumentMouseup('jf5a1daf', function(e){
				_self.execResizeYContents(delimId, initPos, app.getMousePosition(e));
				_self.releaseProtection();

				// 枠線の非表示
				_self.jqYContentsResizeBorder.css('display', 'none');

				// バインドの解除
				_self.unbindDocumentMouseup('jf5a1daf');
			});

			// 画面保護
			_self.protection("clear", false, function(){

				// 枠線の表示
				// leftを右側のxContentの合算にする
				// widthをxContentの幅にする
				// y位置をマウスの押されてた位置-2pxにする
				_self.jqYContentsResizeBorder.css('display', 'block');

				var barLeft = 0;
				var barWidth = 100;
				for(var i=0; i<_self.layoutList.length; i++){

					if(_self.layoutList[i].xid == parentId){
						barWidth = _self.layoutList[i].jquery.width();
						break;
					}
					else{
						barLeft += _self.layoutList[i].jquery.width();
						if( _self.layoutList[i] != null){
							barLeft += _self.layoutList[i].delim.width();
						}
					}
				}
				_self.jqYContentsResizeBorder.css('left', barLeft + _self.lobjContents.marginLeft);
				_self.jqYContentsResizeBorder.width(barWidth);
				_self.jqYContentsResizeBorder.css('top', app.num2px(initPos.y - 2));

			});
			return false;
		});
	}

	////////////////////////////////////////////////
	// xContentsリサイズイベントバインド
	_self.execResizeXContents = function(delimId, initPos, endPos){

		// 対象のx-contentが固定値の場合は対象の値と次のリサイズ可能なオブジェクトのサイズを変更
		var targetXContentId = delimId.replace(CREATE_OBJECT_ID_PREFIX, '');
		targetXContentId = targetXContentId.replace(CREATE_OBJECT_ID_SUFFIX_XCONTENT_DELIM, '');
		var moveLen = endPos.x-initPos.x;

		// 事前に次のオブジェクトのサイズがマイナスにならないように稼働範囲を調整する
		var moveCurrent = null;
		var moveNext = null;
		var next = false;
		for(var i=0; i<_self.layoutList.length; i++){
			if(!next){
				// 現在のオブジェクトをシーク
				if(_self.layoutList[i]['xid'] == targetXContentId){
					moveCurrent = _self.layoutList[i];
					next = true;
				}
			}
			else{
				// 次の可変オブジェクトをシーク
				if(!_self.layoutList[i]['fixed']){
					moveNext = _self.layoutList[i];
					break;
				}
			}
		}
		if(moveCurrent.jquery.width() + moveLen < 30){
			moveLen = 30 - moveCurrent.jquery.width();
		}
		if(moveNext.jquery.width() - moveLen < 30){
			moveLen = moveNext.jquery.width() - 30;
		}

		var moveCurrentWidth = moveCurrent.jquery.width() + moveLen;
		moveCurrent.jquery.width(moveCurrentWidth);

		for(var i=0; i<moveCurrent.yContents.length; i++){
			moveCurrent.yContents[i].jquery.width(moveCurrentWidth);
			if(moveCurrent.yContents[i].delim != null){
				moveCurrent.yContents[i].delim.removeClass('y_content_delim_80');
				moveCurrent.yContents[i].delim.removeClass('y_content_delim_25');
				if(moveCurrentWidth < 160 && moveCurrentWidth >= 80){
					moveCurrent.yContents[i].delim.addClass('y_content_delim_80');
				}
				else if(moveCurrentWidth < 80){
					moveCurrent.yContents[i].delim.addClass('y_content_delim_25');
				}
				moveCurrent.yContents[i].delim.width(moveCurrentWidth);
			}

			var win = _self.getWindow(moveCurrent.yContents[i]['yid']);
			if(win != null){
				win._resize(moveCurrentWidth, null);
			}
		}

		var moveNextWidth = moveNext.jquery.width() - moveLen;
		moveNext.jquery.width(moveNextWidth);

		for(var i=0; i<moveNext.yContents.length; i++){
			moveNext.yContents[i].jquery.width(moveNextWidth);

			if(moveNext.yContents[i].delim != null){
				moveNext.yContents[i].delim.removeClass('y_content_delim_80');
				moveNext.yContents[i].delim.removeClass('y_content_delim_25');
				if(moveNextWidth < 160 && moveNextWidth >= 80){
					moveNext.yContents[i].delim.addClass('y_content_delim_80');
				}
				else if(moveNextWidth < 80){
					moveNext.yContents[i].delim.addClass('y_content_delim_25');
				}
				moveNext.yContents[i].delim.width(moveNextWidth);
			}

			var win = _self.getWindow(moveNext.yContents[i]['yid']);
			if(win != null){
				win._resize(moveNextWidth, null);
			}

		}

		var totalWidth = 0;
		// 全オブジェクトのオブジェクトを取得して比率を再計算してセット
		for(var i=0; i<_self.layoutList.length; i++){
			if(_self.layoutList[i]['raitoflg']){
				totalWidth += _self.layoutList[i]['jquery'].width();
			}
		}
		for(var i=0; i<_self.layoutList.length; i++){
			if(_self.layoutList[i]['raitoflg']){
				var raito = (_self.layoutList[i]['jquery'].width()/totalWidth)*10;
				if(raito < 1){
					if(_self.isLastRaito(_self.layoutList, i)){
						// raitoの最後の場合、さかのぼって2以上のraitoから-1して、raitoに1をセットする
						_self.decrementPreviousRaito(_self.layoutList, i);
					}
					raito = 1;
				}
				if(raito > 9){
					raito = 9;
				}
				if(_self.layoutList.length == 1){
					raito = 10;
				}
				_self.layoutList[i]['raito'] = Math.round(raito);
			}
		}
	}

	////////////////////////////////////////////////
	// yContentsリサイズイベントバインド
	_self.execResizeYContents = function(delimId, initPos, endPos){

		// xContentのIDを求める
		var parentId = $('#' + delimId).parent().attr('id');

		// 対象のy-contentが固定値の場合は対象の値と次のリサイズ可能なオブジェクトのサイズを変更
		var targetYContentId = delimId.replace(CREATE_OBJECT_ID_PREFIX, '');
		targetYContentId = targetYContentId.replace(CREATE_OBJECT_ID_SUFFIX_YCONTENT_DELIM, '');
		var moveLen = endPos.y-initPos.y;

		// 事前に次のオブジェクトのサイズがマイナスにならないように稼働範囲を調整する
		var moveCurrent = null;
		var moveNext = null;
		var next = false;
		for(var i=0; i<_self.layoutList.length; i++){
			if(_self.layoutList[i].xid == parentId){
				for(var j=0; j<_self.layoutList[i].yContents.length; j++){
					if(!next){
						// 現在のオブジェクトをシーク
						if(_self.layoutList[i]['yContents'][j]['yid'] == targetYContentId){
							moveCurrent = _self.layoutList[i]['yContents'][j];
							next = true;
						}
					}
					else{
						// 次の可変オブジェクトをシーク
						if(!_self.layoutList[i]['yContents'][j]['fixed']){
							moveNext = _self.layoutList[i]['yContents'][j];
							break;
						}
					}
				}
				break;
			}
		}
		if(moveCurrent.jquery.height() + moveLen < 30){
			moveLen = 30 - moveCurrent.jquery.height();
		}
		if(moveNext.jquery.height() - moveLen < 30){
			moveLen = moveNext.jquery.height() - 30;
		}

		var moveCurrentHeight = moveCurrent.jquery.height() + moveLen;
		moveCurrent.jquery.height(moveCurrentHeight);

		var win = _self.getWindow(moveCurrent['yid']);
		if(win != null){
			win._resize(null, moveCurrentHeight);
		}

		var moveNextHeight = moveNext.jquery.height() - moveLen;
		moveNext.jquery.height(moveNextHeight);

		var win = _self.getWindow(moveNext['yid']);
		if(win != null){
			win._resize(null, moveNextHeight);
		}


		var totalHeight = 0;
		// 全オブジェクトのオブジェクトを取得して比率を再計算してセット
		for(var i=0; i<_self.layoutList.length; i++){
			if(_self.layoutList[i].xid == parentId){
				for(var j=0; j<_self.layoutList[i].yContents.length; j++){
					if(_self.layoutList[i].yContents[j]['raitoflg']){
						totalHeight += _self.layoutList[i].yContents[j]['jquery'].height();
					}
				}
			}
		}
		for(var i=0; i<_self.layoutList.length; i++){
			if(_self.layoutList[i].xid == parentId){
				for(var j=0; j<_self.layoutList[i].yContents.length; j++){
					if(_self.layoutList[i].yContents[j]['raitoflg']){
						var raito = (_self.layoutList[i].yContents[j]['jquery'].height()/totalHeight)*10;
						if(raito < 1){
							if(_self.isLastRaito(_self.layoutList[i].yContents, j)){
								// raitoの最後の場合、さかのぼって2以上のraitoから-1して、raitoに1をセットする
								_self.decrementPreviousRaito(_self.layoutList[i].yContents, j);
							}
							raito = 1;
						}
						if(raito > 9){
							raito = 9;
						}
						if(_self.layoutList[i].yContents.length == 1){
							raito = 10;
						}
						_self.layoutList[i].yContents[j]['raito'] = Math.round(raito);
					}
				}
			}
		}
	}

	////////////////////////////////////////////////
	// 最終の比率か判定
	_self.isLastRaito=function(list, currentIndex){
		var ret = true;
		for(var j=currentIndex + 1; j<list.length; j++){
			if(list[j]['raitoflg']){
				ret = false;
				break;
			}
		}
		return ret;
	}

	////////////////////////////////////////////////
	// 直前の比率を減算
	_self.decrementPreviousRaito=function(list, currentIndex){
		var ret = true;
		for(var j=currentIndex - 1; j>=0; j--){
			if(list[j]['raitoflg'] && list[j]['raito'] > 1){
				list[j]['raito'] -= 1;
				break;
			}
		}
	}

	////////////////////////////////////////////////
	// ウィンドウの追加
	_self.addWindow=function(obj){
		_self.windows.push(
			{window:obj}
		);
	}


	////////////////////////////////////////////////
	// キーを指定して関数をドキュメントオブジェクトのマウスアップイベントにバインドさせることができる。
	// バインドを解除するには、unbindDocumentMouserupを使用する。
	// キー指定なのでバインドを解除する際に、他のイベントをunbindすることがない。
	// ただし、他の処理で無条件にunbindされるとすべての処理がbindからはずれるので注意
	_self.bindDocumentMouseup=function(key, func){
		_self.documentMouseupEvent.push({key:key, func:func});
	}

	////////////////////////////////////////////////
	// キーを指定して関数をドキュメントオブジェクトのマウスアップイベントをアンバインドさせることができる。
	_self.unbindDocumentMouseup=function(key){
		for(var i=_self.documentMouseupEvent.length - 1; i >= 0; i--){
			if(_self.documentMouseupEvent[i]['key'] == key){
				_self.documentMouseupEvent.splice(i, 1);
			}
		}
	}

	////////////////////////////////////////////////
	// ドキュメントオブジェクトのマウスアップイベントの設定
	_self.setDocumentMouseupEvent=function(){
		$(document).mouseup(function(e){
			for(var i=0; i<_self.documentMouseupEvent.length; i++){
				new _self.documentMouseupEvent[i]['func'](e);
			}
		});
	}

	////////////////////////////////////////////////
	// キーを指定して関数をドキュメントオブジェクトのマウスダウンイベントにバインドさせることができる。
	// バインドを解除するには、unbindDocumentMouserdownを使用する。
	// キー指定なのでバインドを解除する際に、他のイベントをunbindすることがない。
	// ただし、他の処理で無条件にunbindされるとすべての処理がbindからはずれるので注意
	_self.bindDocumentMousedown=function(key, func){
		_self.documentMousedownEvent.push({key:key, func:func});
	}

	////////////////////////////////////////////////
	// キーを指定して関数をドキュメントオブジェクトのマウスダウンイベントをアンバインドさせることができる。
	_self.unbindDocumentMousedown=function(key){
		for(var i=_self.documentMousedownEvent.length - 1; i >= 0; i--){
			if(_self.documentMousedownEvent[i]['key'] == key){
				_self.documentMousedownEvent.splice(i, 1);
			}
		}
	}

	////////////////////////////////////////////////
	// ドキュメントオブジェクトのマウスダウンイベントの設定
	_self.setDocumentMousedownEvent=function(){
		$(document).mousedown(function(e){
			for(var i=0; i<_self.documentMousedownEvent.length; i++){
				new _self.documentMousedownEvent[i]['func'](e);
			}
		});
	}

	////////////////////////////////////////////////
	// ウィンドウと親フレーム枠のマッピング
	_self.mappingWindows=function(){
		for(var i=0; i<_self.windows.length; i++){
			var winObj = _self.windows[i]['window'];
			winObj.jquery = $('#' + winObj.id);
			_self.windows[i]['parent'] = winObj.jquery.parent();
		}
	}

	////////////////////////////////////////////////
	// 子ウィンドウの初期化
	this.rainy3ObjectList=[];

	_self.initWindow=function(jqYContent){
		var _self=this;
		var jqObjs = jqYContent.children();
		if(jqObjs.length > 0){
			var jqObj = jqObjs.eq(0);
			if(jqObj.attr('id') != null){

				for(var i=0; i<_self.rainy3ObjectList.length; i++){
					if(jqObj.hasClass(_self.rainy3ObjectList[i]['key'])){
						var obj = new _self.rainy3ObjectList[i]['_class']();

						obj.create(jqObj.attr('id'));
						_self.addWindow(obj);
						break;
					}
				}
				if(i>=_self.rainy3ObjectList.length){
					var obj = new Rainy3Base();
					obj.create(jqObj.attr('id'));
					_self.addWindow(obj);
				}
			}
		}
	}
	
	////////////////////////////////////////////////
	// ウィンドウIDを指定して、ウィンドウの取得を行う
	_self.getWindow=function(yid){
		var ret = null;
		for(var i=0; i<_self.windows.length; i++){
			if(_self.windows[i]['window']['parentId'] == yid){
				ret = _self.windows[i]['window'];
				break;
			}
		}
		return ret;
	}

	////////////////////////////////////////////////
	// ウィンドウIDを指定して、JQueryオブジェクトの取得
	_self.getWindowJQueryObject=function(yid){
		var ret = null;
		for(var i=0; i<_self.windows.length; i++){
			if(_self.windows[i]['window']['parentId'] == yid){
				ret = _self.windows[i]['parent'];
				break;
			}
		}
		return ret;
	}

	////////////////////////////////////////////////
	// ウィンドウIDを指定して、画面内のtop位置を取得する
	_self.getWindowTop=function(yid){

		// ヘッダー分を初期値に設定
		var ret = _self.lobjHeader.totalHeight + _self.contentsYSpaceSingle;

		for(var i=0; i<_self.layoutList.length; i++){
			var topPos = 0;
			var yContetnsList = _self.layoutList[i]['yContents'];
			for(var j=0; j<yContetnsList.length; j++){

				if(yContetnsList[j]['yid'] == yid){
					ret += topPos;
					break;
				}

				if(yContetnsList[j]['delim'] != null){
					topPos += yContetnsList[j]['delim'].height();
				}
				topPos += yContetnsList[j]['jquery'].height();
			}
		}
		return ret;
	}

	////////////////////////////////////////////////
	// ウィンドウIDを指定して、画面内のleft位置を取得する
	_self.getWindowLeft=function(yid){

		var ret=0;
		var leftPos = rainy3.lobjContents.marginLeft;
		for(var i=0; i<_self.layoutList.length; i++){

			var yContetnsList = _self.layoutList[i]['yContents'];
			for(var j=0; j<yContetnsList.length; j++){
				if(yContetnsList[j]['yid'] == yid){
					ret = leftPos;
					break;
				}
			}

			if(_self.layoutList[i]['delim'] != null){
				leftPos += _self.layoutList[i]['delim'].width();
			}
			leftPos += _self.layoutList[i]['jquery'].width();

		}
		return ret;
	}

	////////////////////////////////////////////////
	// ＨＴＴＰリクエスト
	_self.request=function(url, dataMap, force, successFunction, userErrorFunction, preFunction, postFunction){
		// 事前処理
		if(preFunction != null){
			new preFunction();
		}

		if(dataMap == null){
			dataMap = {};
		}
		if(force){
			dataMap['force']=true;
		}

		$.ajax({
			url: url,
			type: 'POST',
			data: dataMap,
			async: true,
			cache: true,
			dataType:"xml",
			error: function(XMLHttpRequest, textStatus, errorThrown){
				if(debug){
					alert(url + "\n" + textStatus + "\n-------------------------\n" + errorThrown);
				}
				// アラートを表示して終了
				_self.showMessageWindow('OK_ONLY', RAINY3MESSAGE.TITLE_DOWN, RAINY3MESSAGE.MESSAGE_DOWN, null, null);
			},
			success: function(xml){

				var head = $(xml).find("head");
				var body = $(xml).find("body");
				var result = head.find("result").text();

				var title = head.find('title').text();
				var message = head.find('message').text();

				// 正常終了
				if(result == '1'){
					// 成功時の処理を実行
					new successFunction(head, body);
					// 事後処理
					if(postFunction != null){
						new postFunction(head, body);
					}
				}
				// ユーザーエラー
				else if(result == '2'){
					// ユーザーエラー時の処理を実行
					new userErrorFunction(head, body);
					// 事後処理
					if(postFunction != null){
						new postFunction(head, body);
					}
				}
				// 警告（OK後、再リクエスト）
				else if(result == '3'){
					// 事後処理
					if(postFunction != null){
						new postFunction(head, body);
					}

					if(title == ''){
						title = RAINY3MESSAGE.TITLE_CONFIRM;
					}

					if(message == ''){
						message = RAINY3MESSAGE.MESSAGE_CONFIRM;
					}

					// ワーニングを表示して、OKであれば再リクエスト
					_self.showMessageWindow('OK_CANCEL', 
											title, message, 
											function(){
												_self.request(url, dataMap, true, successFunction, userErrorFunction, preFunction, postFunction);
											}, null);
				}
				// アラート（処理中断）
				else if(result == '4'){
					// 事後処理
					if(postFunction != null){
						new postFunction(head, body);
					}

					if(title == ''){
						title = RAINY3MESSAGE.TITLE_ALERT;
					}

					if(message == ''){
						message = RAINY3MESSAGE.MESSAGE_ALERT;
					}

					// ワーニングを表示して、OKであれば再リクエスト
					_self.showMessageWindow('OK_ONLY', 
											title, message, null, null);
				}
				// 認証失敗
				else if(result == '5'){

					// 事後処理
					if(postFunction != null){
						new postFunction(head, body);
					}

					var forward = head.find('forward').text();

					if(title == ''){
						title = RAINY3MESSAGE.TITLE_SESSION_TIME_OUT;
					}

					if(message == ''){
						message = RAINY3MESSAGE.MESSAGE_SESSION_TIME_OUT;
					}

					// アラートを表示してリダイレクト先に遷移
					_self.showMessageWindow('OK_ONLY', title, message, function(){location.href=forward}, null);
				}
				// アクセス権限なし
				else if(result == '6'){

					// 事後処理
					if(postFunction != null){
						new postFunction(head, body);
					}

					if(title == ''){
						title = RAINY3MESSAGE.TITLE_PARMIT;
					}

					if(message == ''){
						message = RAINY3MESSAGE.MESSAGE_PARMIT;
					}

					// アラートを表示して終了
					_self.showMessageWindow('OK_ONLY', title, message, null, null);
				}
				// システムエラー（継続不可）
				else if(result == '9'){
					// 事後処理
					if(postFunction != null){
						new postFunction(head, body);
					}

					if(title == ''){
						title = RAINY3MESSAGE.TITLE_SYSTEM;
					}

					if(message == ''){
						message = RAINY3MESSAGE.MESSAGE_SYSTEM;
					}

					// アラートを表示して終了
					_self.showMessageWindow('OK_ONLY', title, message, null, null);
				}
			}
		});
	}

	////////////////////////////////////////////////
	// モーダルウィンドウの表示
	// type：'OK_ONLY' or 'OK_CANCEL'
	_self.flgMessageWindow=false;
	_self.showMessageWindow=function(type, title, message, okFunction, cancelFunction){

		// すでに画面保護済みの場合は、保護が解除するまで待機する
		if(_self.flgProtection){
			setTimeout(function(){
				_self.showMessageWindow(type, title, message, okFunction, cancelFunction);
			}, 100);
		}
		else{
			_self.protection("black", true, function(){
				// ウィンドウの表示
				_self.jqMessageWindowTitle.html(title);
				_self.jqMessageWindowMessage.html(message);
				_self.jqMessageWindowOK.unbind('click');
				_self.jqMessageWindowCANCEL.unbind('click');
				_self.jqMessageWindowOK.click(function(){
					if(okFunction != null){
						var _ret = okFunction();
						if(_ret != null && _ret == false){
							return;
						}
					}
					_self.flgMessageWindow=false;
					_self.jqMessageWindow.hide();
					_self.releaseProtection();
				});

				_self.jqMessageWindowCANCEL.click(function(){
					if(cancelFunction != null){
						_ret = cancelFunction();
						if(_ret != null && _ret == false){
							return;
						}
					}
					_self.flgMessageWindow=false;
					_self.jqMessageWindow.hide();
					_self.releaseProtection();
				});

				if(type == 'OK_ONLY'){
					_self.jqMessageWindowOK.show();
					_self.jqMessageWindowCANCEL.hide();
				}
				else{
					_self.jqMessageWindowOK.show();
					_self.jqMessageWindowCANCEL.show();
				}

				// 枠の位置合わせ
				var msgHeight = _self.jqMessageWindow.height();
				var msgWidth = _self.jqMessageWindow.width();
				_self.jqMessageWindowLL.height(msgHeight);
				_self.jqMessageWindowLL.css('top', app.num2px(_self.messageWindowPadding));
				_self.jqMessageWindowLB.css('top', app.num2px(_self.messageWindowPadding  + msgHeight));
				_self.jqMessageWindowTT.width(msgWidth);
				_self.jqMessageWindowTT.css('left', app.num2px(_self.messageWindowPadding));
				_self.jqMessageWindowBB.width(msgWidth);
				_self.jqMessageWindowBB.css('left', app.num2px(_self.messageWindowPadding));
				_self.jqMessageWindowBB.css('top', app.num2px(_self.messageWindowPadding  + msgHeight));
				_self.jqMessageWindowRT.css('left', app.num2px(_self.messageWindowPadding + msgWidth));
				_self.jqMessageWindowRR.height(msgHeight);
				_self.jqMessageWindowRR.css('top', app.num2px(_self.messageWindowPadding));
				_self.jqMessageWindowRR.css('left', app.num2px(_self.messageWindowPadding + msgWidth));
				_self.jqMessageWindowRB.css('top', app.num2px(_self.messageWindowPadding  + msgHeight));
				_self.jqMessageWindowRB.css('left', app.num2px(_self.messageWindowPadding + msgWidth));


				// 画面中央
				_self.flgMessageWindow=true;
				_self.setMessageWindowPosition();
				_self.jqMessageWindow.show();

				_self.messageWindowWidth = _self.jqMessageWindow.width();
				_self.messageWindowHeight = _self.jqMessageWindow.height();

				// IEで謎の隙間を空くのを防ぐためにリサイズ処理を行う
				_self.resizeMessageWindow(null, null);
				

				if(type == 'OK_ONLY'){
					_self.jqMessageWindowOK.focus();
				}
				else{
					_self.jqMessageWindowCANCEL.focus();
				}
			});
		}
	}

	////////////////////////////////////////////////
	// メッセージウィンドウのクローズ
	this.hideMessageWindow=function(){
			_self.flgMessageWindow=false;
			_self.jqMessageWindow.hide();
			_self.releaseProtection();
	}
	////////////////////////////////////////////////
	// メッセージウィンドウのポジション設定
	_self.setMessageWindowPosition=function(){

		if(_self.flgMessageWindow){
			var padding = _self.messageWindowPadding;
			var winWidth = app.getWindowWidth();
			var winHeight = app.getWindowHeight();
			var msgWidth = _self.jqMessageWindow.width();
			var msgHeight = _self.jqMessageWindow.height();
			var left = (winWidth - (msgWidth + padding * 2))/2;
			var top = (winHeight - (msgHeight + padding * 2))/2;

			_self.jqMessageWindow.css('left', left + "px");
			_self.jqMessageWindow.css('top', top + "px");
		}
	}

	////////////////////////////////////////////////
	// ボタンアクション設定
	_self.setButtonAction=function(id){
		var button = $('#' + id);
		button.mouseover(function(){
			button.addClass('hover');
		});
		button.mouseout(function(){
			button.removeClass('hover');
			button.removeClass('click');
		});
		button.mousedown(function(){
			button.addClass('click');
		});
		button.mouseup(function(){
			button.removeClass('click');
		});
	}

	////////////////////////////////////////////////
	// サブメニューの表示
	_self.showSubmenu=function(key, content, width, height, direction, left1, top1, left2, top2, preloadaction, loadaction, clickTargetId){
		// 表示されているものと同一キーの場合は非表示
		// 背景がクリックされた場合は非表示
		// リサイズされたら非表示（サイズ・位置取りが困難なため）
		var show = true;

		if(_self.currentSubmenu != null){
			_self.jqSubmenu.hide();
			if(key == _self.currentSubmenu){
				show = false;
			}
			_self.currentSubmenu = null;
		}
		if(show){

			_self.jqSubmenuContents.width(width);
			_self.jqSubmenuContents.height(height);

			_self.jqSubmenuContents.html(content);

			var winWidth = app.getWindowWidth();
			var winHeight = app.getWindowHeight();

			var msgHeight = _self.jqSubmenu.height();
			var msgWidth = _self.jqSubmenu.width();


			var left =0;
			var top = 0;

			// top,leftで位置補正を行う
			// 画面内に収まるように設定する
			// どこに表示しても収まらない場合は、サイズを小さくする
			// その際、scrollをonにする
			if(direction == 'height'){
				// 縦方向に表示
				// 横方向の中心位置を求める
				var center = msgWidth / 2;
				left = left1 - center;

				// widthの長さが全画面幅をこえる場合、全画面幅に合わせた幅にする
				if((msgWidth + _self.submenuPadding * 2) > winWidth - (SUBMENU_MARGIN * 2)){
					_self.jqSubmenuContents.width(winWidth - (_self.submenuPadding * 2) - (_self.submenuContentsPadding * 2) - (SUBMENU_MARGIN * 2));
					msgWidth = _self.jqSubmenu.width();
					msgHeight = _self.jqSubmenu.height();
					left = SUBMENU_MARGIN;
				}
				// 基準位置で左側が画面外にでる場合、画面左位置に合わせる
				else if((msgWidth + _self.submenuPadding * 2)/2 > left1 - SUBMENU_MARGIN){
					left = SUBMENU_MARGIN;
				}
				// 基準位置で右側が画面外にでる場合、画面右位置に合わせる
				else if((left + (msgWidth + _self.submenuPadding * 2)) > winWidth - SUBMENU_MARGIN){
					left = winWidth - SUBMENU_MARGIN - (msgWidth + _self.submenuPadding * 2);
				}

				top = top1;
				under_top = top;
				// top1を基準に下方向へ表示する
				// 画面外にはみ出す場合は上方向に表示する
				// それでも画面外にはみ出す場合は、上と下で表示領域が大きい方の高さでheightを調整する
				if(top + (msgHeight + _self.submenuPadding * 2) > winHeight - SUBMENU_MARGIN){
					top = top2 - (msgHeight + _self.submenuPadding * 2);

					if(top < SUBMENU_MARGIN){
						if(winHeight - top1 > top2){
							// 下が大きい
							top = under_top;
							_self.jqSubmenuContents.height(winHeight - top1  - (_self.submenuPadding * 2) - (_self.submenuContentsPadding * 2) - SUBMENU_MARGIN);
							msgWidth = _self.jqSubmenu.width();
							msgHeight = _self.jqSubmenu.height();
						}
						else{
							// 上が大きい
							_self.jqSubmenuContents.height(top2 - (_self.submenuPadding * 2) - (_self.submenuContentsPadding * 2) - SUBMENU_MARGIN);
							msgWidth = _self.jqSubmenu.width();
							msgHeight = _self.jqSubmenu.height();
							top = top2 - (msgHeight + _self.submenuPadding * 2);
						}
					}
				}
			}
			else{
				// 横方向に表示
				var center = msgHeight / 2;
				top = top1 - center;

				// heightの長さが全画面高をこえる場合、全画面高に合わせた幅にする
				if((msgHeight + _self.submenuPadding * 2) > winHeight - (SUBMENU_MARGIN * 2)){
					_self.jqSubmenuContents.height(winHeight - (_self.submenuPadding * 2) - (_self.submenuContentsPadding * 2) - (SUBMENU_MARGIN * 2))
					msgWidth = _self.jqSubmenu.width();
					msgHeight = _self.jqSubmenu.height();
					top = SUBMENU_MARGIN;
				}
				// 基準位置で上側が画面外にでる場合、画面上位置に合わせる
				else if((msgHeight + _self.submenuPadding * 2)/2 > top1 - SUBMENU_MARGIN){
					top = SUBMENU_MARGIN;
				}
				// 基準位置で下側が画面外にでる場合、画面下位置に合わせる
				else if((top + (msgHeight + _self.submenuPadding * 2)) > winHeight - SUBMENU_MARGIN){
					top = winHeight - SUBMENU_MARGIN - (msgHeight + _self.submenuPadding * 2);
				}
				left = left1;

				under_left = left;
				// left1を基準に右方向へ表示する
				// 画面外にはみ出す場合は左方向に表示する
				// それでも画面外にはみ出す場合は、右と左で表示領域が大きい方の幅でwidthを調整する
				if(left + (msgWidth + _self.submenuPadding * 2) > winWidth - SUBMENU_MARGIN){
					left = left2 - (msgWidth + _self.submenuPadding * 2);

					if(left < SUBMENU_MARGIN){
						if(winWidth - left1 > left2){
							// 右が大きい
							left = under_left;
							_self.jqSubmenuContents.width(winWidth - left1  - (_self.submenuPadding * 2) - (_self.submenuContentsPadding * 2) - SUBMENU_MARGIN);
							msgWidth = _self.jqSubmenu.width();
							msgHeight = _self.jqSubmenu.height();
						}
						else{
							// 左が大きい
							_self.jqSubmenuContents.width(left2 - (_self.submenuPadding * 2) - (_self.submenuContentsPadding * 2) - SUBMENU_MARGIN);
							msgWidth = _self.jqSubmenu.width();
							msgHeight = _self.jqSubmenu.height();
							left = left2 - (msgWidth + _self.submenuPadding * 2);
						}
					}
				}
			}

			// 枠部分の設定
			_self.jqSubmenuLL.height(msgHeight);
			_self.jqSubmenuLL.css('top', app.num2px(_self.submenuPadding));
			_self.jqSubmenuLB.css('top', app.num2px(_self.submenuPadding  + msgHeight));
			_self.jqSubmenuTT.width(msgWidth);
			_self.jqSubmenuTT.css('left', app.num2px(_self.submenuPadding));
			_self.jqSubmenuBB.width(msgWidth);
			_self.jqSubmenuBB.css('left', app.num2px(_self.submenuPadding));
			_self.jqSubmenuBB.css('top', app.num2px(_self.submenuPadding  + msgHeight));
			_self.jqSubmenuRT.css('left', app.num2px(_self.submenuPadding + msgWidth));
			_self.jqSubmenuRR.height(msgHeight);
			_self.jqSubmenuRR.css('top', app.num2px(_self.submenuPadding));
			_self.jqSubmenuRR.css('left', app.num2px(_self.submenuPadding + msgWidth));
			_self.jqSubmenuRB.css('top', app.num2px(_self.submenuPadding  + msgHeight));
			_self.jqSubmenuRB.css('left', app.num2px(_self.submenuPadding + msgWidth));


			_self.jqSubmenu.css('left', app.num2px(left));
			_self.jqSubmenu.css('top', app.num2px(top));

			if(preloadaction != null){
				new preloadaction();
			}


			_self.jqSubmenu.fadeIn('fast', loadaction);
			_self.currentSubmenu=key;

/*
			_self.jqSubmenuContents.mousedown(
				function(e){
					var submenuObj = app.getParentJQueryObjectByClassName($(e.target), 'submenu');
					if(submenuObj != null){
						return false;
					}
				}
			);
*/

			// マウスダウンイベントにバインド
			_self.bindDocumentMousedown("df83hasuh", function(e){
				var submenuObj = app.getParentJQueryObjectByClassName($(e.target), 'submenu');
				if(submenuObj == null){

					if(clickTargetId != null){
						var targetObj = app.getParentJQueryObjectById($(e.target), clickTargetId);
						if(targetObj == null){
							_self.hideSubmenu();
							_self.unbindDocumentMousedown("df83hasuh");
						}
					}
					else{
						_self.hideSubmenu();
						_self.unbindDocumentMousedown("df83hasuh");
					}
				}
			});
		}
	}

	////////////////////////////////////////////////
	// サブメニューの非表示
	_self.hideSubmenu=function(){
		if(_self.currentSubmenu != null){
			_self.jqSubmenu.hide();
			_self.currentSubmenu = null;
		}
	}
	

	this.mouseOverImage=function(obj){
		obj.src = obj.src.replace('.', '_over.');
	}
	
	this.mouseOutImage=function(obj){
		obj.src = obj.src.replace('_over.', '.');
	}
	
	////////////////////////////////////////////////
	// サブウィンドウ検索の表示
	this.subWindowSelectList=[];
	this.showSearchSubWindow=function(url, searchForm, listItems, height, selectAction, loadOnSearch, sort, direction, multi, escape){
		var _self = this;
			
		// トータルの長さを取得 
		// (padding 6 + border 1) * col数 4 + 左 border 1 = total
		var total = 40 + 6 + 1;
		var th = "<th width='40'>選択</th>";
		for(var i=0; i< listItems.length; i++){
			total += listItems[i]['width'] + 6 + 1;
			th += '<th width="' + listItems[i]['width'] + '">' + listItems[i]['label'] + '</th>'
		}
		total += 1;

		var html = '';
		if(searchForm != null && searchForm != ''){
			html += '<div style="overflow:auto;">'
				+ '<div class="sub_window_search_input"  style="width:' + (total -2 -10 - 30) + 'px;">'
				+ '<form name="sub_window_search_form" id="sub_window_search_form">'
				+ searchForm
				+ '</form>'
				+ '</div>'
				+ '<div class="sub_window_search_exec">'
				+ '<input type="image" src="img/subwindow_icon/search.png">'
				+ '</div>'
				+ '</div>'
			}

			html += '<table cellspacing="0" cellpadding="0" border="0" width="' + total + '" class="sub_window_search">'
				+ '<tr>'
				+ th
				+ '</tr>'
				+ '</table>'

				+ '<div class="sub_window_search" style="width:' + (total -2) +'px;height:' + height + 'px;">'
				+ '<table cellspacing="0" cellpadding="0" border="0" width="57">'
				+ '</table>'
				+ '</div>'

				+ '<div class="sub_window_search_page">'

				+ '<span>1</span>'
				+ '</div>'

		rainy3.showMessageWindow(
				'OK_ONLY', 
				"", 
				// padding:5px分減算
				html
				, 
		function(){
			if(multi){
				new selectAction(_self.subWindowSelectList);
			}
		},null);

		_self.subSearchSort = sort;
		_self.subSearchDirection = direction;

		_self.setButtonAction('sub_window_search_button');
/*
		$('div.sub_window_search_control input').click(function(){
			_self.subSearchPage=1;
			_self.execSearchSubWindow(url, listItems, selectAction)
		})
*/	
		input = $('div.sub_window_search_input');
		exec = $('div.sub_window_search_exec');
		height = input.height() + 10;
		exec.height(height);
		img = $('div.sub_window_search_exec input');
		img.css('margin-top', "" + ((height - img.height()) / 2) + "px");
		
		exec.mouseover(
			function(){
				exec.addClass('sub_window_search_exec_over');
			}
		);
		exec.mouseout(
			function(){
				exec.removeClass('sub_window_search_exec_over');
			}
		);

		exec.click(function(){
			_self.subSearchPage=1;
			_self.execSearchSubWindow(url, listItems, selectAction)
			exec.addClass('sub_window_search_exec_click');
		})
		exec.mouseup(function(){
				exec.removeClass('sub_window_search_exec_click');
		})
		
		if(loadOnSearch){
			_self.subSearchPage=1;
			_self.execSearchSubWindow(url, listItems, selectAction, multi, escape)
		}
	}
	
	////////////////////////////////////////////////
	// サブウィンドウ検索の実行
	this.execSearchSubWindow=function(url, headers, selectAction, multi, escape){
		var _self = this;

		var json = null;
		// フォームオブジェクトの取得
		if(document.getElementById('sub_window_search_form') != null){
			json = app.formParam2jsonData('sub_window_search_form', true)
		}

		var dataMap = {};

		// 検索条件にpage,sort,directionは利用不可
		dataMap={
				page:_self.subSearchPage,
				sort:_self.subSearchSort,
				direction:_self.subSearchDirection
		}


		if(json != null){
			for(var i in json){
				dataMap[i] = json[i];
			}
		}
		
		var appendTo = $('div.sub_window_search table');

		// HTTPリクエスト
		this.request(url, dataMap, false, function(head, body){
			// 成功時の処理結果

			// ページ情報の取得
			var pageNum = parseInt(head.find('page_num').text());
			var pageSize = parseInt(head.find('page_size').text());
			var totalSize = parseInt(head.find('total_size').text());
			var dataCount = ((pageNum - 1) * pageSize) + 1;

			// ページ番号を更新

			_self.subSearchPage = parseInt(pageNum);

			// 一覧を削除する
			$('div.sub_window_search table tr').remove();
			var grid = '';
			body.find('row').each(
				function(i){

					var params = $(this);
					var toggle = "odd";
					if(i % 2 != 0){
						toggle = "even";
					}

					grid += '<tr id="' + params.attr('id') + SUBSEARCH_TR_ID_SUFFIX + '">';

					grid += '<td class="' + toggle + ' select" width="40">'
					
					if(multi != null && multi){
						grid += '<input type="checkbox" style="margin-top:5px;"'
						for(var i=0; i<_self.subWindowSelectList.length; i++){
							if(params.attr('id') == _self.subWindowSelectList[i]['id']){
								grid  += ' checked ';
							}
						}
						grid  += '>';
						
					}
					else{
						grid += '<a href="javascript:void(0);"><img src="img/subwindow_icon/select.png" onMouseOver="this.src=this.src.replace(\'.png\', \'_over.png\')" onMouseOut="this.src=this.src.replace(\'_over.png\', \'.png\')"></a>';
					}
					grid += '</td>';
					for(var j=0; j<headers.length; j++){
							var end = '';
							if(headers.length-1 == j){
								end = ' end';
							}

						grid += '<td class="' + toggle + end + '" style="text-align:' + headers[j]['postion'] + ';width:' + headers[j]['width'] + 'px;">' + params.find(headers[j]['id']).text() + '</td>';
					}
					grid += '</tr>';
				}
			);
			appendTo.append(grid);

			// マウスオーバー処理
			$('div.sub_window_search table tr').mouseover(function(e){

				var obj = app.getParentNode(e.target, 'TR');
				if(obj != null){
					$(obj).children('td').addClass("hover")
				}

			});
			$('div.sub_window_search table tr').mouseout(function(e){
				var obj = app.getParentNode(e.target, 'TR');
				if(obj != null){
					$(obj).children('td').removeClass("hover")
				}
			});

			if(multi != null && multi){
				
				$('div.sub_window_search table td.select input[type=checkbox]').click(function(e){

					var obj = app.getParentNode(e.target, 'TR');
					var id = obj.id.replace(SUBSEARCH_TR_ID_SUFFIX, '');
					if(obj != null){
						if(e.target.checked){
							var fields = {}
							for(var j=0; j<headers.length; j++){
								
								var val = $($(obj).children('td').get(j+1)).text();
								if(escape != null && escape){
									val = app.htmlEscape(val);
								}
								fields[headers[j]['id']] = val;
							}
							_self.subWindowSelectList.push({id:id, fields:fields});
							}
						else{
							for(var i=0;i<_self.subWindowSelectList.length; i++){
								if(_self.subWindowSelectList[i]['id'] == id){
									_self.subWindowSelectList.splice(i, 1);
									break;
								}
							}
						}
					}
				});
			}
			else{
				$('div.sub_window_search table td.select img').click(function(e){
					var obj = app.getParentNode(e.target, 'TR');
					if(obj != null){
						var id = obj.id.replace(SUBSEARCH_TR_ID_SUFFIX, '');
						var fields = {}
						
						for(var j=0; j<headers.length; j++){
							var val = $($(obj).children('td').get(j+1)).text();
							if(escape != null && escape){
								val = app.htmlEscape(val);
							}
							fields[headers[j]['id']] = val;
						}
						new selectAction(id, fields);
					}
					
				});				
			}
			


			// ページングの設定
			var start = 1;
			var end = 999;
			var prev = null;
			var next = null;
	
			// 内容クリア
			var pageInfo =$('div.sub_window_search_page')
			pageInfo.empty();

			// 全ページ数を求める
			var totalPageCount = Math.ceil(totalSize / pageSize);
			// 現在のページ番号から表示する範囲を決定する
			// 1～4の場合は、1～（最大）10までを表示
			// 最大-4～最大の場合は、最大-9～最大までを表示
			// 上記以外の場合
				// 現在のページ-4～現在のページ+5（最大）まで表示する
			if(pageNum >= 1 && pageNum <= 5){
				start = 1;
				if(totalPageCount > 10){
					end = 10;
				}
				else{
					end = totalPageCount;
				}
				// <は表示しない
			}
			else if(pageNum >= totalPageCount-5 && pageNum <= totalPageCount){
				start = totalPageCount - 9;
				if(start < 1){
					start = 1;
				}
				end = totalPageCount;
				// >は表示しない
			}
			else{
				start = pageNum - 4;
				if(start < 1){
					start = 1;
				}
				end = pageNum + 5;
				if(end > totalPageCount){
					end = totalPageCount;
				}
			}
	
			if(pageNum > 1){
				prev = pageNum - 1;
			}
			if(pageNum < totalPageCount){
				next = pageNum + 1;
			}
	

	
			if(prev != null){
				var page = $('<span><a href="javascript:void(0);" onclick="rainy3">＜</a></span>').appendTo(pageInfo)
				page.children('a').click(function(){
					_self.subSearchPage=prev;
					_self.execSearchSubWindow(url, headers, selectAction);
				})
			}
			
			var path = false;
			for(var i=start; i<=end; i++){
				if(i == pageNum){
					var page = $('<span>' + i + '</span>').appendTo(pageInfo)
				}
				else{
					var page = $('<span><a href="javascript:void(0);" onclick="rainy3">' + i + '</a></span>').appendTo(pageInfo)
					page.children('a').click(function(e){
						_self.subSearchPage=parseInt($(e.target).text());
						//_self.subSearchPage=parseInt(page.children('a').text())
						_self.execSearchSubWindow(url, headers, selectAction);
					})
				}
				path = true;
			}
			
			if(!path){
				 $('<span style="visibility:hidden;">1</span>').appendTo(pageInfo)
			}
			
			
			if(next){
				var page = $('<span><a href="javascript:void(0);" onclick="rainy3">＞</a></span>').appendTo(pageInfo)
				page.children('a').click(function(){
					_self.subSearchPage=next;
					_self.execSearchSubWindow(url, headers, selectAction);
				})
			}

		}, 
		function(){
			/* ユーザーエラー */
		},
		function(){
			/* 送信前処理 */
		},
		function(head, body){
			/* 送信後処理 */
		});
	}
	
	////////////////////////////////////////////////
	// リサイズ時に実行する関数
	this.setResizeFunction=function(func){
		this.resizeFunction=func;
	}
	
}

/****************************************************************
 * サブメニューオブジェクト
 ****************************************************************/
var Rainy3Submenu = function(_width, _height, _html, _loadaction){
	this.type='submenu';
	this.id="menu" + (Math.random() + 100) + (Math.random() + 100) + (Math.random() + 100);
	this.html = _html;
	this.loadaction=_loadaction;
	this.preloadaction = null;
	this.top1=0;
	this.left2=0;
	this.top2=0;
	this.left2=0;
	this.width=_width;
	this.height=_height;
	this.clickTargetId=null;

	// 表示方向 "width" or "height"
	this.showDirection="width";

	this.setSize=function(width, height){
		this.width = width;
		this.height=height;
	}
	
	// 表示位置のセット
	// 候補1と候補2をセットする
	this.setPosition=function(direction, left1, top1, left2, top2){

		this.direction=direction;
		this.top1 = top1;
		this.left1 = left1;
		this.top2 = top2;
		this.left2 = left2;
	}

	// クリックした場所のID
	// mousedownイベントの対象からはずす
	this.setClickTargetId=function(clickTargetId){
		this.clickTargetId = clickTargetId;
	}

	this.setHtml=function(_html){
		this.html = _html;
	}
	this.addHtml=function(_html){
		if(this.html == null || this.html == 'undefined'){
			this.html = "";
		}
		this.html += _html;
	}

	this.setLoadAction=function(_loadaction){
		this.loadaction=_loadaction;
	}
	this.setPreLoadAction=function(_loadaction){
		this.preloadaction=_loadaction;
	}

	this.show=function(){
		rainy3.showSubmenu(this.id, this.html, this.width, this.height, this.direction, this.left1, this.top1, this.left2, this.top2, this.preloadaction, this.loadaction, this.clickTargetId);
	}
	this.hide=function(){
		rainy3.hideSubmenu();
	}
}

/****************************************************************
 * 基本ヘッダーオブジェクト
 ****************************************************************/
var Rainy3Header = function(){

	this.handler = this;
	this.type='header';

	var CREATE_OBJECT_ID_PREFIX="72hdshdure_";
	var CREATE_OBJECT_ID_SUFFIX_CONTENT_HEADER="_ja9hudeep";

	this.id = null;
	this.jquery = null;
	this.height = 0;
	this.border_top = 0;
	this.border_bottom = 0;
	this.border_left = null;
	this.border_right = null;
	this.padding_left = null;
	this.padding_right = null;
	this.parentWindowId=null

	////////////////////////////////////////////////
	// 初期化
	this.init=function(obj, _type){
		this.handler = obj;
		this.type = _type;
	}

	////////////////////////////////////////////////
	//ヘッダーの描画
	this.draw=function(parentId, headerInfo, jqTarget){
		var _self = this.handler;

		_self.id= CREATE_OBJECT_ID_PREFIX + parentId + CREATE_OBJECT_ID_SUFFIX_CONTENT_HEADER;
		_self.jquery = $('<div id="' + _self.id + '" class="content_header"></div>').insertBefore(jqTarget);

		var content = headerInfo.html();
		// ヘッダー内部コンテンツの追加
		var hcontent = _self.addContent(_self.id, _self.jquery, content);

		// ヘッダー描画情報の生成
		_self.height = _self.jquery.height();
		_self.border_top = app.px2num(_self.jquery.css("border-top-width"));
		_self.border_bottom = app.px2num(_self.jquery.css("border-bottom-width"));
		_self.border_left = app.px2num(_self.jquery.css("border-left-width"));
		_self.border_right = app.px2num(_self.jquery.css("border-right-width"));
		_self.padding_left = app.px2num(_self.jquery.css("padding-left"));
		_self.padding_right = app.px2num(_self.jquery.css("padding-right"));


	}

	////////////////////////////////////////////////
	// ヘッダーコンテンツの追加
	// このクラスを拡張する場合は、以下のメソッドをオーバーライドする
	this.addContent=function(id, appendTo, headerInfo){
		var _self = this.handler;
		appendTo.append(headerInfo);
	}
}

/****************************************************************
 *コントロール付きヘッダーオブジェクト
 ****************************************************************/
var Rainy3ControlHeader = function(){

	this.controls = {};
	this.rand = Math.random();
	this.parentWindowId=null;

	////////////////////////////////////////////////
	// 初期化
	this.init(this, 'control_header');

	////////////////////////////////////////////////
	// ヘッダーコンテンツの追加
	this.addContent=function(id, appendTo, headerInfo){
		var _self = this;
		var hcontent = headerInfo;
		var header = null;
		eval(hcontent);
		if(header == null){
			alert('Please define "var header={}"');
		}
		else {
			var jsonData = header;
			hcontent = "";
			if(jsonData['title'] != null && jsonData['title'] != ''){
				hcontent += '<div class="title"';
				if(jsonData['title_width'] != null){
					hcontent += 'style="width:' + jsonData['title_width'] + 'px"';

				}
				hcontent += '>' + jsonData['title'] + '</div>';
			}

			if(jsonData['title'] != null && jsonData['title'] != '' && jsonData['controls'] != null && jsonData['controls'].length > 0){
				hcontent += '<div class="delim_l"></div><div class="delim_r">';
			}

			// ヘッダー内部コンテンツの追加（タイトルと区切り）
			var jqHeaderTitle = $(hcontent).appendTo(appendTo);

			if(jsonData['controls'] != null && jsonData['controls'].length > 0){
				for(var i=0; i<jsonData['controls'].length; i++){

					var r3control = new Rainy3Control();
					r3control.parentWindowId = this.parentWindowId;
					var controlid = r3control.draw(id, appendTo, jsonData['controls'][i]);
					_self.controls[controlid] = r3control;
				}
			}
		}
	}

	////////////////////////////////////////////////
	// コントロールの取得
	this.getControl=function(controlid){
		var _self = this;
		return _self.controls[controlid];
	}

}
Rainy3ControlHeader.prototype = new Rainy3Header();

/****************************************************************
 * コントロールオブジェクト
 ****************************************************************/
var Rainy3Control = function(){

	var _self = this;

	_self.id = null;
	_self.jquery = null;
	_self.icon = null;
	_self.disabledicon = null;

	_self.aObj = null;
	_self.parentButtonObj = null;
	_self.parentImgObj = null;
	_self.parentLabelObj = null;

	_self.childImgObj = null;
	_self.childLabelObj = null;

	_self.currentStatus = ''
	_self.clickEvent = null;
	_self.submenu = null;
	_self.parentWindowId=null;

	////////////////////////////////////////////////
	//ヘッダーの描画
	_self.draw=function(parentId, jqHeaderObj, jsonData){

		_self.clickEvent = jsonData['click'];
		_self.submenu = jsonData['submenu'];

		_self.icon = jsonData['icon'];
		_self.disabledicon = jsonData['disabledicon'];

		// 生成後、マウスオーバーで表示する枠を追加（通常は透過度を最強にしておく）
		_self.id= parentId + '_' + jsonData['id'];

		hcontent = "";
		hcontent += '<a href="javascript:void(0);" class="'
		if(jsonData['initdisable']){
			hcontent += 'disabled';
			_self.currentStatus = 'disabled';
		}
		else{
			hcontent += 'control';
			_self.currentStatus = 'active';
		}
		hcontent += '"></a>';
		
		_self.aObj = $(hcontent).appendTo(jqHeaderObj);
		_self.jquery = _self.aObj;

		hcontent =   '<div id="' + _self.id + "_button" + '" class="control_button"></div>';
		_self.parentButtonObj = $(hcontent).appendTo(_self.aObj);


		hcontent = '<img src="';
		if(jsonData['initdisable']){
			hcontent += _self.disabledicon;
		}
		else{
			hcontent += _self.icon;
		}
		hcontent += '">';
		_self.parentImgObj = $(hcontent).appendTo(_self.parentButtonObj);


		hcontent = '<span>' + jsonData['label'] + '</span>';
		_self.parentLabelObj = $(hcontent).appendTo(_self.parentButtonObj);

		$(window).bind('load', function(){
			var button =  _self.parentButtonObj;
			var width = button.width() + app.px2num(button.css('padding-left')) + app.px2num(button.css('padding-right'));
			var height = button.height();

			button.prepend('<div class="l" style="height:' + (height - 7) + 'px;"></div>');
			button.prepend('<div class="t" style="width:' + (width - 1) + 'px;"></div>');
			button.prepend('<div class="r" style="height:' + (height - 7) + 'px;left:' +  (width) + 'px;"></div>');
			button.prepend('<div class="b" style="width:' + (width - 1) + 'px;top:' + (height - 2) + 'px;"></div>');

			button.prepend('<div class="il" style="height:' + (height - 9) + 'px;"></div>');
			button.prepend('<div class="it" style="width:' + (width - 3) + 'px;"></div>');
			button.prepend('<div class="ir" style="height:' + (height - 9) + 'px;left:' +  (width - 1) + 'px;"></div>');
			button.prepend('<div class="ib" style="width:' + (width - 3) + 'px;top:' + (height - 3) + 'px;"></div>');

			button.prepend('<div class="lt"></div>');
			button.prepend('<div class="rt" style="left:' + (width - 1) + 'px;"></div>');
			button.prepend('<div class="lb" style="top:' + (height - 3) + 'px;"></div>');
			button.prepend('<div class="rb" style="top:' + (height - 3) + 'px;left:' + (width - 1) + 'px;"></div>');

			button.prepend('<div class="ilt" style=""></div>');
			button.prepend('<div class="irt" style="left:' + (width - 2) + 'px;"></div>');
			button.prepend('<div class="ilb" style="top:' + (height - 4) + 'px;"></div>');
			button.prepend('<div class="irb" style="top:' + (height - 4) + 'px;left:' + (width - 2) + 'px;"></div>');

			button.prepend('<div class="bg" style="width:' + (width -1) + 'px;height:' + (height - 7) + 	'px;"></div>');

			var obj = $('<div class="label"></div>').prependTo(button);



			_self.childImgObj = _self.parentImgObj.clone().appendTo(obj);
			_self.childLabelObj = _self.parentLabelObj.clone().appendTo(obj);

			_self.parentLabelObj.css('filter', 'alpha(opacity = 0)');
			_self.parentLabelObj.css('-moz-opacity', '0');
			_self.parentLabelObj.css('opacity', '0');

			_self.parentImgObj.css('filter', 'alpha(opacity = 0)');
			_self.parentImgObj.css('-moz-opacity', '0');
			_self.parentImgObj.css('opacity', '0');

		});


		_self.aObj.click(function(){
			_self.click();
		});

		return jsonData['id'];
	}

	////////////////////////////////////////////////
	// ボタン押下処理の実行
	_self.click=function(){
		if(_self.currentStatus == 'active' && _self.clickEvent != null){
			new _self.clickEvent();
		}
		else if(_self.currentStatus == 'active' && _self.submenu != null){
			// 画面内の位置取得
			// まずはウィンドウの右の位置を取得する
			var left = rainy3.getWindowLeft(this.parentWindowId);
			var top2 = rainy3.getWindowTop(this.parentWindowId);
			left = _self.aObj.get(0).offsetLeft;
			left = _self.parentButtonObj.get(0).offsetLeft;
			left += (_self.parentButtonObj.width() / 2);
			var top1 = top2 + _self.parentButtonObj.height();
			
			// サブメニューの表示
			_self.submenu.setPosition('height', left, top1, left, top2+7);
			_self.submenu.clickTargetId = _self.id + "_button";
			_self.submenu.show();
		}
	}


	////////////////////////////////////////////////
	// ボタン無効化／有効化処理の実行
	_self.disable=function(disableFlg){
		if(disableFlg){
			_self.jquery.removeClass('control');
			_self.jquery.addClass('disabled');
			if(_self.childImgObj != null){
				_self.childImgObj.attr('src', _self.disabledicon);
				_self.currentStatus = 'disabled';
			}
		}
		else{
			_self.jquery.removeClass('disabled');
			_self.jquery.addClass('control');
			if(_self.childImgObj != null){
				_self.childImgObj.attr('src', _self.icon);
				_self.currentStatus = 'active';
			}
		}
	}
}

/****************************************************************
 * 基本フッターオブジェクト
 ****************************************************************/
var Rainy3Footer = function(){

	this.handler = this;
	this.type='footer';

	var CREATE_OBJECT_ID_PREFIX="92hasdezde_";
	var CREATE_OBJECT_ID_SUFFIX_CONTENT_FOOTER="_ia3xu60ap";

	this.id = null;
	this.jquery = null;
	this.height = 0;
	this.border_top = 0;
	this.border_bottom = 0;
	this.border_left = null;
	this.border_right = null;
	this.padding_left = null;
	this.padding_right = null;
	this.parentWindowId=null

	////////////////////////////////////////////////
	// 初期化
	this.init=function(obj, _type){
		this.handler = obj;
		this.type = _type;
	}

	////////////////////////////////////////////////
	// フッターの描画
	this.draw=function(parentId, footerInfo, jqTarget){
		var _self = this.handler;

		_self.id= CREATE_OBJECT_ID_PREFIX + parentId + CREATE_OBJECT_ID_SUFFIX_CONTENT_FOOTER;
		_self.jquery = $('<div id="' + _self.id + '" class="content_footer"></div>').insertAfter(jqTarget);

		var content = footerInfo.html();
		// ヘッダー内部コンテンツの追加
		var hcontent = _self.addContent(_self.id, _self.jquery, content);

		// ヘッダー描画情報の生成
		_self.height = _self.jquery.height();
		_self.border_top = app.px2num(_self.jquery.css("border-top-width"));
		_self.border_bottom = app.px2num(_self.jquery.css("border-bottom-width"));
		_self.border_left = app.px2num(_self.jquery.css("border-left-width"));
		_self.border_right = app.px2num(_self.jquery.css("border-right-width"));
		_self.padding_left = app.px2num(_self.jquery.css("padding-left"));
		_self.padding_right = app.px2num(_self.jquery.css("padding-right"));

	}

	////////////////////////////////////////////////
	// ヘッダーコンテンツの追加
	// このクラスを拡張する場合は、以下のメソッドをオーバーライドする
	this.addContent=function(id, appendTo, footerInfo){
		var _self = this.handler;
		appendTo.append(footerInfo);
	}
}

/****************************************************************
 * ページングフッターオブジェクト
 ****************************************************************/
var Rainy3PagingFooter=function(){

	var IMG_PAGING_LEFT = 'img/paging/paging_left.png';
	var IMG_PAGING_RIGHT = 'img/paging/paging_right.png';

	this.pageNum = 1;
	this.pageSize = 20;
	this.totalSize = 360;
	this.direction = 'asc';
	this.clickAction=null;		// 引数ページ番号
	this.appendTo=null;

	////////////////////////////////////////////////
	// 初期化
	this.init(this, 'paging_footer');

	////////////////////////////////////////////////
	// 描画
	this.addContent=function(id, appendTo, footerInfo){
		// 初期表示しない
		this.appendTo=appendTo;
	}

	////////////////////////////////////////////////
	// ページ情報の描画
	this.showPageInfo=function(){
		var _self = this;
		var start = 1;
		var end = 999;
		var prev = null;
		var next = null;

		// 内容クリア
		this.appendTo.empty();

		// 全ページ数を求める
		var totalPageCount = Math.ceil(this.totalSize / this.pageSize);
		// 現在のページ番号から表示する範囲を決定する
		// 1～4の場合は、1～（最大）10までを表示
		// 最大-4～最大の場合は、最大-9～最大までを表示
		// 上記以外の場合
			// 現在のページ-4～現在のページ+5（最大）まで表示する
		if(this.pageNum >= 1 && this.pageNum <= 5){
			start = 1;
			if(totalPageCount > 10){
				end = 10;
			}
			else{
				end = totalPageCount;
			}
//			// <は表示しない
//			if(totalPageCount > 10){
//				next = end + 1;
//			}
		}
		else if(this.pageNum >= totalPageCount-5 && this.pageNum <= totalPageCount){
			start = totalPageCount - 9;
			if(start < 1){
				start = 1;
			}
			end = totalPageCount;
			// >は表示しない
//			if(start > 1){
//				prev = start - 1;
//			}
		}
		else{
			start = this.pageNum - 4;
			if(start < 1){
				start = 1;
			}
			
			end = this.pageNum + 5;
			if(end > totalPageCount){
				end = totalPageCount;
			}

//			if(start > 1){
//				prev = start - 1;
//			}
//
//			if(end < totalPageCount){
//				next = end + 1;
//			}
		}

		if(this.pageNum > 1){
			prev = this.pageNum - 1;
		}
		if(this.pageNum < totalPageCount){
			next = this.pageNum + 1;
		}


		if(prev != null){
			var obj = $('<div class="paging_link"><img src="' + IMG_PAGING_LEFT + '"></div>').appendTo(this.appendTo);
			_self.bindHover(obj);
			_self.bindClick(obj, prev);
		}

		for(var i=start; i<=end; i++){
			var className = 'paging_link';
			if(i == this.pageNum){
				className = 'paging_link_current';
			}
			var obj = $('<div class="' + className + '">' + i + '</div>').appendTo(this.appendTo);
			_self.bindHover(obj);
			_self.bindClick(obj, i);
		}
		if(next){
			var obj = $('<div class="paging_link"><img src="' + IMG_PAGING_RIGHT + '"></div>').appendTo(this.appendTo);
			_self.bindHover(obj);
			_self.bindClick(obj, next);
		}

		var rengeMin = 0;
		var rangeMax = 0;

		if(this.direction == 'asc'){
			rangeMin = this.pageNum*this.pageSize-this.pageSize+1;
			rangeMax = this.pageNum*this.pageSize;
			if(rangeMax > this.totalSize){
				rangeMax = this.totalSize;
			}
			if(rangeMax == 0){
				rangeMin = 0;
			}
			
		}
		else{

			rangeMin = this.totalSize - (this.pageNum*this.pageSize-this.pageSize);
			rangeMax = this.totalSize - (this.pageNum*this.pageSize) + 1;
			if(rangeMax < 1){
				rangeMax = 1;
			}
		}

		$('<div class="paging_guide">' + rangeMin + ' - ' + rangeMax + ' of ' + this.totalSize + '</div>').appendTo(this.appendTo);

	}

	////////////////////////////////////////////////
	// マウスオーバー処理
	this.bindHover=function(jqObj){
		jqObj.mouseover(function(e){
			var link = app.getParentJQueryObjectByClassName($(e.target), 'paging_link');
			if(link != null){
				link.addClass('paging_link_hover');
			}
		});
		jqObj.mouseout(function(e){
			var link = app.getParentJQueryObjectByClassName($(e.target), 'paging_link');
			if(link != null){
				link.removeClass('paging_link_hover');
			}
		});
	}

	////////////////////////////////////////////////
	// マウスオーバー処理
	this.bindClick=function(jqObj, pageNum){
		var _self = this;
		jqObj.click(function(e){
			var _pageNum = pageNum;
			if(_self.clickAction != null){
				new _self.clickAction(_pageNum);
			}
		});
	}
}
Rainy3PagingFooter.prototype = new Rainy3Footer();

/****************************************************************
 * 基本ウィンドウオブジェクト
 ****************************************************************/
var Rainy3Base = function(){

	this.handler = this;
//	var _self = this;
	var CONTENT_OBJECT_ID_SUFFIX_PROTECT="_zsdsohfd1";

	this.type='base';
	this.id = null;
	this.header = null;
	this.footer = null;
	this.jqTarget = null;
	this.jqParent = null;
	this.parentId = null;
	this.jqContentProtection = null;
	this.jqContentTabStop1 = null;
	this.jqContentTabStop2 = null;
	this.jqContentLoading = null;
	this.jqMessageBox=null;
	this.jqMessageBoxPadding=null;
	this.jqMessageBoxShadow=null;

	this.lastTotalHeight = null;
	this.lastTotalWidth = null;
	
	
	////////////////////////////////////////////////
	// create
	this.create=function(id){
		var _self = this.handler;

		_self.id = id;
		_self.jqTarget = $('#' + _self.id);
		_self.jqParent = _self.jqTarget.parent();
		_self.parentId = _self.jqParent.get(0).id;
		_self.jqTarget.addClass('content_body');

		_self.border_top = app.px2num(_self.jqTarget.css("border-top-width"));
		_self.border_bottom = app.px2num(_self.jqTarget.css("border-bottom-width"));
		_self.border_left = app.px2num(_self.jqTarget.css("border-left-width"));
		_self.border_right = app.px2num(_self.jqTarget.css("border-right-width"));

		_self.eventList = [];
		_self.jqTarget.css('display', 'block');


		// 画面保護オブジェクトの追加
		// タブストップオブジェクトの追加（before）
		_self.drawProtectionBefore(id);

		// ヘッダーの描画
		_self.drawHeader(id);

		// タブストップオブジェクトの追加（after）
		_self.drawProtectionAfter(id);

		// フッターの描画
		_self.drawFooter(id, _self.jqFooter);

		_self.drawContent(id);
		
		// イベントのバインド
		for(var i=0; i<_self.eventList.length; i++){
			$('#' + _self.eventList[i]['id']).bind(_self.eventList[i]['event'], _self.eventList[i]['function']);
		}
	}
	
	////////////////////////////////////////////////
	// 初期化
	this.init=function(obj, type){
		var _self = this.handler;

		this.handler = obj;
		this.type = type;
	}


	////////////////////////////////////////////////
	// ヘッダーの描画
	this.drawHeader=function(id){
		var _self = this.handler;

		// 標準ヘッダーオブジェクトの描画判定
		var header = _self.jqTarget.children('.r3header');
		if(header.length > 0){
			_self.header = new Rainy3Header();
			_self.header.parentWindowId=_self.parentId;
			_self.header.draw(id, header.eq(0), _self.jqTarget);

		}
		else{
			delete header;
			header = null;
		}

		// コントロールヘッダーオブジェクトの描画判定
		if(header == null){
			var header = _self.jqTarget.children('.r3control_header');
			if(header.length > 0){
				_self.header = new Rainy3ControlHeader();
				_self.header.parentWindowId=_self.parentId;
				_self.header.draw(id, header.eq(0), _self.jqTarget);
			}
			else{
				delete header;
				header = null;
			}
		}

		/** ヘッダーの拡張クラスを追加する場合には、以下に追加 */
		

		/** ここまで **/
		if(header != null){
			header.remove();
		}
	}

	////////////////////////////////////////////////
	// フッダーの描画
	this.drawFooter=function(id, footerInfos){
		var _self = this.handler;


		// フッターオブジェクトの描画判定
		var footer = _self.jqTarget.children('.r3footer');
		if(footer.length > 0){
			_self.footer = new Rainy3Footer();
			_self.footer.parentWindowId=_self.parentId;
			_self.footer.draw(id, footer.eq(0), _self.jqTarget);
		}
		else{
			delete footer;
			footer = null;
		}

		// ページングヘッダーオブジェクトの描画判定
		if(footer == null){
			var footer = _self.jqTarget.children('.r3paging_footer');
			if(footer.length > 0){
				_self.footer = new Rainy3PagingFooter();
				_self.footer.parentWindowId=_self.parentId;
				_self.footer.draw(id, footer.eq(0), _self.jqTarget);
			}
			else{
				delete footer;
				footer = null;
			}
		}

		/** フッターの拡張クラスを追加する場合には、以下に追加 */


		/** ここまで **/
		if(footer != null){
			footer.remove();
		}
	}

	////////////////////////////////////////////////
	// コンテンツの描画
	this.drawContent=function(id){

		var _self = this.handler;

		var contentInfo = _self.jqTarget.html();
		_self.jqTarget.empty();
		var content = _self.addContent(id, _self.jqTarget, contentInfo);
	}


	////////////////////////////////////////////////
	// コンテンツの取得
	// このクラスを拡張する場合は、本メソッドを拡張する
	this.addContent=function(id, jqTarget, contentInfo){
		var _self = this.handler;
		_self.jqTarget.append(contentInfo);
	}

	////////////////////////////////////////////////
	// フッターコンテンツの取得
	// このクラスを拡張する場合は、本メソッドを拡張する
	this.getFooterContent=function(footerInfo){
		return footerInfo.html();
	}
	 
	////////////////////////////////////////////////
	// オブジェクトボディのリサイズ
	this._resize=function(width, height){

		var _self = this.handler;
		if(width != null){

			_self.lastTotalWidth = width;

			if(_self.header != null){
				_self.header.jquery.width(width - _self.header.border_left - _self.header.border_right -  _self.header.padding_left - _self.header.padding_right);
			}
			if(_self.footer != null){
				_self.footer.jquery.width(width - _self.border_left - _self.border_right  -  _self.footer.padding_left - _self.footer.padding_righ);
			}
			_self.jqTarget.width(width - _self.border_left - _self.border_right);

			/*
			// 保護領域のリサイズ
			_self.jqContentProtection.width(width);
			_self.jqContentLoading.css('left', app.num2px(width / 2 - (rainy3.lobjDisplayLoding.totalWidth / 2)));
			_self.jqMessageBox.css('left', app.num2px(width / 2 - ((_self.jqMessageBox.width() + _self.jqMessageBoxPadding * 2)/ 2)));
			_self.jqMessageBoxShadow.css('left', app.num2px(width / 2 - ((_self.jqMessageBox.width() + _self.jqMessageBoxPadding * 2) / 2) + 5));
			*/
		}
		if(height != null){

			_self.lastTotalHeight = height;
			var totalHeight = height;
			if(_self.header != null){
				height -= (_self.header.height + _self.header.border_top + _self.header.border_bottom);
			}
			if(_self.footer != null){
				height -= (_self.footer.height + _self.footer.border_top + _self.footer.border_bottom);
			}
			_self.jqTarget.height(height - _self.border_top - _self.border_bottom);

			/*
			// 保護領域のリサイズ
			_self.jqContentProtection.height(totalHeight);
			_self.jqContentLoading.css('top', app.num2px(totalHeight / 2 - (rainy3.lobjDisplayLoding.totalHeight / 2)));
			_self.jqMessageBox.css('top', app.num2px(totalHeight / 2 - ((_self.jqMessageBox.height() + _self.jqMessageBoxPadding * 2)/ 2)));
			_self.jqMessageBoxShadow.css('top', app.num2px(totalHeight / 2 - ((_self.jqMessageBox.height() + _self.jqMessageBoxPadding * 2)/ 2) + 5));
			*/
		}

		_self._resizeProtection(width, height);

		_self.resize(width, height);
	}
	// 拡張用
	this.resize=function(width, height){
	}
	// loadingアイコンとメッセージボックスの位置変更
	this._resizeProtection=function(width, height){

		var _self = this.handler;
		if(width != null){
			// 保護領域のリサイズ
			_self.jqContentProtection.width(width);
			_self.jqContentLoading.css('left', app.num2px(width / 2 - (rainy3.lobjDisplayLoding.totalWidth / 2)));
			_self.jqMessageBox.css('left', app.num2px(width / 2 - ((_self.jqMessageBox.width() + _self.jqMessageBoxPadding * 2)/ 2)));
			_self.jqMessageBoxShadow.css('left', app.num2px(width / 2 - ((_self.jqMessageBox.width() + _self.jqMessageBoxPadding * 2) / 2) + 5));
		}

		if(height != null){
			// 保護領域のリサイズ
			_self.jqContentProtection.height(height);
			_self.jqContentLoading.css('top', app.num2px(height / 2 - (rainy3.lobjDisplayLoding.totalHeight / 2)));
			_self.jqMessageBox.css('top', app.num2px(height / 2 - ((_self.jqMessageBox.height() + _self.jqMessageBoxPadding * 2)/ 2)));
			_self.jqMessageBoxShadow.css('top', app.num2px(height / 2 - ((_self.jqMessageBox.height() + _self.jqMessageBoxPadding * 2)/ 2) + 5));
		}
	}



	////////////////////////////////////////////////
	// コンテンツ内のオブジェクトのIDを生成
	this.getObjectId=function(id){
		var _self = this.handler;
		return _self.id + "_" + id;
	}

	////////////////////////////////////////////////
	// コンテンツ内のオブジェクトのIDを生成
	this.setEvent=function(_event, id, _function){
		var _self = this.handler;
		_self.eventList.push({event:_event, id:id, 'function':_function});
	}
	
	
	////////////////////////////////////////////////
	// コンテンツの保護の描画1
	this.drawProtectionBefore=function(id){
		var _self = this.handler;
		var pos = $('<div class="overlay_base_position"></div>').insertBefore(_self.jqTarget);
		_self.jqContentTabStop1=$('<input type="text" class="content_tab_stop"/>').appendTo(pos);
		_self.jqContentProtection = $('<div id="' + (id + CONTENT_OBJECT_ID_SUFFIX_PROTECT) + '" class="content_protection"></div>').appendTo(pos);
		_self.jqContentLoading = $('<img src="img/loading.gif" class="content_protection_loading">').appendTo(pos);
		_self.jqMessageBox =  $('<div class="content_protection_message"></div>').appendTo(pos);
		_self.jqMessageBoxPadding = app.px2num(_self.jqMessageBox.css('padding-left'));
		_self.jqMessageBoxShadow =  $('<div class="content_protection_message_shadow"></div>').appendTo(pos);


		_self.setTabStop(_self.jqContentTabStop1);
	}

	////////////////////////////////////////////////
	// コンテンツの保護の描画2
	this.drawProtectionAfter=function(id){
		var _self = this.handler;
		var pos = $('<div class="overlay_base_position"></div>').insertAfter(_self.jqTarget);
		_self.jqContentTabStop2=$('<input type="text" class="content_tab_stop"/>').appendTo(pos);
		_self.setTabStop(_self.jqContentTabStop2);
	}

	////////////////////////////////////////////////
	// コンテンツの保護
	this.protection=function(_focus){
		var _self = this.handler;
		_self.jqContentProtection.show();
		_self.jqContentTabStop1.show();
		_self.jqContentTabStop2.show();
		_self.jqContentLoading.show();
		if(_focus){
			_self.jqContentTabStop1.focus();
		}

		var jqObj = $('#' + _self.parentId);
		_self._resizeProtection(jqObj.width(), jqObj.height());
	}

	
	////////////////////////////////////////////////
	// コンテンツの保護解除
	this.releaseProtection=function(){
		var _self = this.handler;
		_self.jqContentProtection.hide();
		_self.jqContentTabStop1.hide();
		_self.jqContentTabStop2.hide();
		_self.jqContentLoading.hide();
		_self.jqMessageBox.hide();
		_self.jqMessageBoxShadow.hide();

	}

	////////////////////////////////////////////////
	// コンテンツの保護（引数指定）
	this.protection2=function(jqContentProtection, jqContentTabStop1, jqContentTabStop2, jqContentLoading, jqContentTabStop1, _focus){
		var _self = this.handler;
		jqContentProtection.show();
		jqContentTabStop1.show();
		jqContentTabStop2.show();
		jqContentLoading.show();
		if(_focus){
			jqContentTabStop1.focus();
		}

		var jqObj = $('#' + _self.parentId);
		_self._resizeProtection(jqObj.width(), jqObj.height());

	}

	
	////////////////////////////////////////////////
	// コンテンツの保護解除（引数指定）
	this.releaseProtection2=function(jqContentProtection, jqContentTabStop1, jqContentTabStop2, jqContentLoading, jqContentTabStop1, jqMessageBox, jqMessageBoxShadow){
		var _self = this.handler;
		jqContentProtection.hide();
		jqContentTabStop1.hide();
		jqContentTabStop2.hide();
		jqContentLoading.hide();
		jqMessageBox.hide();
		jqMessageBoxShadow.hide();
	}

	////////////////////////////////////////////////
	// 完了メッセージの表示
	this.showCompleateMessage=function(jqContentLoading, jqMessageBox, jqMessageBoxShadow, message){
		var _self = this.handler;
		jqContentLoading.hide();
		jqMessageBox.html(message);

		jqMessageBoxShadow.width(jqMessageBox.width());
		jqMessageBoxShadow.height(jqMessageBox.height());
		jqMessageBox.show();
		jqMessageBoxShadow.show();

		var jqObj = $('#' + _self.parentId);
		_self._resizeProtection(jqObj.width(), jqObj.height());
	}

	////////////////////////////////////////////////
	// タブストップ処理
	this.setTabStop = function(tabStop){
		var _self = this.handler;
		tabStop.keydown(
			function(e){
				if(e.keyCode == KEY.TAB){
					return false;
				}
			}
		);
		tabStop.keypress(
			function(e){
				if(e.keyCode == KEY.TAB){
					return false;
				}
			}
		);
	}

	////////////////////////////////////////////////
	// ヘッダーの取得
	this.getHeader=function(){
		return this.header;
	}

}
rainy3.rainy3ObjectList.push({key:'r3base', _class:Rainy3Base});


/****************************************************************
 * メニューウィンドウオブジェクト
 ****************************************************************/
var Rainy3Menu = function(id){
	var _self = this;
	var ICON_MARGIN = 5;
	
	////////////////////////////////////////////////
	// 初期化
	_self.init(this, 'menu');

	////////////////////////////////////////////////
	// コンテンツの表示（基本的にjson、そうでない場合はそのまま表示）
	_self.addContent=function(id, jqTarget, contentInfo){

		var ret = "";

		var jsonText = contentInfo;
		var meun = null;
		eval(jsonText);

		if(menu == null){
			alert('Please define "var menu=[]"');
		}
		else{
			var jsonData = menu;
			// 親ウィンドウの幅を取得
			var parentWidth = jqTarget.width();

			// jsonDataを処理して、html化する
			for(var i=0; i<jsonData.length-1; i++){
				var iconWidth = jqTarget.width() - (ICON_MARGIN * 2);
				var iconMargin = ICON_MARGIN;
				ret += _self.getMenyIcon(jqTarget, iconWidth, iconMargin, jsonData[i], null);
			}
			jqTarget.append(ret);
		}
	}

	////////////////////////////////////////////////
	// メニューアイコンの取得
	_self.getMenyIcon = function(jqTarget, size, margin, option) {

		var ret = "";
		var href='javascript:void(0);';
		var target=null;
		if( option['link'] != null){
			href = option['link'];
		}
		else if(option['click'] != null){
			_self.setEvent("click", _self.getObjectId(option['id']), option['click']);
		}
		else if(option['submenu'] != null){

			var submenu = new Rainy3Submenu(10, 10, "");

			// クリックイベントでサブメニューを表示する
			// 縦5を基準として、それを超える場合は横に増やす
			_self.setEvent("click", _self.getObjectId(option['id']), function(e){

				// 親オブジェクトの取得
				var obj = app.getParentJQueryObjectByClassName($(e.target), 'frame');

				var top = obj.get(0).offsetTop + size / 2;
				// メニューの左位置を取得
				var left2 = rainy3.getWindowLeft(_self.parentId);
				var left1 = left2 + _self.lastTotalWidth;

				submenu.setPosition('width', left1, top, left2, top);

				var wCount = Math.floor((option['submenuList'].length - 1) / 5);
				if((option['submenuList'].length - 1) % 5 != 0){
					wCount++;
				}
				var hCount = Math.floor((option['submenuList'].length - 1) / wCount);
				if((option['submenuList'].length - 1) % wCount != 0){
					hCount++;
				}

				var subwidth = 0;
				for(var i=0; i<wCount; i++){
					if(i==0){
						subwidth += size;
					}
					else{
						subwidth += size + ICON_MARGIN;
					}
				}

				submenu.setSize(subwidth, (size + ICON_MARGIN) * hCount + ICON_MARGIN);

				var html = '';

				for(var i=0; i< (option['submenuList'].length-1); i++){
					var margin = ICON_MARGIN;
					if(i==0 || i % wCount == 0){
						margin = 0;
					}

					html += '<div style="width:' + size+ 'px;height:' + size+ 'px;margin-top:' + ICON_MARGIN + 'px;margin-left:' + margin + 'px;float:left;">';
					html += '<div style="position:relative;width:' + size+ 'px;height:' + size+ 'px;">';

					var iconWidth = jqTarget.width() - (ICON_MARGIN * 2);
					var iconMargin = 0;
					html += _self.getMenyIcon(jqTarget, iconWidth, iconMargin, option['submenuList'][i]);

					html += '</div>';
					html += '</div>';

				}
				submenu.setHtml(html);
				submenu.setClickTargetId('' + _self.getObjectId(option['id']) + '_frame');
				submenu.setLoadAction(function(){
//					$('.r3menu_link').powerTip({followMouse:true, smartPlacement:true, offset:15, fadeInTime:300});

					for(var i=0; i<(option['submenuList'].length - 1); i++){
						if(option['submenuList'][i]['click'] != null){
							$('#' + _self.getObjectId(option['submenuList'][i]['id'])).click(option['submenuList'][i]['click']);
						}
					}

				});
				submenu.show();
			});
		}

		if( option['link'] != null && option['popup'] != null){
			target = option['popup'];
		}

		ret += '<a class="r3menu_icon r3menu_link" id="' + _self.getObjectId(option['id']) + '" href="' + href + '" title="' + option['label'] + '"';

		if(target != null){
			ret += 'target="' + target + '"';
		}
		ret += '>';

		ret += '<div class="frame" id="' + _self.getObjectId(option['id']) + '_frame" style="margin-top:' + margin + 'px;margin-left:' + margin + 'px;width:' + size + 'px;height:' + size + 'px;';
		ret += '">';

		ret += '<div class="l" style="height:' + (size - 2) + 'px;"></div>';
		ret += '<div class="t" style="width:' + (size - 2) + 'px;"></div>';
		ret += '<div class="r" style="height:' + (size - 2) + 'px;left:' + (size - 1) + 'px;"></div>';
		ret += '<div class="b" style="width:' + (size - 2) + 'px;top:' + (size - 1) + 'px;"></div>';

		ret += '<div class="il" style="height:' + (size - 3) + 'px;"></div>';
		ret += '<div class="it" style="width:' + (size - 3) + 'px;"></div>';
		ret += '<div class="ir" style="height:' + (size - 4) + 'px;left:' + (size - 2) + 'px;"></div>';
		ret += '<div class="ib" style="width:' + (size - 4) + 'px;top:' + (size - 2) + 'px;"></div>';

		ret += '<div class="lt"></div>';
		ret += '<div class="rt" style="left:' + (size - 2) + 'px;"></div>';
		ret += '<div class="lb" style="top:' + (size - 2) + 'px;"></div>';
		ret += '<div class="rb" style="top:' + (size - 2) + 'px;left:' + (size - 2) + 'px;"></div>';

		ret += '<div class="ilt"></div>';
		ret += '<div class="irt" style="left:' + (size - 3) + 'px;"></div>';
		ret += '<div class="ilb" style="top:' + (size - 3) + 'px;"></div>';
		ret += '<div class="irb" style="top:' + (size - 3) + 'px;left:' + (size - 3) + 'px;"></div>';
		ret += '<div class="bg" style="width:' + (size - 2) + 'px;height:' + (size - 2) + 'px;"></div>';
		var imgMargin = (size  - parseInt(option['size'])) / 2;
		ret += '<img src="' +option['icon'] + '" width="' + option['size'] + '" height="' + option['size'] + '" style="position:absolute;margin-left:' + imgMargin + 'px;margin-top:' + imgMargin + 'px;">';

		if(option['submenuList'] != null && (option['submenuList'].length -1) != 0){
			ret += '<div style="position:absolute;top:' + (((size - 4)/2) - 5) + 'px;left:' + (((size - 4)) -10) + 'px;"><img src="img/sub_arrow.png"></div>';
			
		}


		if(option['label_image'] != null){
			ret += '<div class="title" style="top:' + (size + option['label_top']) + 'px;left:' + option['label_left'] + 'px;"><img src="' + option['label_image'] + '"></div>';
			
		}
		else{
			ret += '<div class="title" style="top:' + (size + option['label_top']) + 'px;left:' + option['label_left'] + 'px;font-weight:bold;color:#002;font-size:' + option['label_size'] + 'px;">' + option['label'] + '</div>';
		}

		ret += '</div>';
		ret += '</a>';

		return ret;
	}
	
	
}
Rainy3Menu.prototype = new Rainy3Base();
rainy3.rainy3ObjectList.push({key:'r3menu', _class:Rainy3Menu});


/****************************************************************
 * グリッドウィンドウオブジェクト
 ****************************************************************/
var Rainy3Grid = function(id){
	var _self = this;

	var CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA="_isasesfd1"
	var CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX="_issyusfd1"
	var CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDFIXED="_reesdjkdd"
	var CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA_TR="_isjiesfdw"
	var CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA_TH="_ssdix0adw"
	var CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX_TR="_isjd0zfdw"
	var CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX_TK="_i0jd3dfdp"
	var CREATE_OBJECT_ID_SUFFIX_CONTENT_TH_RIGHT="_issd0p1dw"


	var ROW_HOVER_CLASS_NAME = " hover";
	var ROW_SELECTED_CLASS_NAME = " selected";

	// セルのスタイル定義（CSSと同期すること）
	var FIRST_CELL_WIDTH = 20;		// 先頭のCELLの幅
	var CELL_PADDING = 2;			// セルのパディング
	var CELL_BORDER = 1;			// セルのボーダー（RIGHTのみ）
	var CELL_HEIGHT = 20;			// セルの高さ
	var CELL_HANDLE_BOTTOM_HEIGHT = 5;	// リサイズ用の下ハンドル

	var MIN_WIDTH = 20;
	var MIN_HEIGHT = 10;

	// 画像パスのセット
	var IMG_BLANK ='img/blank.png';
	var IMG_ASC ='img/grid_asc.png';
	var IMG_DESC ='img/grid_desc.png';

	_self.currentSelectId=null;

	
	_self.table = null;
	_self.headerTable = null;
	_self.bkRowid = null;
	_self.gridHeader = null;
	_self.gridInfo = null;

	// URL
	_self.url=null;
	// ヘッダー情報
	_self.headers=null;
	// テーブル情報
	_self.table=null;
	// ページ番号
	_self.page=1;
	// ソートキー
	_self.sort=null;
	// ソート方向
	_self.direction=null;
	// 検索条件
	_self.condition={};


	////////////////////////////////////////////////
	// 初期化
	_self.init(this, 'grid');

	////////////////////////////////////////////////
	// gridの描画
	_self.addContent=function(id, jqTarget, contentInfo){
		
		var grid=null;
		eval(contentInfo);

		if(grid == null){
			alert('Please define "var grid={}"');
		}
		else{
			gridInfo = grid;
			_self.gridInfo = gridInfo;

			var gridPosition = $('<div class="overlay_base_position"></div>').insertBefore(_self.jqTarget);
			_self.gridHeader = $('<div class="r3grid_header">' + '</div>').appendTo(gridPosition);
			_self.gridFixed = $('<div class="r3grid_fixed">' + '</div>').appendTo(gridPosition);

			if(_self.gridInfo['fittorow'] ==null || _self.gridInfo['fittorow'] ==false){
				_self.gridIndex = $('<div class="r3grid_index">' + '</div>').appendTo(gridPosition);
			}

			var totalWidth = FIRST_CELL_WIDTH + (CELL_PADDING * 2) + CELL_BORDER;
			for(var i=0; i<gridInfo['props'].length; i++){
				totalWidth += parseInt(gridInfo['props'][i]['width']);
				totalWidth += 5;		// padding+border
			}
		
			// テーブルのヘッダー部の描画
			var table = $('<div style="width:' + totalWidth + 'px;" class="table" id="' + _self.id + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA + '"></div>').appendTo(_self.jqTarget);

			// URLの保持
			_self.url = gridInfo['url'];
			// テーブルの保持
			_self.table = table;
			// ヘッダーの保持
			_self.headers=gridInfo['props'];
			// ソートキー
			_self.sort=gridInfo['initsort'];
			// ソート方向
			_self.direction=gridInfo['initdirection'].toLowerCase();
			// 初期検索条件
			if(gridInfo['initcondition'] != null){
				_self.condition=gridInfo['initcondition'];
			}


			var tr = $('<div class="tr"></div>').appendTo(table);
			var firstTd = $('<div class="th" style="width:' + (FIRST_CELL_WIDTH) + 'px;"></div>').appendTo(tr);
			var head = "";
			for(var i=0; i<gridInfo['props'].length; i++){
				head += '<div class="th" style="width:' + (parseInt(gridInfo['props'][i]['width'])) + 'px;">';
				head += gridInfo['props'][i]['label'];
				head += '</div>';
			}
			var td = $(head).appendTo(tr);


			// 上部固定部
			var sortableThList = [];
			var tableHead = '<div style="width:' + (totalWidth) + 'px;" class="table"><div class="tr">';
			tableHead += '<div class="th" style="width:' + (FIRST_CELL_WIDTH) + 'px;"></div>';
			for(var i=0; i<gridInfo['props'].length; i++){

				var thId = _self.id + '_' + gridInfo['props'][i]['id'] +  CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA_TH;
				// ソートの可否判断
				if(gridInfo['props'][i]['sortable']){
					sortableThList.push({id:thId});
				}
				tableHead += '<div class="th" id="' +  thId + '" style="width:' + (parseInt(gridInfo['props'][i]['width'])) + 'px;position:relative;">';
				tableHead += '<table cellspacing="0" cellpadding="0" border="0"><tr><td>' + gridInfo['props'][i]['label'] + '</td><td>'
				if(_self.sort == gridInfo['props'][i]['id']){
					if(_self.direction == 'desc' || _self.direction == 'DESC'){
						tableHead += '<img src="' + IMG_DESC + '" width="10" height="18">';
					}
					else{
						tableHead += '<img src="' + IMG_ASC + '" width="10" height="18">';
					}
				}
				else{
					tableHead += '<img src="' + IMG_BLANK + '" width="10" height="18">';
				}
				tableHead += '</td></tr></table>';

				tableHead += '<div id="' + _self.id + '_' + gridInfo['props'][i]['id'] +  CREATE_OBJECT_ID_SUFFIX_CONTENT_TH_RIGHT + '" class="handle_right" style="top:1px;left:' + (parseInt(gridInfo['props'][i]['width']) - 1) + 'px;height:' + CELL_HEIGHT + 'px;"></div>';
				tableHead += '</div>';
			}
			tableHead += '</div></div>';

			_self.headerTable = $(tableHead).appendTo(_self.gridHeader);


			// resizeイベントのバインド
			_self.bindResizeXGrid();

			// ヘッダーソートのバインド
			_self.bindHeaderSort(sortableThList);

			// grid dataの読み込み
			if(gridInfo['initload'] == null || gridInfo['initload'] == true){
				_self.load();
			}
		}
	}


	////////////////////////////////////////////////
	// ヘッダーのソートイベントバインド
	_self.bindHeaderSort=function(sortableThList){
		for(var i=0; i<sortableThList.length; i++){
			var sortObj = $('#' + sortableThList[i]['id']);
			sortableThList[i]['img'] =  $('#' + sortableThList[i]['id'] + ' img');

			sortObj.css('cursor', 'pointer');
			sortObj.click(function(e){
				var target = app.getParentJQueryObjectByClassName($(e.target), 'th');
				if(target != null){
					for(var j=0; j<sortableThList.length; j++){
						if(sortableThList[j]['id'] == target.attr('id')){
							var key = target.attr('id').replace( _self.id + '_', '').replace(CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA_TH, '');
							var img = null;
							// すでにソート済みの場合
							if(key == _self.sort){
								if(_self.direction=='asc'){
									_self.direction='desc';
									img=IMG_DESC;
								}
								else{
									_self.direction='asc';
									img=IMG_ASC;
								}
							}
							// 新規ソートの場合
							else{
								_self.direction='asc';
								img=IMG_ASC;
							}
							_self.sort = key;

							// ソート画像を表示
							sortableThList[j]['img'].get(0).src=img;
						}
						else{
							// 画像をブランクに設定
							sortableThList[j]['img'].get(0).src='img/blank.png';
						}
					}
					// ページ番号を先頭に変更
					_self.page=1;
					// 検索の実行
					_self.load();
				}
			});
		}
	}



	////////////////////////////////////////////////
	// X方向リサイズイベントバインド
	_self.bindResizeXGrid=function(){

		// 左右のリサイズ
		$('.handle_right').mousedown(
			function(e){

				var delimId = e.target.id;
				var initPos = app.getMousePosition(e);

				var winHeight = _self.lastTotalHeight;
				var winTop = rainy3.getWindowTop(_self.parentId);

				// マウスカーソルの変更
				rainy3.styleProtection('cursor', 'col-resize');
				// リサイズ時に実行する処理がある場合は実行
				if(rainy3.resizeFunction != null){
					new rainy3.resizeFunction();
				}
			
				// マウス移動で枠線移動
				rainy3.bindProtection('mousemove', function(e){
					var movePos = app.getMousePosition(e);
					rainy3.jqXContentsResizeBorder.css('left', app.num2px(movePos.x - 2));
					return false;
				});

				// マウスアップイベントで、x-contentsリサイズ＆画面保護解除
				rainy3.bindDocumentMouseup('jfdaddaa', function(e){
					_self.execResizeXGrid(delimId, initPos, app.getMousePosition(e));
					rainy3.releaseProtection();

					// 枠線の非表示
					rainy3.jqXContentsResizeBorder.css('display', 'none');

					// バインドの解除
					rainy3.unbindDocumentMouseup('jfdaddaa');
				});

				// 画面保護
				rainy3.protection("clear", false, function(){

					// 枠線の表示
					// topをヘッダー分にする
					// heightをコンテンツ＋区切りの高さにする
					// x位置をマウスの押されてた位置-2pxにする

					rainy3.jqXContentsResizeBorder.css('display', 'block');
					rainy3.jqXContentsResizeBorder.css('top', app.num2px(winTop));
					var barHeight = winHeight - rainy3.lobjHeader.totalHeight - rainy3.lobjFooter.totalHeight;
					rainy3.jqXContentsResizeBorder.height(winHeight);
					rainy3.jqXContentsResizeBorder.css('left', app.num2px(initPos.x - 2));
				});

				return false;
			}
		);
	}

	////////////////////////////////////////////////
	// X方向リサイズ実行
	_self.execResizeXGrid=function(delimId, initPos, endPos){

		var cellId = delimId.replace(_self.id + '_', '');
		cellId = cellId.replace(CREATE_OBJECT_ID_SUFFIX_CONTENT_TH_RIGHT, '');
		var moveLen = endPos.x-initPos.x;

		var targetCount = 1;		// 最左列分
		// 加算する位置を取得
		for(var i=0; i<_self.gridInfo['props'].length; i++){
			if(_self.gridInfo['props'][i]['id'] == cellId){
				targetCount += i;
				break;
			}
		}

		// 移動距離算出（MIN_WIDTH以下には移動できない）
		trList = _self.table.children('.tr');
		for(var i=0; i<trList.length; i++){
			var tdList = $(trList[i]).children('.td,.tk,.th');
			for(var j=0; j<tdList.length; j++){
				if(targetCount == j){
					var cellWidth = $(tdList[j]).width();
					if(cellWidth + moveLen < MIN_WIDTH){
						moveLen = MIN_WIDTH - cellWidth ;
					}
					break;
				}
			}
		}

		// tableに加算
		var totalWidth = _self.table.width();
		_self.table.width(totalWidth + moveLen);
		_self.headerTable.width(totalWidth + moveLen);

		// 対象のtdに加算
		var cellWidth = null;
		for(var i=0; i<trList.length; i++){
			var tdList = $(trList[i]).children('.td,.tk,.th');
			for(var j=0; j<tdList.length; j++){
				if(targetCount == j){
					if(cellWidth == null){
						cellWidth = $(tdList[j]).width();
						cellWidth += moveLen;
					}
					$(tdList[j]).width(cellWidth);
				}
			}
		}

		// 対象のtdに加算
		trList = _self.headerTable.children('.tr');
		var cellWidth = null;
		for(var i=0; i<trList.length; i++){
			var tdList = $(trList[i]).children('.th');

			for(var j=0; j<tdList.length; j++){
				if(targetCount == j){
					if(cellWidth == null){
						cellWidth = $(tdList[j]).width();
						cellWidth += moveLen;
					}
					$(tdList[j]).width(cellWidth);
					handleList = $(tdList[j]).children('.handle_right');
					if(handleList != null){
						for(var k=0; k<handleList.length; k++){
							$(handleList[k]).css('left', app.num2px(cellWidth - 1))
						}
					}
				}
			}
		}

		// 元のjsonデータを変更

		for(var i=0; i<_self.gridInfo['props'].length; i++){
			if(targetCount == i+1){
				_self.gridInfo['props'][i]['width'] = "" + (cellWidth);
			}
		}
	}

	////////////////////////////////////////////////
	// スクロールバーの設定
	_self.setScrollBar=function(){
		_self.gridHeader.width(_self.jqTarget.get(0).clientWidth);
		_self.jqTarget.scroll(
			function(e){
				_self.gridHeader.get(0).scrollLeft = e.target.scrollLeft;

				if(_self.gridInfo['fittorow'] ==null || _self.gridInfo['fittorow'] ==false){
					_self.gridIndex.get(0).scrollTop = e.target.scrollTop;
				}
			}
		);
	}

	////////////////////////////////////////////////
	// テーブルデータのロード
	_self.load=function(loadaction){
		_self._load(loadaction, true)
	}
	////////////////////////////////////////////////
	// テーブルデータのロード
	_self._load=function(loadaction, action){

		// 条件の追加
		var dataMap = {};
		var headers = _self.headers;

		// 検索条件にpage,sort,directionは利用不可
		dataMap={
				page:_self.page,
				sort:_self.sort,
				direction:_self.direction
		}

		if(_self.condition != null){
			for(var i in _self.condition){
				dataMap[i] = _self.condition[i];
			}
		}

		// HTTPリクエスト
		rainy3.request(_self.url, dataMap, false, function(head, body){
			// 成功時の処理結果


			// ページ情報の取得
			var pageNum = parseInt(head.find('page_num').text());
			var pageSize = parseInt(head.find('page_size').text());
			var totalSize = parseInt(head.find('total_size').text());
			var dataCount = ((pageNum - 1) * pageSize) + 1;

			// ページ番号を更新
			_self.page = parseInt(pageNum);
			if(_self.page == 0){
				_self.page = 1;
			}

			// 現在の選択をクリア
			_self.deselectRow(_self.currentSelectId, action);

			// 2行目以降のGridDataを削除する
			var trList = _self.table.children('.tr');
			for(var i=1; i<trList.length; i++){
				$(trList[i]).remove();
			}

			if(_self.gridInfo['fittorow'] ==null || _self.gridInfo['fittorow'] ==false){
				// Grid Indexを削除する
				_self.gridIndex.empty();
			}

			// Grid Fixedを削除する
			$(_self.id + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDFIXED).remove();

			// Grid Indexの描画
			var gridIndex = '<div style="width:' + (FIRST_CELL_WIDTH + (CELL_PADDING * 2) + CELL_BORDER) + 'px;" class="table" id="' + _self.id + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX + '"><div class="tr even" height="' + CELL_HEIGHT + '"><div class="th" style="width:' + (FIRST_CELL_WIDTH) + 'px;"></div></div>';

			// Grid Dataの描画
			var gridData = '';
			var initHeight = CELL_HEIGHT;
			// 高さが指定されている場合は、初期高さを変更
			if(_self.gridInfo['height'] != null){
				initHeight = _self.gridInfo['height'];
			}

			body.find('row').each(
				function(i){
					var toggle = "odd";
					if(i % 2 != 0){
						toggle = "even";
					}
					var params = $(this);

					indexNo = dataCount;
					if(_self.direction == 'desc'){
						indexNo = parseInt(totalSize) - indexNo + 1;
					}
					dataCount++;

					gridData += '<div id="' + _self.id + '_' + params.attr('id') +  CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA_TR + '" class="tr ' + toggle + '">';

					gridData += '<div class="tk" style="width:' + FIRST_CELL_WIDTH + 'px;height:';
					if(_self.gridInfo['fittorow'] ==null || _self.gridInfo['fittorow'] ==false){
						gridData += '' + initHeight + 'px;';
					}
					else{
						gridData += 'auto;';
					}
					gridData += '">';

					gridData += indexNo;
					gridData += '</div>';

					gridIndex += '<div id="' + _self.id + '_' + params.attr('id') +  CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX_TR + '" class="tr ' + toggle + '">';
					gridIndex += '<div class="tk" style="width:' + FIRST_CELL_WIDTH + 'px;height:' + initHeight + 'px;position:relative;">';
					gridIndex += indexNo;

					gridIndex += '<div id="' + _self.id + '_' + params.attr('id') +  CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX_TK + '" class="handle_bottom" style="top:' + (initHeight + CELL_PADDING - CELL_HANDLE_BOTTOM_HEIGHT) + 'px;left0px;height:' + CELL_HANDLE_BOTTOM_HEIGHT + 'px;width:' + FIRST_CELL_WIDTH +'px;"></div>';
					gridIndex += '</div></div>';

					for(var j=0; j<headers.length; j++){

//						gridData += '<div class="td" style="text-align:' + headers[j]['postion'] + ';width:' + headers[j]['width'] + 'px;height:' + initHeight + 'px;">' + params.find(headers[j]['id']).text() + '</div>';


						var fieldid = _self.getFieldId(params.attr('id'),  headers[j]['id']);


						gridData += '<div id="' + fieldid + '" class="td" style="text-align:' + headers[j]['postion'] + ';width:' + headers[j]['width'] + 'px;height:';
						if(_self.gridInfo['fittorow'] ==null || _self.gridInfo['fittorow'] ==false){
							gridData += '' + initHeight + 'px;';
						}
						else{
							gridData += 'auto;';
						}
						gridData += '">' + params.find(headers[j]['id']).text() + '</div>';

					}
					gridData += '</div>';
				}
			);
			gridIndex += '</div>';
			var jqGridData = $(gridData).appendTo(_self.table);

			// fittorowが設定されている場合は、index列の表示を行わない
			if(_self.gridInfo['fittorow'] ==null || _self.gridInfo['fittorow'] ==false){
				$(gridIndex).appendTo(_self.gridIndex);
			}
			else{

				// 最大の高さのカラムを取得してそれにあわせて高さを設定する
				
				jqDataObjList = jqGridData.children();
//				lineCount = jqTrList.length / headers.length;
				var lineCount=0;
				var lineMaxHeight=CELL_HEIGHT;
				var lineChache=[];
				for(var i=0; i < jqDataObjList.length; i++){
	
	
//	app.debug(jqDataObjList.eq(i).height())
					var _jq=jqDataObjList.eq(i)				
					lineChache.push(_jq);

					if(lineMaxHeight < _jq.height()){
						lineMaxHeight = _jq.height();
					}
					
					if((i + 1)%(headers.length+1)==0){

//						if(lineCount != 0){
							
							for(var j=0; j<lineChache.length; j++){
								lineChache[j].height(lineMaxHeight);
							}
						lineCount++;
						lineMaxHeight=CELL_HEIGHT;
						for(var j=lineChache.length-1; j>=0; j--){
							lineChache.splice(j,1);
						}
					}
				}
			}
			

			// 左上固定部の描画
			var gridFixed = '<div id="' + _self.id + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDFIXED + '" style="width:' + (FIRST_CELL_WIDTH + (CELL_PADDING * 2) + 1) + 'px;" class="table">';		// border+1
			gridFixed+='<div class="tr odd">';
			gridFixed+='<div class="th" style="width:' + (FIRST_CELL_WIDTH) + 'px;height:' + (CELL_HEIGHT) + 'px;">';
			gridFixed+='</div>';
			gridFixed+='</div>';
			gridFixed+='</div>';
			$(gridFixed).appendTo(_self.gridFixed);

			// マウスオーバー処理
			if(_self.gridInfo['select'] != 'none'){
				$('#' + _self.id + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA + ' .tr').mouseover(function(e){
					var row = app.getParentJQueryObjectByClassName($(e.target), 'tr');
					if(row != null && row.attr('id') != null){
						var rowid = row.attr('id').replace(_self.id + '_', '');
						rowid = rowid.replace(CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA_TR, '');
						_self.mouseoverGrid(rowid);
					}
				});
	
				$('#' + _self.id + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX + ' .tr').mouseover(function(e){
					var row = app.getParentJQueryObjectByClassName($(e.target), 'tr');
					if(row != null){
						if(row.attr('id') != null){
							var rowid = row.attr('id').replace(_self.id + '_', '').replace(CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX_TR, '');
							_self.mouseoverGrid(rowid);
						}
					}
				});
	
				// マウスアウト処理
				$('#' + _self.id + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA + ' .tr').mouseout(function(e){
					var row = app.getParentJQueryObjectByClassName($(e.target), 'tr');
					if(row != null && row.attr('id') != null){
						var rowid = row.attr('id').replace(_self.id + '_', '').replace(CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA_TR, '');
						_self.mouseoutGrid(rowid);
					}
				});
	
				$('#' + _self.id + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX + ' .tr').mouseout(function(e){
					var row = app.getParentJQueryObjectByClassName($(e.target), 'tr');
					if(row != null){
						var rowid = row.attr('id').replace(_self.id + '_', '').replace(CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX_TR, '');
						_self.mouseoutGrid(rowid);
					}
				});

				// クリック処理
				$('#' + _self.id + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA + ' .tr').click(function(e){
					var row = app.getParentJQueryObjectByClassName($(e.target), 'tr');
					if(row != null && row.attr('id') != null){
						var rowid = row.attr('id').replace(_self.id + '_', '').replace(CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA_TR, '');
						if(_self.currentSelectId == null){
							// 選択
							_self.selectGrid(rowid);
							//if(action){
								_self.gridInfo['selectaction'](rowid);
							//}
							_self.currentSelectId = rowid;
						}
						else if(rowid == _self.currentSelectId){
							// 解除
							_self.deselectGrid(rowid);
							// if(action){
								_self.gridInfo['deselectaction'](rowid);
							//}
							_self.currentSelectId = null;
						}
						else{
							// 解除→選択
							_self.deselectGrid(_self.currentSelectId);
							//if(action){
								_self.gridInfo['deselectaction'](_self.currentSelectId);
							//}
							_self.selectGrid(rowid);
							// if(action){
								_self.gridInfo['selectaction'](rowid);
							// }
							_self.currentSelectId = rowid;
						}
					}
				});
	
				$('#' + _self.id + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX + ' .tr').click(function(e){
					var row = app.getParentJQueryObjectByClassName($(e.target), 'tr');
					if(row != null){
						var rowid = row.attr('id').replace(_self.id + '_', '').replace(CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX_TR, '');
						if(_self.currentSelectId == null){
							// 選択
							_self.selectGrid(rowid);
							//if(action){
								_self.gridInfo['selectaction'](rowid);
							//}
							_self.currentSelectId = rowid;
						}
						else if(rowid == _self.currentSelectId){
							// 解除
							_self.deselectGrid(rowid);
							//if(action){
								_self.gridInfo['deselectaction'](rowid);
							//}
							_self.currentSelectId = null;
						}
						else{
							// 解除→選択
							_self.deselectGrid(_self.currentSelectId);
							//if(action){
								_self.gridInfo['deselectaction'](_self.currentSelectId);
							//}
							_self.selectGrid(rowid);
							// if(action){
								_self.gridInfo['selectaction'](rowid);
							// }
							_self.currentSelectId = rowid;
						}
					}
				});

			}
			// Y方向のリサイズイベントバインド
			_self.bindResizeYGrid();

			// スクロールバーの設定
			_self.setScrollBar();

			// フッターがRainy3PagingFooterの場合、ページング表示を行う
			if(_self.footer != null && _self.footer.type=='paging_footer'){
				_self.footer.pageNum = pageNum;
				_self.footer.pageSize = pageSize;
				_self.footer.totalSize = totalSize;
				_self.footer.direction = _self.direction;

				_self.footer.clickAction=function(pageNum){
					_self.page=pageNum;
					_self.load();
				}

				_self.footer.showPageInfo();
			}

			// ロード後のアクション
			if(_self.gridInfo['loadaction'] != null){
				new _self.gridInfo['loadaction']();
			}

			// ロード後のアクション（引数指定・内部メソッド）
			if(loadaction != null){
				new loadaction();
			}
		}, 
		function(){},
		function(){
			_self.protection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, true)
		},
		function(head, body){
			_self.releaseProtection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, _self.jqMessageBox, _self.jqMessageBoxShadow);
		});
	}
	
	////////////////////////////////////////////////
	// フィールドのIDを取得
	this.getFieldId=function(rowid, fieldid){
		var ret = this.id + '_' + rowid + '_' + fieldid + '_' + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX_TR
		return ret;
	}


	////////////////////////////////////////////////
	// テーブルデータのリロード
	_self.reload=function(){
		var bkSelectId = _self.currentSelectId;
		_self.load(function(){
			// 選択を復元
			_self.selectRow(bkSelectId);
		});
	}


	////////////////////////////////////////////////
	// テーブルデータのリロード
	// 選択時のイベントを発生させない
	_self.reloadWithoutSelectAction=function(){
		var bkSelectId = _self.currentSelectId;

		_self._load(function(){
			// 選択を復元
			_self._selectRow(bkSelectId, false);
		}, false);
	}


	////////////////////////////////////////////////
	// Y方向リサイズイベントバインド
	_self.bindResizeYGrid=function(){

		// 左右のリサイズ
		$('.handle_bottom').mousedown(
			function(e){

				var delimId = e.target.id;
				var initPos = app.getMousePosition(e);

				var winWidth = _self.lastTotalWidth;
				var winLeft = rainy3.getWindowLeft(_self.parentId);

				// マウスカーソルの変更
				rainy3.styleProtection('cursor', 'row-resize');
				if(rainy3.resizeFunction != null){
					new rainy3.resizeFunction();
				}

				// マウス移動で枠線移動
				rainy3.bindProtection('mousemove', function(e){
					var movePos = app.getMousePosition(e);
					rainy3.jqYContentsResizeBorder.css('top', app.num2px(movePos.y - 2));
					return false;
				});

				// マウスアップイベントで、x-contentsリサイズ＆画面保護解除
				rainy3.bindDocumentMouseup('j2daddaa', function(e){

					_self.execResizeYGrid(delimId, initPos, app.getMousePosition(e));
					rainy3.releaseProtection();
					// 枠線の非表示
					rainy3.jqYContentsResizeBorder.css('display', 'none');
					// バインドの解除
					rainy3.unbindDocumentMouseup('j2daddaa');
				});

				// 画面保護
				rainy3.protection("clear", false, function(){

					// 枠線の表示
					// topをヘッダー分にする
					// heightをコンテンツ＋区切りの高さにする
					// x位置をマウスの押されてた位置-2pxにする
					rainy3.jqYContentsResizeBorder.css('display', 'block');
					rainy3.jqYContentsResizeBorder.css('left', winLeft);
					rainy3.jqYContentsResizeBorder.width(winWidth);
					rainy3.jqYContentsResizeBorder.css('top', app.num2px(initPos.y - 2));
				});
				return false;
			}
		);
	}



	////////////////////////////////////////////////
	// Y方向リサイズ実行
	_self.execResizeYGrid=function(delimId, initPos, endPos){

		var cellId = delimId.replace(_self.id + '_', '');
		cellId = cellId.replace(CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX_TK, '');
		var moveLen = endPos.y-initPos.y;

		var targetCount = 1;		// 最上列分
		// 加算する位置を取得
		var tr = $('#' + _self.id + '_' + cellId +  CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA_TR);
		tdList = tr.children('.td,.tk');

		// 移動距離算出
		for(var i=0; i<tdList.length; i++){
			if(i==0){
				var h = $(tdList[i]).height();
				if(h + moveLen < MIN_HEIGHT){
					moveLen = MIN_HEIGHT - h;
					break;
				}
			}
		}

		var cellHeight = null;
		for(var i=0; i<tdList.length; i++){
			if(cellHeight == null){
				cellHeight = $(tdList[i]).height() + moveLen;
			}
			$(tdList[i]).height(cellHeight);
		}


		var tr = $('#' + _self.id + '_' + cellId +  CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX_TR);
		tdList = tr.children('.td,.tk,.tk');

		for(var i=0; i<tdList.length; i++){
			$(tdList[i]).height(cellHeight);
			
			handleList = $(tdList[i]).children('.handle_bottom');
			if(handleList != null){
				for(var j=0; j<handleList.length; j++){
					$(handleList[j]).css('top', app.num2px(cellHeight - CELL_HANDLE_BOTTOM_HEIGHT + CELL_PADDING));
				}
			}
		}
	}


	////////////////////////////////////////////////
	// マウスオーバー処理
	_self.mouseoverGrid=function(rowid){

		if(rowid != _self.bkRowid){

			if(_self.bkRowid != null){
				var _bkRowid = _self.bkRowid;
				_self.bkRowid = null;

				var rowObj = document.getElementById(_self.id + "_" + _bkRowid + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA_TR);
				rowObj.className = rowObj.className.replace(ROW_HOVER_CLASS_NAME, '');
				delete rowObj;


				rowObj = document.getElementById(_self.id + "_" + _bkRowid + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX_TR);
				rowObj.className = rowObj.className.replace(ROW_HOVER_CLASS_NAME, '');
				delete rowObj;

			}

			var rowObj = document.getElementById(_self.id + "_" + rowid + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA_TR);
			rowObj.className += ROW_HOVER_CLASS_NAME;
			delete rowObj;

			if(_self.gridInfo['fittorow'] ==null || _self.gridInfo['fittorow'] ==false){
				rowObj = document.getElementById(_self.id + "_" + rowid + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX_TR);
				rowObj.className += ROW_HOVER_CLASS_NAME;
				delete rowObj;
			}
			
			_self.bkRowid = rowid;
		}
	}

	////////////////////////////////////////////////
	// マウスアウト処理
	_self.mouseoutGrid=function(rowid){

		if(rowid == _self.bkRowid){
			if(_self.bkRowid != null){
			
				_self.bkRowid = null;
				var rowObj = document.getElementById(_self.id + "_" + rowid + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA_TR);
				rowObj.className = rowObj.className.replace(ROW_HOVER_CLASS_NAME, '');
				delete rowObj;

				if(_self.gridInfo['fittorow'] ==null || _self.gridInfo['fittorow'] ==false){
					rowObj = document.getElementById(_self.id + "_" + rowid + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX_TR);
					rowObj.className = rowObj.className.replace(ROW_HOVER_CLASS_NAME, '');
					delete rowObj;
				}
			}
		}
	}

	////////////////////////////////////////////////
	// 選択行取得（外部公開用）
	_self.getSelectedRowId=function(){
		return _self.currentSelectId;
	}
	
	////////////////////////////////////////////////
	// フィールドの値を取得
	this.getFieldValue=function(rowid, fieldid){
		
		var ret = null;

		var fieldid = this.getFieldId(rowid,  fieldid);
 
		var obj = document.getElementById(fieldid)
		if(obj != null){
			ret = obj.innerHTML;
		}
		return ret;
	}

	////////////////////////////////////////////////
	// 選択処理（外部公開用）
	_self.selectRow=function(rowid){

		_self._selectRow(rowid, true)
/*
		if(_self.currentSelectId == null){
			// 選択
			var ret = _self.selectGrid(rowid);
			if(ret){
				_self.gridInfo['selectaction'](rowid);
				_self.currentSelectId = rowid;
			}
		}
		else if(rowid == _self.currentSelectId){
			// なにもしない
		}
		else{
			// 解除→選択
			_self.deselectGrid(_self.currentSelectId);
			_self.gridInfo['deselectaction'](_self.currentSelectId);

			var ret = _self.selectGrid(rowid);
			
			if(ret){
				_self.gridInfo['selectaction'](rowid);
				_self.currentSelectId = rowid;
			}
			else{
				_self.currentSelectId = null;
			}			
			// _self.selectGrid(rowid);
			// _self.gridInfo['selectaction'](rowid);
			// _self.currentSelectId = rowid;
		}
*/
	}

	////////////////////////////////////////////////
	// 選択処理
	_self._selectRow=function(rowid, action){

		if(_self.currentSelectId == null){
			// 選択
			var ret = _self.selectGrid(rowid);
			if(ret){
				if(action){
					_self.gridInfo['selectaction'](rowid);
				}
				_self.currentSelectId = rowid;
			}
		}
		else if(rowid == _self.currentSelectId){
			// なにもしない
		}
		else{
			// 解除→選択
			_self.deselectGrid(_self.currentSelectId);
			if(action){
				_self.gridInfo['deselectaction'](_self.currentSelectId);
			}

			var ret = _self.selectGrid(rowid);
			
			if(ret){
				if(action){
					_self.gridInfo['selectaction'](rowid);
				}
				_self.currentSelectId = rowid;
			}
			else{
				_self.currentSelectId = null;
			}			
			// _self.selectGrid(rowid);
			// _self.gridInfo['selectaction'](rowid);
			// _self.currentSelectId = rowid;
		}
	}


	////////////////////////////////////////////////
	// 非選択処理（外部公開用）
	_self.deselectRow=function(rowid, action){
		if(_self.currentSelectId == null){
			// なにもしない
		}
		else if(rowid == _self.currentSelectId){
			// 解除
			_self.deselectGrid(rowid);
			if(action){
				_self.gridInfo['deselectaction'](rowid);
			}
			_self.currentSelectId = null;
		}
		else{
			// なにもしない
		}
	}


	////////////////////////////////////////////////
	// 選択処理（描画・内部メソッド）
	_self.selectGrid=function(rowid){

		var ret = false;
		var rowObj = document.getElementById(_self.id + "_" + rowid + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA_TR);
		if(rowObj != null){
			rowObj.className += ROW_SELECTED_CLASS_NAME;
			delete rowObj;
			ret = true;
		}

		rowObj = document.getElementById(_self.id + "_" + rowid + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX_TR);
		if(rowObj != null){
			rowObj.className += ROW_SELECTED_CLASS_NAME;
			delete rowObj;
		}
		return ret;
	}

	////////////////////////////////////////////////
	// 非選択処理（描画・内部メソッド）
	_self.deselectGrid=function(rowid){
		var rowObj = document.getElementById(_self.id + "_" + rowid + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDDATA_TR);
		if(rowObj != null){
			rowObj.className = rowObj.className.replace(ROW_SELECTED_CLASS_NAME, '');
			delete rowObj;
		}

		rowObj = document.getElementById(_self.id + "_" + rowid + CREATE_OBJECT_ID_SUFFIX_CONTENT_GRIDINDEX_TR);
		if(rowObj != null){
			rowObj.className = rowObj.className.replace(ROW_SELECTED_CLASS_NAME, '');
			delete rowObj;
		}
	}

	////////////////////////////////////////////////
	// オブジェクトボディのリサイズ
	_self.resize=function(width, height){
		_self.gridHeader.width(_self.jqTarget.get(0).clientWidth);

		if(_self.gridInfo['fittorow'] ==null || _self.gridInfo['fittorow'] ==false){
			_self.gridIndex.height(_self.jqTarget.get(0).clientHeight);
		}
	}

	////////////////////////////////////////////////
	// 検索条件の取得
	_self.setCondition=function(condition){
		
		_self.condition = condition;

		results = _self.condition;
/*
			for(var i in results){ 
				var ret = i + "|";
				if($.type(results[i]) == 'array'){
					for(var j=0; j<results[i].length; j++){
						ret += results[i][j] + ','
					}
				}
				else{
					ret += results[i];
				}
				app.debug(ret);
			}
*/
	}

	////////////////////////////////////////////////
	// 検索条件の取得
	_self.getCondition=function(condition){
		return _self.condition;
	}

	
	////////////////////////////////////////////////
	// グリッドのリセット
	_self.reset=function(){
		// 2行目以降のGridDataを削除する
		var trList = _self.table.children('.tr');
		for(var i=1; i<trList.length; i++){
			$(trList[i]).remove();
		}
		// Grid Indexを削除する
		_self.gridIndex.empty();
	}

}
Rainy3Grid.prototype = new Rainy3Base();
rainy3.rainy3ObjectList.push({key:'r3grid', _class:Rainy3Grid});

/****************************************************************
 * フォームウィンドウオブジェクト
 ****************************************************************/
var Rainy3Form = function(id){

	var FORM_ID_SUFFIX = "_dfsjasds";
	this.loadurl = null;
	this.loadaction = null;
	this.unloadaction = null;
	this.jqTable = null;
	this.condition = {};
	this.defaultData = {};
	this.form=null;

	////////////////////////////////////////////////
	// 初期化
	this.init(this, 'form');

	////////////////////////////////////////////////
	// formの描画
	this.addContent=function(id, jqTarget, contentInfo){

		var _self=this;
		var form = null;
		eval(contentInfo);

		if(form == null){
			alert('Please define "var form={}"');
		}
		else{
			this.formData = form;
			this.loadurl = form['url'];
			this.loadaction = form['loadaction'];
			this.unloadaction = form['unloadaction'];

			// 初期検索条件
			if(this.formData['initcondition'] != null){
				_self.condition=this.formData['initcondition'];
			}
			var totalWidth = parseInt(form['label_width']) + parseInt(form['data_width']) + parseInt(form['example_width']);

			this.jqForm = $('<form name="' + this.id + FORM_ID_SUFFIX + '" id="' + this.id + FORM_ID_SUFFIX + '"></form>').appendTo(jqTarget);
			this.jqTable = $('<table cellspacing="0" cellpadding="0" border="0" width="' + totalWidth + '" class="form_table"></table>').appendTo(this.jqForm);
			var html = '<colgroup width="' + form['label_width'] + '">';
			html += '<colgroup width="' + form['data_width'] + '">';


			if(form['example_width'] != '0') {
				html += '<colgroup width="' + form['example_width'] + '">';
			}

			$(html).appendTo(this.jqTable);

			for(var i=0; i<form['forms'].length; i++){
				if(form['forms'][i]['plural'] != null && form['forms'][i]['plural']){
					if(form['forms'][i]['groupname'] == null || form['forms'][i]['groupname'] == ''){
						alert("Please specify groupname.");
						return;
					}
				}

				html = '<tr id=' + this.getObjectId(form['forms'][i]['id']) + '>';
				html += '</tr>';
				var trObj = $(html).appendTo(this.jqTable);

				html = '<th>';
				html += form['forms'][i]['label'];
				html += '</th>';
				var thObj = $(html).appendTo(trObj);


				html = '<td>';
				html += '</td>';
				var tdObj = $(html).appendTo(trObj);


				html = '';
				if(form['forms'][i]['plural'] != null && form['forms'][i]['plural']){
					html += '<div class="pluralbox">';
					html += '<div class="boxdelete" onclick="var obj=$(this).parent();obj.fadeOut(\'normal\', function(){obj.remove()});">×</div><br style="clear:both;">'
				}

				if(form['forms'][i]['plural'] != null && form['forms'][i]['plural']){
					html += '<span title="' + form['forms'][i]['id'] + '[0]" class="r3form_message"></span>';
				}
				else{
					html += '<span title="' + form['forms'][i]['id'] + '" class="r3form_message"></span>';
				}

				html += form['forms'][i]['html'];
				if(form['forms'][i]['plural'] != null && form['forms'][i]['plural']){
					html += '</div>';
				}
				var inputObj = $(html).appendTo(tdObj);


				var aObj = null;
				if(form['forms'][i]['plural'] != null && form['forms'][i]['plural']){
					html = '<p class="pluralhandle"><a href="javascript:void(0);" class="add_field" onclick="rainy3.getWindow(\'' + this.parentId +  '\').addInputFields(\'' + form['forms'][i]['id'] + '\');">' + RAINY3MESSAGE.FORM_WINDOW_ADD_FIELD_MESSAGE + '</a></p>';
					aObj = $(html).appendTo(tdObj);
				}

				if(form['example_width'] != '0') {
					html = '<td class="example">';
					html += form['forms'][i]['example'];
					html += '</td>';
					var exObj = $(html).appendTo(trObj);
				}
				
				
				


				// 下位の入力フィールドを参照して、nameの前に"id[0]."を付加する
				if(form['forms'][i]['plural'] != null && form['forms'][i]['plural']){
					form['forms'][i]['pluralbox'] = inputObj;
					form['forms'][i]['pluralhandle'] = aObj;
					this.formData['forms'][i]['pluralcount']=1;
					this.addListParamPrefix(form['forms'][i]['groupname'], 0, inputObj);
				}

			}
			this.bindFocusAction();
			this.defaultData = app._formParam2jsonData(this.jqForm, true);
		}
	}

	////////////////////////////////////////////////
	// フィールドの追加
	this.addInputFields=function(id){
		var _self = this;
		for(var i=0; i<this.formData['forms'].length; i++){
			if(this.formData['forms'][i]['id'] == id){

				var html = '<div class="pluralbox pluralboxnext">';
				html += '<div class="boxdelete" onclick="var obj=$(this).parent();obj.fadeOut(\'normal\', function(){obj.remove()});">×</div><br style="clear:both;">'
				html += '<span title="' + this.formData['forms'][i]['id'] + '[' + _self.formData['forms'][i]['pluralcount'] + ']" class="r3form_message"></span>';
				html += this.formData['forms'][i]['html'];
				html += '</div>';

				var jqObj = $(html).insertBefore(this.formData['forms'][i]['pluralhandle']);
	
				jqObj.fadeIn('normal', function(){
					_self.bindFocusAction2(jqObj);
				});

				_self.addListParamPrefix(_self.formData['forms'][i]['groupname'], _self.formData['forms'][i]['pluralcount'], jqObj);
				_self.formData['forms'][i]['pluralcount']++;

				break;
			}
		}
	}

	////////////////////////////////////////////////
	// フィールドの追加
	this.addFirstInputFields=function(id){
		var _self = this;
		var ret = null;
		for(var i=0; i<this.formData['forms'].length; i++){
			if(this.formData['forms'][i]['id'] == id){

				var html = '<div class="pluralbox">';
				html += '<div class="boxdelete" onclick="var obj=$(this).parent();obj.fadeOut(\'normal\', function(){obj.remove()});">×</div><br style="clear:both;">'
				html += '<span title="' + this.formData['forms'][i]['id'] + '[0]" class="r3form_message"></span>';
				html += this.formData['forms'][i]['html'];
				html += '</div>';

				var jqObj = $(html).insertBefore(this.formData['forms'][i]['pluralhandle']);
				ret = jqObj;
				
				jqObj.fadeIn('normal', function(){
					_self.bindFocusAction2(jqObj);
				});
				_self.addListParamPrefix(_self.formData['forms'][i]['groupname'], 0, jqObj);

				break;
			}
		}
		return ret
	}


	////////////////////////////////////////////////
	// 配列パラメータにプレフィックス（groupname[n].）を追加する
	// railsの場合は.は不要
	this.addListParamPrefix=function(prefix, count, jqObj){

		if(jqObj[0].nodeName.toUpperCase() == 'INPUT'){
			if(jqObj.attr('type').toUpperCase() == 'TEXT' || 
				jqObj.attr('type').toUpperCase() == 'HIDDEN' || 
				jqObj.attr('type').toUpperCase() == 'PASSWORD' ||
				jqObj.attr('type').toUpperCase() == 'CHECKBOX' || 
				jqObj.attr('type').toUpperCase() == 'RADIO'){
				name = jqObj.attr('name');
				
				if(rainy3.RAILS){
					jqObj.attr({name:prefix + '[' + count + ']' + name})
				}
				else{
					jqObj.attr({name:prefix + '[' + count + '].' + name})
				}
			}
		}
		else if(jqObj[0].nodeName.toUpperCase() == 'TEXTAREA' || jqObj[0].nodeName.toUpperCase() == 'SELECT'){
			name = jqObj.attr('name');
			if(rainy3.RAILS){
				jqObj.attr({name:prefix + '[' + count + ']' + name})
			}
			else{
				jqObj.attr({name:prefix + '[' + count + '].' + name})
			}
		}
		else{
			var children = jqObj.children();
			for(var i=0; i<children.length; i++){
				this.addListParamPrefix(prefix, count, $(children[i]));
			}
		}
	}

	////////////////////////////////////////////////
	// フォームデータの読み込み
	this.load=function(){
		var _self = this;

		var dataMap = {};
		if(this.condition != null){
			for(var i in this.condition){
				dataMap[i] = this.condition[i];
			}
		}

		// HTTPリクエスト
		rainy3.request(this.loadurl, dataMap, false, function(head, body){
			// 成功時のアクション

			// エラーメッセージをクリア
			$('.r3form_message p').remove();

			// 追加した要素を削除
			_self.resetPluralFields();

			// xmlで帰ってきた値をjsonデータに変換してフォームにセット
			var pluralList = [];
			var jsonData = _self.xml2formData(body, pluralList);

			// 複数要素がある場合には、数文要素を増やす
			_self.createPluralFields(pluralList);

			// 返ってきた値をセット
			app._jsonData2formParam(jsonData, _self.jqForm);

			// ロードアクションを実行
			if(_self.loadaction != null){
				new _self.loadaction();
			}
		},
		function(){},
		function(){
			_self.protection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, true)
		},
		function(head, body){
			_self.releaseProtection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, _self.jqMessageBox, _self.jqMessageBoxShadow);
		});
	}

	////////////////////////////////////////////////
	// 複数入力フィールドを指定数分増やす
	this.createPluralFields=function(pluralList){
		var _self = this;

		for(var i in pluralList){ 

			for(var j=0; j<_self.formData['forms'].length; j++){
				if(i==_self.formData['forms'][j]['groupname']){
					
					for(var k=1; k<pluralList[i]; k++){
						_self.addInputFields(_self.formData['forms'][j]['id'])
					}
				}
			}
		}
	}

	////////////////////////////////////////////////
	// 複数入力フィールドをリセット
	this.resetPluralFields=function(){
		this.jqForm.find('.pluralboxnext').remove();
		for(var i=0; i<this.formData['forms'].length; i++){
			if(this.formData['forms'][i]['pluralcount'] != null){
				this.formData['forms'][i]['pluralcount'] = 1;
			}
		}

		// 先頭のフィールドがない場合は追加
		for(var j=0; j<this.formData['forms'].length; j++){
			if(this.formData['forms'][j]['groupname'] != null){
				// 対象のフィールドが0件の場合は、デフォルトを追加する
				if(this.formData['forms'][j]['pluralhandle'].parent().children('.pluralbox').length == 0){
					var ret = this.addFirstInputFields(this.formData['forms'][j]['id'])
					this.formData['forms'][j]['pluralbox'] = ret;
				}
			}
		}
	}


	////////////////////////////////////////////////
	// 入力を初期状態に戻す
	this.reset=function(){

		// エラーメッセージをクリア
		$('.r3form_message p').remove();

		// 追加した要素を削除
		this.resetPluralFields();


		// デフォルト値をセット
		app._jsonData2formParam(this.defaultData, this.jqForm);

		if(this.unloadaction != null){
			new this.unloadaction();
		}
	}

	////////////////////////////////////////////////
	// データ取得条件の設定
	this.setCondition=function(condition){
		this.condition = condition;
	}

	////////////////////////////////////////////////
	// 検索条件の取得
	this.getCondition=function(condition){
		return this.condition;
	}

	////////////////////////////////////////////////
	// リクエストURLの設定
	this.setUrl=function(url){
		this.loadurl = url;
	}

	////////////////////////////////////////////////
	// データの送信
	this.send=function(url, afterFunction){
		var _self = this;

		// フォームデータを取得
		var dataMap = app._formParam2jsonData(this.jqForm, false);
		// HTTPリクエスト
		rainy3.request(url, dataMap, false, function(head, body){
			// 成功

			// メッセージをクリア
			$('.r3form_message p').remove();

			// 追加した要素を削除
			_self.resetPluralFields();

			// xmlで帰ってきた値をjsonデータに変換してフォームにセット
			var pluralList = [];
			var jsonData = _self.xml2formData(body, pluralList);

			// 複数要素がある場合には、数文要素を増やす
			_self.createPluralFields(pluralList);

			// 返ってきた値をセット（エラーの場合はセットしない）
			app._jsonData2formParam(jsonData, _self.jqForm);

			// 正常終了のメッセージを表示（1秒くらい）
			
			
			// 処理後のメソッド実行
			if(afterFunction != null){
				new afterFunction()
			}
		},
		function(head, body){
			// ユーザーエラー

			// メッセージをクリア
			$('.r3form_message p').remove();

			// エラーメッセージを表示
			var jsonData = _self.xml2messageData(body);

			// 返ってきた値をセット
			_self.jsonData2message($('#' + _self.id + FORM_ID_SUFFIX), jsonData);

			// フォーカス
			_self.messageFocus(body);

			// xmlで帰ってきた値をjsonデータに変換してフォームにセット
//			var jsonData = _self.xml2formData(body);

			// 返ってきた値をセット（エラーの場合はセットしない）
//			app._jsonData2formParam(jsonData, _self.jqForm);

		},
		function(){
			_self.protection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, true)
		},
		function(head, body){

			var result = head.find("result").text();
			var message = head.find("message").text();
			if(message == null || message == ''){
				message = RAINY3MESSAGE.MESSAGE_COMPLATE;
			}
			if(result == '1'){
				_self.showCompleateMessage( _self.jqContentLoading, _self.jqMessageBox, _self.jqMessageBoxShadow, message);


				setTimeout(function(){
					_self.releaseProtection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, _self.jqMessageBox, _self.jqMessageBoxShadow);
				}, 1000);

			}
			else{
				_self.releaseProtection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, _self.jqMessageBox, _self.jqMessageBoxShadow);
			}
		});
	}


	////////////////////////////////////////////////
	// 返却されたXMLからjsonデータ（入力）を生成する
	this.xml2formData=function(body, pluralList){
		var _self = this;
		var jsonData = {};
		var fields = body.find('fields');
		fields.children('field').each(
			function(i){
				var field = $(this);
				var id = field.attr('id');
				field.find('value').each(function(j){
					var value = $(this);
					_self.setJsonData(jsonData, id, value.text());
				});
			}
		);

		// 複数要素の処理を行う
		fields.children('plural').each(
			function(i){
				var plural = $(this);
				var groupname = plural.attr('id');

				plural.children('datablock').each(
					function(j){
						var datablock = $(this);

						if(pluralList[groupname] == null){
							pluralList[groupname] = 1;
						}
						else{
							pluralList[groupname]++;
						}

						datablock.children('field').each(
							function(k){
								var field = $(this);
								var id = field.attr('id');

								if(rainy3.RAILS){
									id = groupname + "[" + j + "]" + id;
								}
								else{
									id = groupname + "[" + j + "]." + id;
								}
								

								field.find('value').each(function(l){
									var value = $(this);
									_self.setJsonData(jsonData, id, value.text());
								});
							}
						)
					}
				);
			}
		);

		return jsonData;
	}
	this.setJsonData=function(jsonData, id, value){
		if(jsonData[id] == null){
			jsonData[id]=value;
		}
		else{
			if($.type(jsonData[id]) == 'array'){
				jsonData[id].push(value);
			}
			else{
				var bk = jsonData[id];
				jsonData[id] = [];
				jsonData[id].push(bk);
				jsonData[id].push(value);
			}
		}
	}

	////////////////////////////////////////////////
	// 返却されたXMLからjsonデータ（メッセージ）を生成する
	this.xml2messageData=function(body){
		var jsonData = {};
		body.find('message').each(
			function(i){
				var message = $(this);
				var id = message.attr('id');
				var message = message.text();
				jsonData[id]=message;
			}
		);
		return jsonData;
	}

	////////////////////////////////////////////////
	// メッセージをセットする
	this.jsonData2message=function(jqObj, jsonData){
		if(jqObj[0].nodeName.toUpperCase() == 'SPAN' && jqObj.hasClass('r3form_message')){
			var message = jsonData[jqObj.attr('title')];
			if(message != null && message != ''){
				jqObj.html('<p>' + message + '</p>');
			}
		}
		else{
			var children = jqObj.children();
			for(var i=0; i<children.length; i++){
				this.jsonData2message($(children[i]), jsonData);
			}
		}
	}

	////////////////////////////////////////////////
	// フォーカスする
	this.messageFocus=function(body){
		var _self = this;
		var focus = body.find('focus');
		if(focus.length == 1){
			_self._messageFocus($('#' + _self.id + FORM_ID_SUFFIX), focus.text());
		}
	}
	this._messageFocus=function(jqObj, focusName){
		if(jqObj[0].nodeName.toUpperCase() == 'INPUT' || jqObj[0].nodeName.toUpperCase() == 'SELECT' || jqObj[0].nodeName.toUpperCase() == 'TEXTAREA'){
			if(jqObj.attr('name') == focusName){
				jqObj.focus();
			}
		}
		else{
			var children = jqObj.children();
			for(var i=0; i<children.length; i++){
				this._messageFocus($(children[i]), focusName);
			}
		}
	}

	////////////////////////////////////////////////
	// 入力フィールドにフォーカスした場合に、背景を変更する
	this.bindFocusAction=function(){

		var target = '#' + this.id + FORM_ID_SUFFIX + ' input';
		target += ',';
		target += '#' + this.id + FORM_ID_SUFFIX + ' select'
		target += ',';
		target += '#' + this.id + FORM_ID_SUFFIX + ' textarea'

		$(target).focus(function(e){
			var parentObj = app.getParentJQueryObjectByTagName($(e.target), 'tr');
			if(parentObj != null){
				parentObj.children('td,th').addClass("selected");
			}
		});

		$(target).blur(function(e){
			var parentObj = app.getParentJQueryObjectByTagName($(e.target), 'tr');
			if(parentObj != null){
				parentObj.children('td,th').removeClass("selected");
			}
		});
	}

	////////////////////////////////////////////////
	// 入力フィールドにフォーカスした場合に、背景を変更する（Jqueryオブジェクト指定）
	this.bindFocusAction2=function(jqObj){

		var target = 'input';
		target += ',';
		target += 'select'
		target += ',';
		target += 'textarea'

		jqObj.find(target).focus(function(e){
			var parentObj = app.getParentJQueryObjectByTagName($(e.target), 'tr');
			if(parentObj != null){
				parentObj.children('td,th').addClass("selected");
			}
		});

		jqObj.find(target).blur(function(e){
			var parentObj = app.getParentJQueryObjectByTagName($(e.target), 'tr');
			if(parentObj != null){
				parentObj.children('td,th').removeClass("selected");
			}
		});
	}

	////////////////////////////////////////////////
	// ロード後の処理を定義する
	this.setLoadAction=function(){
	}

	////////////////////////////////////////////////
	// ロード直前の処理を定義する
	this.setPreLoadAction=function(){
	}


}
Rainy3Form.prototype = new Rainy3Base();
rainy3.rainy3ObjectList.push({key:'r3form', _class:Rainy3Form});

/****************************************************************
 * 横棒グラフオブジェクト
 ****************************************************************/
var Rainy3WidthBarGraph = function(id){

	var FORM_ID_SUFFIX = "_yfpj1zds";
	this.wBarGraphData=null;
	this.loadurl=null;
	this.loadaction=null;
	this.unloadaction=null;
	this.target=null;

	////////////////////////////////////////////////
	// 初期化
	this.init(this, 'width_bar_graph');

	////////////////////////////////////////////////
	// イメージの描画
	this.addContent=function(id, jqTarget, contentInfo){
		var wBarGraph = null;
		eval(contentInfo);

		if(wBarGraph == null){
			alert('Please define "var wBarGraph={}"');
		}
		else{
			this.wBarGraphData = wBarGraph;
			this.loadurl = wBarGraph['url'];
			this.loadaction = wBarGraph['loadaction'];
			this.unloadaction = wBarGraph['unloadaction'];
			this.target=jqTarget;
		}
	}

	////////////////////////////////////////////////
	// 画像の読み込み
	this.load=function(){
		var _self = this;
		var _url = this.loadurl;
		var first=true;
		var dataMap = {};
		if(this.condition != null){
			for(var i in this.condition){
				dataMap[i] = this.condition[i];
			}
		}

		// HTTPリクエスト
		rainy3.request(this.loadurl, dataMap, false, function(head, body){
			// 成功時のアクション
			var jsonData = {};
			jsonData['max']=parseInt(body.find('max').text());
			jsonData['first_marker_label']=body.find('first_marker_label').text();
			jsonData['y_label']=_self.wBarGraphData['ylabel'];
			jsonData['x_label']=_self.wBarGraphData['xlabel'];
			jsonData['dial_px']=_self.wBarGraphData['dialpx'];
			jsonData['markers']=[];
			var markers = body.find('markers');
			markers.children('marker').each(
				function(i){
					var value = parseInt($(this).find('value').text());
					var label = $(this).find('label').text();
					jsonData['markers'].push({value:value, label:label});
				}
			);
			jsonData['bars']=[];
			var bars = body.find('bars');
			bars.children('bar').each(
				function(i){
					var value = parseInt($(this).find('value').text());
					var label = $(this).find('label').text();
					jsonData['bars'].push({value:value, label:label});
				}
			);
			_self.target.empty();

			var totalWidth = (jsonData['max'] * jsonData['dial_px']) + _self.wBarGraphData['bar_label_width'] + _self.wBarGraphData['ylabel_width'];
			
			
			var jqDiv = $('<div class="graph_frame" style="width:' + (totalWidth)+ 'px"></div>').appendTo(_self.target);
			var jqTable = $('<table cellspacing="0" cellpadding="0" border="0" width="' + totalWidth + '" class="graph_table"></table>').appendTo(jqDiv);

			// 上部スペースの描画
			_self.drawSpace(true, jsonData, jqTable);
			for(var i=0; i<jsonData['bars'].length; i++){
				// 棒グラフの描画
				_self.drawBar(i, jsonData, jqTable);
				// 下部スペースの描画
				_self.drawSpace(false, jsonData, jqTable);
			}
			// フッターの描画
			_self.drawFoot(jsonData, jqDiv, totalWidth);

			// ロードアクションを実行
			if(_self.loadaction != null){
				new _self.loadaction();
			}
		},
		function(){},
		function(){
			_self.protection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, true)
		},
		function(head, body){
			_self.releaseProtection2(_self.jqContentProtection, _self.jqContentTabStop1, _self.jqContentTabStop2, _self.jqContentLoading, _self.jqContentTabStop1, _self.jqMessageBox, _self.jqMessageBoxShadow);
		});
	}

	////////////////////////////////////////////////
	// テーブルスペースの描画
	this.drawSpace=function(first, jsonData, jqTable){
		var _self=this;
		var jqSpTr = $('<tr></tr>').appendTo(jqTable);

		if(first){
			var dataCount = jsonData['bars'].length;
			var height = (_self.wBarGraphData['barheight']*dataCount) + (_self.wBarGraphData['separate']*dataCount) + _self.wBarGraphData['separate'];
			var html = '<td style="width:' + _self.wBarGraphData['ylabel_width'] + 'px;height:' + (height) + 'px" class="y_label" rowspan="' + (dataCount * 2 + 1) + '">';
			
			html += '<img src="' + _self.wBarGraphData['ylabel_img'] + '">'
			html += '</td>';
			var jqTdLabel = $(html).appendTo(jqSpTr);
		}

		var html = '<td style="width:' + (_self.wBarGraphData['bar_label_width'] - 3) + 'px;height:' + (_self.wBarGraphData['separate']) + 'px" class="space_bar_label marker"></td>';
		var jqTdLabel = $(html).appendTo(jqSpTr);
		for(var j=0; j<jsonData['max']; j++){
			var marker=false;
			for(var k=0; k<jsonData['markers'].length; k++){
				if(jsonData['markers'][k]['value'] == j + 1){
					marker=true;
					break;
				}
			}
			
			var width=jsonData['dial_px'];
			if(marker){
				width-=1;
			}
			var html = '<td style="width:' + width + 'px;height:' + (_self.wBarGraphData['separate']) + 'px" class="space_bar';
			if(marker){
				html += ' marker'
			}
			html += '"></td>';
			var jqTd = $(html).appendTo(jqSpTr);
		}
	}
	
	////////////////////////////////////////////////
	// 棒グラフの描画
	this.drawBar=function(i, jsonData, jqTable){
		var _self=this;
		var jqTr = $('<tr></tr>').appendTo(jqTable);
		var html = '<td align="right" style="width:' + (_self.wBarGraphData['bar_label_width'] - 3) + 'px;height:' + (_self.wBarGraphData['barheight']) + 'px" class="bar_label marker">';
		html += jsonData['bars'][i]['label'] + "";
		html += '</td>';
		var jqTdLabel = $(html).appendTo(jqTr);

		for(var j=0; j<jsonData['max']; j++){
			var marker=false;
			for(var k=0; k<jsonData['markers'].length; k++){
				if(jsonData['markers'][k]['value'] == j + 1){
					marker=true;
					break;
				}
			}

			var width=jsonData['dial_px'];
			if(marker){
				width-=1;
			}
			var html = '<td style="width:' + width + 'px;height:' + (_self.wBarGraphData['barheight']) + 'px" class="bar';
			if(marker){
				html += ' marker'
			}
			html += '"></td>';
			var jqTd = $(html).appendTo(jqTr);

			if(j==0 && jsonData['bars'][i]['value'] != '' && jsonData['bars'][i]['value'] > 0){
				var frame=$('<div class="bar_pos"></div>').appendTo(jqTd);

				
				var barLen = (jsonData['bars'][i]['value'] / jsonData['max']) * (jsonData['dial_px'] * 100);
				$('<div style="background-color:' + _self.wBarGraphData['barcolor'] +  ';width:' + barLen + 'px;height:' + (_self.wBarGraphData['barheight']) + 'px;" class="bar"></div>').appendTo(frame);
			}
		}
	}

	////////////////////////////////////////////////
	// フッターの描画
	this.drawFoot=function(jsonData, jqDiv, totalWidth){
		var _self=this;
		var html = '<div class="foot_frame" style="width:' + (totalWidth + (_self.wBarGraphData['marker_label_width'] / 2)) + 'px;height:' + _self.wBarGraphData['marker_label_height'] + 'px;"></div>';
		var jqFoot = $(html).appendTo(jqDiv);
		var jqFootFrame = $('<div class="foot_pos"></div>').appendTo(jqFoot);
		var labelPos = _self.wBarGraphData['bar_label_width'] + _self.wBarGraphData['ylabel_width'];
		labelPos -= (_self.wBarGraphData['marker_label_width'] / 2)
		$('<div style="left:' + labelPos + 'px;width:' + _self.wBarGraphData['marker_label_width'] + 'px;" class="box">' + jsonData['first_marker_label'] + '</div>').appendTo(jqFootFrame);
		for(var i=0; i<jsonData['markers'].length; i++){
			var labelPos = _self.wBarGraphData['bar_label_width'] + (jsonData['markers'][i]["value"] * _self.wBarGraphData['dialpx']) + _self.wBarGraphData['ylabel_width'];
			labelPos -= (_self.wBarGraphData['marker_label_width'] / 2);
			$('<div style="left:' + labelPos + 'px;width:' + _self.wBarGraphData['marker_label_width'] + 'px;" class="box">' + jsonData['markers'][i]["label"] + '</div>').appendTo(jqFootFrame);
		}
		var html = '<div class="foot_frame" style="text-align:center;width:' + (totalWidth + (_self.wBarGraphData['marker_label_width'] / 2)) + 'px;height:' + _self.wBarGraphData['marker_label_height'] + 'px;">' + '<img src="' + _self.wBarGraphData['xlabel_img'] + '">' + '</div>';
		var jqFoot = $(html).appendTo(jqDiv);
		
	}
	
	////////////////////////////////////////////////
	// 画像を削除する
	this.reset=function(){
		var _self=this;
		if(_self.unloadaction != null){
			new _self.unloadaction();
		}
		this.target.empty();
	}

	////////////////////////////////////////////////
	// データ取得条件の設定
	this.setCondition=function(condition){
		this.condition = condition;
	}
	
		////////////////////////////////////////////////
	// 検索条件の取得
	this.getCondition=function(condition){
		return this.condition;
	}
}
Rainy3WidthBarGraph.prototype = new Rainy3Base();
rainy3.rainy3ObjectList.push({key:'r3wbgraph',		_class:Rainy3WidthBarGraph});


/****************************************************************
 * イメージオブジェクト
 ****************************************************************/
var Rainy3Image = function(id){

	var FORM_ID_SUFFIX = "_dfsjasds";
	this.loadurl = null;
	this.loadaction = null;
	this.unloadaction = null;
	this.jqTable = null;
	this.condition = {};
	this.defaultData = {};
	this.iamge=null;
	this.imageObj=null;

	////////////////////////////////////////////////
	// 初期化
	this.init(this, 'image');

	////////////////////////////////////////////////
	// グラフの描画
	this.addContent=function(id, jqTarget, contentInfo){
		var image = null;
		eval(contentInfo);

		if(image == null){
			alert('Please define "var image={}"');
		}
		else{
			this.imageData = image;
			this.loadurl = image['url'];
			this.loadaction = image['loadaction'];
			this.unloadaction = image['unloadaction'];
			this.target=jqTarget;
		}
	}

	////////////////////////////////////////////////
	// フォームデータの読み込み
	this.load=function(){
		var _self = this;
		var _url = this.loadurl;
		var first=true;
		var dataMap = {};
		if(this.condition != null){
			for(var i in this.condition){
				if(first && _url.indexOf('?') == -1){
					_url += '?';
				}
				else{
					_url += '&';
				}
				_url += i + "=" + this.condition[i];
				first=false;
			}
		}
		if(first && _url.indexOf('?') == -1){
			_url += '?';
		}
		else{
			_url += '&';
		}
		_url += "dt=" + (new Date()).getTime();
		this.target.empty();
		this.imageObj = $('<img src="' + _url + '">').appendTo(this.target);
		this.imageObj.load(function(){_self.target.get(0).scrollTop = _self.imageObj.height() });
	}

	////////////////////////////////////////////////
	// 画像を削除する
	this.reset=function(){
		this.target.empty();
	}
}
Rainy3Image.prototype = new Rainy3Base();
rainy3.rainy3ObjectList.push({key:'r3image', _class:Rainy3Image});

$(rainy3.init);
