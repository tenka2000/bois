﻿// キーコードの定義
var KEY = {
	'ENTER':13,
	'TAB':9,
	'LEFT_ARROW':37,
	'UP_ARROW':38,
	'RIGHT_ARROW':39,
	'DOWN_ARROW':40,
	'F5':116,
	'F2':113,
	'SPACE':32,
	'NUM0':48,
	'NUM9':57,
	'ALPHA_A':65,
	'ALPHA_Z':90,
	'ZENKAKU':229,
	'CTL':17,
	'ALT':18,
	'BUCKSPACE':8,
	'HOME':36,
	'END':35,
	'PAGEUP':33,
	'PAGEDOWN':34,
	'INSERT':45,
	'DELETE':46,
	'YEN':220,
	'TILDE':222,
	'HYPHEN':189,
	'RBRACKET':219,
	'LBRACKET':221,
	'ATMARK':192,
	'COLON':186,
	'SEMICOLON':187,
	'BKSLASH':226,
	'LT':188,
	'GT':90,
	'SHIFT':16
};

var app = new function(){
	var _self = this;

    // ウィンドウサイズの取得タイプ
	this.windowType = 3;
	this.DEBUG = true;
	this.debugArea = null;

	// ウィンドウロード時の処理
	$(document).ready(function(){
		if (window.innerHeight) {
			_self.windowType = 0;
		}
		else if (document.documentElement && document.documentElement.clientHeight) {
			_self.windowType = 1;
		}
		else if (document.body && document.body.clientHeight) {
			_self.windowType = 2;
 		}
	});

	////////////////////////////////////////////////////////////////
	// デバッグプリント
	this.debug = function(text){
		var _self = this;
		if (_self.DEBUG) {
			if (_self.debugArea == null) {
				$("body").append('<div style="width:auto;height:auto;opacity: 0.95;background-color:#ffffff;position:absolute;top:0px;z-index:9999;border:solid 1px black;" id="debugWindow" onDblClick="$(\'#debugWindow\').remove();spreadsheet.debugObj=null;"><span>デバッグ情報</span><br/></div>');
				_self.debugArea = $("#debugWindow");
			}
			_self.debugArea.append("<span>" + text + "</span><br/>");
		}
	}


	////////////////////////////////////////////////////////////////
	// ウィンドウの高さ取得
	// JQueryで取得すると、処理に時間がかかるので独自で実装する
	this.getWindowHeight = function(){
		var _self = this;
		if (_self.windowType == 0) {
			return parseInt(window.innerHeight);
		}
		else {
			if (_self.windowType == 1) {
				return parseInt(document.documentElement.clientHeight);
			}
			else {
				return parseInt(document.body.clientHeight);
			}
		}
	}

	////////////////////////////////////////////////////////////////
	// ウィンドウの幅取得
	// JQueryで取得すると、処理に時間がかかるので独自で実装する
	this.getWindowWidth = function(){
		var _self = this;
		if (_self.windowType == 0) {
			return parseInt(window.innerWidth);
		}
		else {
			if (_self.windowType == 1) {
				return parseInt(document.documentElement.clientWidth);
			}
			else {
				return parseInt(document.body.clientWidth);
			}
		}
	}

	////////////////////////////////////////////////////////////////
	// テキストまたはテキストエリアのカーソルを末尾に移動する
	this.endCursor = function(id, focusFlg){
		var obj = null;
		if((typeof id) == 'string'){
			obj = $('#'+ id);
		}else{
			obj = $(id);
		}
		
		if(obj.length > 0 && (obj.get(0).tagName == 'TEXTAREA' || (obj.get(0).tagName == 'INPUT' && obj.attr('type') == 'text'))){
			if(focusFlg){
				obj.focus();
			}
			var elm = obj.get(0);
			if (elm.createTextRange) {
				var range = elm.createTextRange();
				range.move('character', elm.value.length);
				range.select();
			} else if (elm.setSelectionRange) {
				elm.setSelectionRange(elm.value.length, elm.value.length);
			}
		}
		delete obj;
	}

	////////////////////////////////////////////////////////////////
	// テキストまたはテキストエリアのカーソルを先頭に移動する
	this.startCursor = function(id, focusFlg){
		var obj = null;
		if((typeof id) == 'string'){
			obj = $('#'+ id);
		}else{
			obj = $(id);
		}
		
		if(obj.length > 0 && (obj.get(0).tagName == 'TEXTAREA' || (obj.get(0).tagName == 'INPUT' && obj.attr('type') == 'text'))){
			if(focusFlg){
				obj.focus();
			}
			var elm = obj.get(0);
			if (elm.createTextRange) {
				var range = elm.createTextRange();
				range.move('character', 0);
				range.select();
			} else if (elm.setSelectionRange) {
				elm.setSelectionRange(0, 0);
			}
		}
		delete obj;
	}

	////////////////////////////////////////////////////////////////
	// テキストまたはテキストエリアのカーソルを指定位置に移動する
	this.moveCursor = function(id, pos){
		var elm = document.getElementById(id); // テキストエリアのelement取得
		elm.focus();

		if (elm.createTextRange) {
			var range = elm.createTextRange();
			range.move('character', pos);
//		range.moveStart('character', 10);	// 先頭から数えた開始地点
//		range.moveEnd('character', -20);	// 末尾から数えた執着地点
			range.select();
		} else if (elm.setSelectionRange) {
			elm.setSelectionRange(pos, pos);
		}
		delete(elm);
	}

	////////////////////////////////////////////////////////////////
	// マウス位置の取得
	this.getMousePosition = function(e) {
		var obj = new Object();
        if (e) {
			obj.x = e.pageX;
			obj.y = e.pageY;
		}
		else {
			obj.x = event.x + document.body.scrollLeft;
			obj.y = event.y + document.body.scrollTop;
		}

		return obj;
	}

	////////////////////////////////////////////////////////////////
	// HTMLエスケープ
	this.htmlEscape = function(str){
		
		if(str == null){
			str = "";
		}
		str = str.replace(/&/g, "&amp;");	//&
		str = str.replace(/</g, "&lt;");	//<
		str = str.replace(/>/g, "&gt;");	//>
		str = str.replace(/"/g, "&quot;");	//"
		return str;
	}

	////////////////////////////////////////////////////////////////
	// HTMLエスケープ or blank
	this.htmlEscapeOrBlank = function(str){
		
		if(str == null || str === ''){
			str = "&nbsp;";
			return str;
		}
		str = str.replace("&", "&amp;");
		str = str.replace("<", "&lt;");
		str = str.replace(">", "&gt;");
		str = str.replace('"', "&quot;");
		return str;
	}


	////////////////////////////////////////////////////////////////
	// テキスト取得
	this.getErrorText = function(str){
		if(str == null || str === ''){
			return "";
		}
		return '<div class="error">' + str + '</div>';
	}

	////////////////////////////////////////////////////////////////
	// HTMLエスケープ
	this.nl2br = function(str){
		// 改行コードを/nに統一
		str = str.replace(/\r\n/g, "\n");
		str = str.replace(/\r/g, "");
		// BRに変換
		str = str.replace(/\n/g, "<br/>");
		return str;
	}

	////////////////////////////////////////////////////////////////
	// 改行変換
	this.nl2br = function(str){
		// 改行コードを/nに統一
		str = str.replace(/\r\n/g, "\n");
		str = str.replace(/\r/g, "");
		// BRに変換
		str = str.replace(/\n/g, "<br/>");
		return str;
	}


	this.getEvalString = function(_json){

		var first = true;
		var str = "{";
		if(_json != null){
			for(key in _json){
				if(!first){
					str += ','
				}
				key =  key.replace(/"/g, '\\"');
				key =  key.replace(/\n/g, '\\n"');
				body = _json[key].replace(/"/g, '\\"');
				body = body.replace(/\r\n/g, "\n");
				body = body.replace(/\r/g, "");
				body = body.replace(/\n/g, '\\n');
				str += '\'' + key + '\':\'' + body + '\'';
				first = false;
			}
		}
		str += "}";
		return str;
	}

    this.getScrollPos = function() {
        return (document.documentElement.scrollTop || document.body.scrollTop);   
    }

	this.showBox = function(id){
		if($('#' + id).css('display') == 'none'){
			$('.guide_detail').css('display', 'none');
			$('#' + id).css('display', '');
		}
		else{
			$('#' + id).css('display', 'none');
		}
	}

	// 数値チェック
	this.isInt = function(val){
		var str='' + val;
		if (str.match(/[^0-9]/g)){
			return false;
		}
		return true;
	}


	// 数値チェック
	this.isNumber = function(val){
		var str='' + val;
		if (str.match(/[^0-9\.]/g)){
			return false;
		}
		return true;
	}

	// 数値からpx表記へ変換
	this.num2px = function(val){
		return '' + Math.round(val) + 'px';
	}

	// px表記から数値へ変換
	this.px2num = function(val){
		if(val){
			if(app.isNumber(val.replace('px', ''))){
				// 小数点が帰ってきた場合に、四捨五入する（IE9で発生）
				var ret = parseFloat(val.replace('px', ''));
				ret = Math.round(ret);
				return ret;
			}
			else{
				return 0;
			}
		}
		else{
			return 0;
		}
	}

	// 親ノードの取得（再起）
	this.getParentNode = function(target, tagName){

		var obj = target;
		var ret = null;
		while(obj.parentNode){
			if(obj.parentNode.tagName == tagName){
				ret = obj.parentNode;
				break;
			}
			else{
				obj = obj.parentNode;
			}
		}
		return ret;
	}

	// オブジェクトの表示
	this.showObject = function(id){
		var obj = document.getElementById(id)
		if(obj != null){
			obj.style.display='block';
		}
	}

	// オブジェクトの非表示
	this.hideObject = function(id){
		var obj = document.getElementById(id)
		if(obj != null){
			obj.style.display='none';
		}
	}

	// Select-Optionの選択
	this.selectOption=function(id, value){
		var select = false;
		var obj = document.getElementById(id)
		var selectedIndex = null;
		if(obj != null && value != null){
			for(var i=0; i<obj.options.length; i++){
				if(obj.options[i].value == "" + value){
					selectedIndex = i;
					break;
				}
			}
			if(selectedIndex != null){
				select = true;
				obj.selectedIndex = selectedIndex;
			}
		}
		if(select == false){
			obj.selectedIndex = 0;
		}
		delete obj;
	}

	this.selectOptionWithShow=function(id, value){
		var select = false;
		var obj = document.getElementById(id)
		var selectedIndex = null;
		if(obj != null && value != null){
			for(var i=0; i<obj.options.length; i++){
				if(obj.options[i].value == "" + value){
					selectedIndex = i;
					break;
				}
			}
			if(selectedIndex != null){
				select = true;
				obj.selectedIndex = selectedIndex;
			}
		}
		if(select == false){
			obj.selectedIndex = 0;
		}
		obj.style.display='block';
		delete obj;
	}

	// 選択済みの値を取得
	this.getSelectedValue=function(id){
		var obj = document.getElementById(id);
		var ret = null;
		if(obj != null){
			ret = obj.options[obj.selectedIndex].value;
		}
		delete obj;
		return ret;
	}

	// Select-Optionの内容を入れ替え
	this.resetSelectOptions=function(id, removeIndex, options){
		var obj = document.getElementById(id);
		for (var i = obj.options.length - removeIndex; removeIndex <= i; --i){
			obj.removeChild(obj.options[i]);
		}
		for(var i=0; i<options.length; i++){
			var opt = document.createElement("option");
			opt.value = options[i]['value'];
			opt.text = options[i]['text'];
			obj.add(opt);
		}
		obj.options[0].selected = true;
		delete obj;
	}


	// テキストフィールドに値の設定
	this.setTextValue=function(id, value){
		var obj = document.getElementById(id)
		if(obj != null){
			obj.value = this.htmlEscape("" + value);
		}
		delete obj;
	}
	this.setTextValueWidthShow=function(id, value){
		var obj = document.getElementById(id)
		if(obj != null){
			obj.value = this.htmlEscape("" + value);
		}
		obj.style.display='block';
		delete obj;
	}


	// ダブルクォートでくくった文字列を返す
	// 両サイドに"を付加する
	// 文字中の"を""にする
	this.getDoubleQuoteString=function(str){
		if(str == null){
			return str;
		}
		str = str.replace(/"/g, '""');  	// "
		return '"' + str + '"';
	}


	// ダブルクォートでくくった文字列からダブルクォートを除去する
	// 両サイドに"がある場合のみ、処理する
	// 文字中の"を""にする
	this.removeDoubleQuoteString=function(str){
		if(str == null){
			return str;
		}
		if(str.length > 1 && str[0] == '"' && str[str.length - 1] == '"'){
			var tmp = str.substring(1, str.length - 1);
			return tmp.replace(/""/g, '"');
		}
		else{
			return str;
		}
	}

	// 少数○桁の四捨五入
	this.round=function(num, decPoint){
		var _tmp = 1;
		for(var i=0; i<decPoint -1; i++){
			_tmp = _tmp * 10;
		}
		return (Math.round(num * _tmp)) / _tmp;
	}
	
	////////////////////////////////////////////////////////////////
	// 数値に三桁毎のカンマを挿入する
	this.formatComma = function(data){
		var ret = "";
		var d = "";
		var lc;
		var lc2;
		
		if(data == ''){
			return '';
		}

		if(this.isSignedNumber(data)){
			d = this.formatNumber(data);

			for(lc=d.length - 1, lc2=1; lc >= 0; lc--, lc2++){
				ret = "" + d.charAt(lc) + ret;
				if(lc2 % 3 == 0){
					if((lc == 1 && d.charAt(lc-1) == '-')){
						// 先頭がマイナスの場合はカンマの挿入を行わない
					}
					else if(lc  == 0){
						// 先頭の数字の場合は、カンマの挿入を行わない
					}
					else{
						ret = "," + ret;
					}
				}
			}
		}

		if(ret == ""){
			ret = data;
		}
		return ret;
	}

	////////////////////////////////////////////////////////////////
	// 消費税計算を行う
	this.calcTax=function(data, toObj){
		var d="";
		if(data == ""){
			toObj.value="";
			return;
		}
		if(this.isSignedNumber(data)){
			d = this.formatNumber(data);
			var val = parseInt(d);
			val = Math.round(val * 1.05);
			val = this.formatComma("" + val);
			toObj.value=val;
		}
		else{
			toObj.value="";
		}
	}

	////////////////////////////////////////////////////////////////
	// 整数チェックを行う（少数点数可能／マイナス可能）
	this.isSignedNumber=function(data){

		var ret = true;
		var firstNum = true;

		// 指定されていない場合も正常終了
		if(data == ""){
			return true;
		}

		for(lc=0; lc < data.length; lc++){
			// 先頭の文字の場合
			if(lc == 0){
				if(
					data.charAt(lc) != "-" &&
					data.charAt(lc) != '0' &&
					data.charAt(lc) != '1' &&
					data.charAt(lc) != '2' &&
					data.charAt(lc) != '3' &&
					data.charAt(lc) != '4' &&
					data.charAt(lc) != '5' &&
					data.charAt(lc) != '6' &&
					data.charAt(lc) != '7' &&
					data.charAt(lc) != '8' &&
					data.charAt(lc) != '9'){
					ret = false;
				}
				if(data.charAt(lc) == '-'){
					firstNum = false;
				}
			}
			// 二番目以降の文字の場合
			else{
				// マイナスの直後に、カンマがくいる場合はエラー
				if(!firstNum && lc == 1 && data.charAt(lc) == ','){
					ret = false;
				}
				else if(
					data.charAt(lc) != "," &&
					data.charAt(lc) != '0' &&
					data.charAt(lc) != '1' &&
					data.charAt(lc) != '2' &&
					data.charAt(lc) != '3' &&
					data.charAt(lc) != '4' &&
					data.charAt(lc) != '5' &&
					data.charAt(lc) != '6' &&
					data.charAt(lc) != '7' &&
					data.charAt(lc) != '8' &&
					data.charAt(lc) != '9'){
					ret = false;
				}
			}
		}
		return ret;
	}

	////////////////////////////////////////////////////////////////
	// 数値の成形（カンマを取り除く）
	this.formatNumber=function(data){
		var ret = "";
		var flg = false;

		if(data == ''){
			return '';
		}

		if(data.match(/^0+$/)){
			return '0';
		}

		for(lc=0; lc < data.length; lc++){

			if(data.charAt(lc) != ','){

				if(data.charAt(lc) == '0'){
					if(flg){
						ret = "" + ret + data.charAt(lc);
					}
				}
				else{
					ret = "" + ret + data.charAt(lc);
					if(data.charAt(lc) != '-'){
						flg = true;
					}
				}
			}
		}
		return ret;
	}
	
	this.getLayoutObject = function(key){
		var jq = $(key);
		var ret = {
			borderLeft:app.px2num(jq.css("border-left-width")),
			borderRigtht:app.px2num(jq.css("border-right-width")),
			borderTop:app.px2num(jq.css("border-top-width")),
			borderBottom:app.px2num(jq.css("border-bottom-width")),
			marginLeft:app.px2num(jq.css("margin-left")),
			marginRigtht:app.px2num(jq.css("margin-right")),
			marginTop:app.px2num(jq.css("margin-top")),
			marginBottom:app.px2num(jq.css("margin-bottom")),

			paddingLeft:app.px2num(jq.css("padding-left")),
			paddingRigtht:app.px2num(jq.css("padding-right")),
			paddingTop:app.px2num(jq.css("padding-top")),
			paddingBottom:app.px2num(jq.css("padding-bottom")),

			hFrameSize:app.px2num(jq.css("border-top-width")) + app.px2num(jq.css("border-bottom-width")) + app.px2num(jq.css("margin-top")) + app.px2num(jq.css("margin-bottom")),
			wFrameSize:app.px2num(jq.css("border-left-width")) + app.px2num(jq.css("border-right-width")) + app.px2num(jq.css("margin-left")) + app.px2num(jq.css("margin-bottom")),
			totalHeight:jq.height() + app.px2num(jq.css("border-top-width")) + app.px2num(jq.css("border-bottom-width")) + app.px2num(jq.css("margin-top")) + app.px2num(jq.css("margin-bottom")),
			totalWidth:jq.width() + app.px2num(jq.css("border-left-width")) + app.px2num(jq.css("border-right-width")) + app.px2num(jq.css("margin-left")) + app.px2num(jq.css("margin-right")),
			jquery:jq
		};
		
		return ret;
	
	}

	////////////////////////////////////////////////
	// 親オブジェクトを取得する。
	// 指定されたクラス名のオブジェクトが見つかるまで
	// 再帰的に参照する
	this.getParentJQueryObjectByClassName=function(jqObj, className){

		if(jqObj.hasClass(className)){
			return jqObj;
		}
		var parentObj = jqObj.parent();
		if(parentObj.length == 0){
			return null;
		}
		else{
			return app.getParentJQueryObjectByClassName(parentObj, className);
		}
	}

	////////////////////////////////////////////////
	// 親オブジェクトを取得する。
	// 指定されたIDのオブジェクトが見つかるまで
	// 再帰的に参照する
	this.getParentJQueryObjectById=function(jqObj, id){

		if(jqObj.attr('id') == id){
			return jqObj;
		}
		var parentObj = jqObj.parent();
		if(parentObj.length == 0){
			return null;
		}
		else{
			return app.getParentJQueryObjectById(parentObj, id);
		}
	}


	////////////////////////////////////////////////
	// 親オブジェクトを取得する。
	// 指定されたタグ名が見つかるまで
	// 再帰的に参照する
	this.getParentJQueryObjectByTagName=function(jqObj, tagName){
		if(jqObj[0].nodeName.toUpperCase() == tagName.toUpperCase()){
			return jqObj;
		}
		var parentObj = jqObj.parent();
		if(parentObj.length == 0){
			return null;
		}
		else{
			return app.getParentJQueryObjectByTagName(parentObj, tagName);
		}
	}

	////////////////////////////////////////////////
	// フォームデータをJSONデータに変換する
	// input type=fileについては、別途fileupload機能を利用すること
	// id：フォームのIDを指定する
	// message: メッセージ要素取得の有無
	this.formParam2jsonData=function(id, message){
		var formObj = $('#' + id);
		return this._formParam2jsonData(formObj, message);
	}
	////////////////////////////////////////////////
	// フォームオブジェクト指定
	this._formParam2jsonData=function(formObj, message){
		var ret = {};
		if(formObj.length == 0){
			alert('Please specify a form object.length=0.');
			return;
		}

		if(formObj[0].nodeName.toUpperCase() == 'FORM'){
			// オブジェクト階層をすべてシークして、入力フォームデータを取得する
			var results = {};
			this.getChildInputData(formObj, results, message);
			ret = results;
/*
			for(var i in results){ 
				var ret = i + "|";
				if($.type(results[i]) == 'array'){
					for(var j=0; j<results[i].length; j++){
						ret += results[i][j] + ','
					}
				}
				else{
					ret += results[i];
				}
				app.debug(ret);
			}
*/
		}
		else{
			alert('Please specify a form object.');
		}
		return results;
	}
	this.getChildInputData=function(jqObj, results, message){
		var _self=this;
		var name = null;
		var value = [];
		if(jqObj[0].nodeName.toUpperCase() == 'INPUT'){
			if(jqObj.attr('type').toUpperCase() == 'TEXT' || jqObj.attr('type').toUpperCase() == 'HIDDEN' || jqObj.attr('type').toUpperCase() == 'PASSWORD'){
				name = jqObj.attr('name');
				value.push(jqObj.val());
			}
			else if(jqObj.attr('type').toUpperCase() == 'CHECKBOX' || jqObj.attr('type').toUpperCase() == 'RADIO'){

				if(jqObj.attr('checked')){
					name = jqObj.attr('name');
					value.push(jqObj.val());
				}
			}
		}
		else if(jqObj[0].nodeName.toUpperCase() == 'TEXTAREA'){
				name = jqObj.attr('name');
				value.push(jqObj.val());
		}
		else if(jqObj[0].nodeName.toUpperCase() == 'SELECT'){
			name = jqObj.attr('name');
			var options = jqObj.children('option');
			for(var i=0; i<options.length; i++){
				if(options[i].selected){
					value.push(options[i].value);
				}
			}
		}
		else if(message && jqObj[0].nodeName.toUpperCase() == 'DIV' && jqObj.hasClass('message')){
			name = jqObj.attr('title');
			value.push(jqObj.html());
		}
		else{
			var children = jqObj.children();
			for(var i=0; i<children.length; i++){
				this.getChildInputData($(children[i]), results, message);
			}
		}

		// 値のセット
		// 重複名があった場合は、配列化する
		if(name != null && value != null && value.length > 0){
			if(results[name] != null){
				if($.type(results[name]) == 'array'){
					for(var j=0; j < value.length; j++){
						results[name].push(value[j]);
					}
				}
				else{
					var bk = results[name];
					results[name]=[];
					results[name].push(bk);
					for(var j=0; j < value.length; j++){
						results[name].push(value[j]);
					}
				}
			}
			else{
				if(value.length == 1){
					results[name] = value;
				}
				else{
					results[name]=[];
					for(var k=0; k < value.length; k++){
						results[name].push(value[k]);
					}
				}
			}
		}
	}

	////////////////////////////////////////////////
	// JSONデータをフォームデータに変換する
	// input type=fileについては、別途fileupload機能を利用すること
	// jsonData：JSONデータ
	// id：フォームのIDを指定する
	this.jsonData2formParam=function(jsonData, id){
		var formObj = $('#' + id);
		this._jsonData2formParam(jsonData, formObj);
	}
	////////////////////////////////////////////////
	// Formオブジェクト指定
	this._jsonData2formParam=function(jsonData, formObj){

		if(formObj.length == 0){
			alert('Please specify a form object.length=0.');
			return;
		}


		if(formObj[0].nodeName.toUpperCase() == 'FORM'){

			this.setChildInputData(formObj, jsonData);

/*
			for(var i in jsonData){ 
				if($.type(jsonData[i]) == 'array'){
					for(var j=0; j<jsonData[i].length; j++){
						this.setChildInputData(formObj, i, jsonData[i][j]);
					}
				}
				else{
					this.setChildInputData(formObj, i, jsonData[i]);
				}
			}
*/

		}
		else{
			alert('Please specify a form object.');
		}
	}
	this.setChildInputData=function(jqObj, jsonData){

		var getValue=function(jsonData, name){
			if(jsonData[name] != null){
				if($.type(jsonData[name]) == 'array' && jsonData[name].length > 0){
					return jsonData[name][0];
				}
				else{
					return jsonData[name];
				}
			}
			return '';
		}

		var isSetValue=function(jsonData, name, value){
			if(jsonData[name] != null){
				if($.type(jsonData[name]) == 'array'){
					for(var j=0; j<jsonData[name].length; j++){
						if(jsonData[name][j] == value){
							return true;
						}
					}
				}
				else{
					if(jsonData[name] == value){
						return true;
					}
				}
			}
			return false;
		}

		if(jqObj[0].nodeName.toUpperCase() == 'INPUT'){

			if(jqObj.attr('type').toUpperCase() == 'TEXT' || jqObj.attr('type').toUpperCase() == 'HIDDEN' || jqObj.attr('type').toUpperCase() == 'PASSWORD'){
				jqObj.val(getValue(jsonData, jqObj.attr('name')));
			}
			else if(jqObj.attr('type').toUpperCase() == 'CHECKBOX' || jqObj.attr('type').toUpperCase() == 'RADIO'){
				if(isSetValue(jsonData, jqObj.attr('name'), jqObj.val())){
					jqObj.attr({'checked':true});
				}
				else{
					jqObj.attr({'checked':false});
				}

			}
		}
		else if(jqObj[0].nodeName.toUpperCase() == 'SELECT'){
			var options = jqObj.children('option');
			for(var i=0; i<options.length; i++){
				if(isSetValue(jsonData, jqObj.attr('name'), options[i].value)){
					options[i].selected = true;
				}
				else{
					options[i].selected = false;
				}
			}
		}
		else if(jqObj[0].nodeName.toUpperCase() == 'TEXTAREA' ){
			jqObj.val(getValue(jsonData, jqObj.attr('name')));
		}
		else if(jqObj[0].nodeName.toUpperCase() == 'DIV' && jqObj.hasClass('message')){
			var data = getValue(jsonData, jqObj.attr('title'));
			if(jqObj.hasClass('escape_off')){
				jqObj.html(data);
			}
			else{
				jqObj.html(_self.nl2br(_self.htmlEscape(data)));
			}
		}
		else{
			var children = jqObj.children();
			for(var i=0; i<children.length; i++){
				this.setChildInputData($(children[i]), jsonData);
			}
		}
	}
	
	//  2013年04月11日（火）の形式で日付リストを返す()
	this.getDateJPYMDWList=function(year, month, keyFormat, labelFormat){
		
		ret = [];
		var week = new Array("日", "月", "火", "水", "木", "金", "土");
		day = 1
		if(year == null || month == null){
			var date = new Date();
			year = date.getFullYear()
			month = date.getMonth() + 1
		}
		while(true){
			date = new Date(year, month - 1, day);
			if(date.getMonth() + 1 == month){
				var key = sprintf(keyFormat, date.getFullYear(), date.getMonth() + 1, date.getDate());
				var label = sprintf(labelFormat, date.getFullYear(), date.getMonth() + 1, date.getDate(), week[date.getDay()]);
				ret.push({key:key, label:label})
				day++;
			}
			else{
				break;
			}
		}
		return ret;		
	}
	
	// 様々な日付文字列をパースして、Date型にして返す
	this.parseDateString=function(str){
		
		var ret = null;
		var formats = [
											new RegExp('^([0-9]{4})([0-1]{1}[0-9]{1})([0-3]{1}[0-9]{1})$'),
											new RegExp('^([0-9]{4})/([0-1]{0,1}[0-9]{1})/([0-3]{0,1}[0-9]{1})$'),
											new RegExp('^([0-9]{4})-([0-1]{0,1}[0-9]{1})-([0-3]{0,1}[0-9]{1})$'),
											new RegExp('^([0-9]{4})年([0-1]{0,1}[0-9]{1})月([0-3]{0,1}[0-9]{1})日$')
									];
		
		for(var i=0; i<formats.length; i++){
			if(str.match(formats[i])){
				var year = RegExp.$1;
				var month = RegExp.$2;
				var day = RegExp.$3;
				ret = new Date(parseInt(year), parseInt(month) - 1, parseInt(day));
				break
			}
		}
		return ret;
	}

	// 様々な時間文字列をパースして、Date型にして返す（日付は、2000年1月1日）
	this.parseTimeString=function(str){
		
		var ret = null;
		var formats = [
											new RegExp('^([0-2]{1}[0-9]{1})([0-5]{1}[0-9]{1})$'),
											new RegExp('^([0-2]{0,1}[0-9]{1}):([0-5]{0,1}[0-9]{1})$'),
											new RegExp('^([0-2]{0,1}[0-9]{1})-([0-5]{0,1}[0-9]{1})$'),
											new RegExp('^([0-2]{0,1}[0-9]{1})時([0-5]{0,1}[0-9]{1})分$')
									];
		
		for(var i=0; i<formats.length; i++){
			if(str.match(formats[i])){
				var hour = RegExp.$1;
				var min = RegExp.$2;
				ret = new Date(2000, 0, 1, hour, min);
				break
			}
		}
		return ret;
	}

	// 様々な月日文字列をパースして、Date型にして返す（日付は１日）
	this.parseMonthlyString=function(str){
		
		var ret = null;
		var formats = [
											new RegExp('^([0-9]{4})([0-1]{1}[0-9]{1})$'),
											new RegExp('^([0-9]{4})/([0-1]{0,1}[0-9]{1})$'),
											new RegExp('^([0-9]{4})-([0-1]{0,1}[0-9]{1})$'),
											new RegExp('^([0-9]{4})年([0-1]{0,1}[0-9]{1})月$')
									];
		
		for(var i=0; i<formats.length; i++){
			if(str.match(formats[i])){
				var year = RegExp.$1;
				var month = RegExp.$2;
				var day = RegExp.$3;
				ret = new Date(parseInt(year), parseInt(month) - 1, 1);
				break
			}
		}
		return ret;
	}

}


// 文字列型のtrim関数を実装（前後の空白削除）
String.prototype.trim =function(){
	var str = this.replace(/^\s+|\s+$/g, "");
	str = str.replace(/^　+|　+$/g, "");
	return str;
}

// 文字列型のtrimAll関数を実装（空白全削除）
String.prototype.trimAll =function(){
	var str = this.replace(/\s/g, "");
	str = str.replace(/　/g, "");
	return str;
}

// 文字列型のtrimLf関数を実装（改行全削除）
String.prototype.trimLf =function(){
	var str = this.replace(/\n/g, "");
	str = str.replace(/\r/g, "　");
	return str;
}
