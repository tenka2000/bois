var hoursPicker=new function(){
	
	var _self=this;
	this.target=null;
	this.showTarget=null;
	this.top=null;
	this.left=null;
	this.pos=null;
	this.frame=null;

	this.top=null;
	this.body=null;
	this.bottom=null;

	this.hour=null;
	this.min=null;
	this.baseYear=null;
	this.baseMonth=null;
	this.imagePath="../img/hours.picker/";
	this.dayBoxIdPrefix="ioy6u1f2i_";
	this.selectedHour=null;
	this.selectedMin=null;
	
	this.clickHour=null;
	this.clickMin=null;
	this.scroll=0;
			
	$(function(){
		_self.pos = $('<div class="hours_picker_pos" style=""><div>').prependTo('body');
		_self.frame = $('<div class="time_frame"></div>').appendTo(_self.pos);


		_self.top = $('<div class="time_top"></div>').appendTo(_self.frame);
		_self.body = $('<div class="time_body"></div>').appendTo(_self.frame);
		_self.bottom = $('<div class="time_bottom"></div>').appendTo(_self.frame);
		_self.hour = $('<div class="time_hours"></div>').appendTo(_self.body);

		rainy3.bindDocumentMousedown("tst1pnfdsq", function(e){
			var parent = app.getParentJQueryObjectByClassName($(e.target), 'hours_picker_pos');
			if(parent == null){
				_self.hide();
			}
		});
	});

	/**
	 * obj・・・対象のオブジェクト
	 * showTarget・・・追加先
	 * top・・・top
	 * left・・・left
	 * pos・・・追加位置（before, after(null)）
	 */
	this.show=function(obj,showTarget, top, left, pos){
		var _self=this;
		_self.frame.hide();
		/* 初期化 */
		_self.top=null;
		_self.left=null;
		_self.frame.css('top', "0px");
		_self.frame.css('left', "0px");
		_self.selectedHour=null;
		_self.selectedMin=null;
		_self.clickHour=null;
		_self.clickMin=null;
		_self.scroll=0;
	
		_self.showTarget=null;
		var year=null;
		var month=null;
		_self.target=$(obj);
		if(_self.target.val() != null && _self.target.val() != ""){
			_self.selectedHour=_self.target.val() ;
		}
		if(showTarget == null){
			_self.showTarget=_self.target;
		}
		else{
			_self.showTarget=$(showTarget);
		}
		_self.top=top;
		_self.left=left;
		_self._show(year, month);
		if(pos != null && pos == 'before'){
			_self.showTarget.before(_self.pos);
		}
		else{
			_self.showTarget.after(_self.pos);
		}
		if(_self.top != null){
			_self.frame.css('top', _self.top + "px");
		}
		if(_self.left != null){
			_self.frame.css('left', _self.left + "px");
		}
		_self.target.keydown(function(e){
			if(e.keyCode == KEY.TAB){
				_self.hide();
			}
		})
		_self.frame.fadeIn('fast', function(){
																	_self.hour.scrollTop(_self.scroll - 30);
																});
	}

	this._show=function(){
		var _self=this;

		_self.hour.empty();

		var hourList = [];
		for(var i=0; i<24; i++){
			hourList.push('' + i + '.00');
			hourList.push('' + i + '.25');
			hourList.push('' + i + '.50');
			hourList.push('' + i + '.75');
		}
		var hourPrefix = "h1a2s3aw3ufsdha_";

		var html = '';
		for(var i=0; i<hourList.length; i++){
			
			var id=hourPrefix + i;
			html += '<a href="javascript:void(0);" onClick="hoursPicker.setHour(' + "'" +  hourList[i] + "'" + ');"><div id="' + id + '" class="';
			
			if(_self.selectedHour != null && _self.selectedHour == hourList[i]){
				html += 'box box_selected';
				_self.scroll = parseInt((i+4)/4) * 22 - 22;
			}
			else{
				html += 'box box_sel';
			}
			html += '">' + hourList[i] + '</div></a>';
		}
		_self.hour.append(html);
	}
	
	this.hide=function(obj){
		var _self=this;
		if(_self.target!=null){
			_self.target.unbind('keydown');
		}
		_self.frame.fadeOut('fast');
		_self.target=null;
	}
	
	this.setHour=function(hour){
		var _self=this;
		_self.target.val(hour);
		_self.hide();
	}
}
