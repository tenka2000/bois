var orderPicker=new function(){
	
	var _self=this;
	this.target=null;
	this.showTarget=null;
	this.top=null;
	this.left=null;
	this.pos=null;
	this.frame=null;

	this.top=null;
	this.body=null;
	this.bottom=null;

	this.order=null;
	this.imagePath="../img/order.picker/";
	this.dayBoxIdPrefix="aoy8u2f9a_";
	this.selectedOrder=null;
	
	this.scroll=0;
	this.matterId=null;
	this.url=null;
			
	$(function(){
		_self.pos = $('<div class="order_picker_pos" style=""><div>').prependTo('body');
		_self.frame = $('<div class="order_frame"></div>').appendTo(_self.pos);


		_self.top = $('<div class="order_top"></div>').appendTo(_self.frame);
		_self.body = $('<div class="order_body"></div>').appendTo(_self.frame);
		_self.bottom = $('<div class="order_bottom"></div>').appendTo(_self.frame);

		_self.order = $('<div class="order_label"><a href="javascript:void(0);" onclick="orderPicker.setOrderLastOne();"><div class="box box_sel">末尾に1加算</div></a><a href="javascript:void(0);" onclick="orderPicker.setOrderLastDigit();"><div class="box box_sel">末尾に桁加算</div></a></div>').appendTo(_self.body);

		_self.order = $('<div class="order_order"></div>').appendTo(_self.body);

		rainy3.bindDocumentMousedown("t1e0anfds3", function(e){
			var parent = app.getParentJQueryObjectByClassName($(e.target), 'order_picker_pos');
			if(parent == null){
				_self.hide();
			}
		});
	});

	/**
	 * matterId・・・案件ID
	 * obj・・・対象のオブジェクト
	 * showTarget・・・追加先
	 * top・・・top
	 * left・・・left
	 * pos・・・追加位置（before, after(null)）
	 */
	this.show=function(matterId, obj,showTarget, top, left, pos){
		var _self=this;
		_self.frame.hide();
		/* 初期化 */
		_self.top=null;
		_self.left=null;
		_self.frame.css('top', "0px");
		_self.frame.css('left', "0px");
		_self.selectedOrder=null;
		_self.scroll=0;
	
		_self.matterId=matterId;
		_self.showTarget=null;
		var year=null;
		var month=null;
		_self.target=$(obj);
		if(_self.target.val() != null && _self.target.val() != ""){
			_self.selectedOrder=_self.target.val() ;
		}
		if(showTarget == null){
			_self.showTarget=_self.target;
		}
		else{
			_self.showTarget=$(showTarget);
		}
		_self.top=top;
		_self.left=left;
		_self._show(year, month);
		if(pos != null && pos == 'before'){
			_self.showTarget.before(_self.pos);
		}
		else{
			_self.showTarget.after(_self.pos);
		}
		if(_self.top != null){
			_self.frame.css('top', _self.top + "px");
		}
		if(_self.left != null){
			_self.frame.css('left', _self.left + "px");
		}
		_self.target.keydown(function(e){
			if(e.keyCode == KEY.TAB){
				_self.hide();
			}
		})
		_self.frame.fadeIn('fast', function(){
																	_self.order.scrollTop(_self.scroll - 30);
																});
	}

	this._show=function(){
		var _self=this;

		_self.order.empty();

		var orderList = [];
		for(var i=1; i<=1000; i++){
			if(i%10==0){
				orderList.push(i);
			}
		}
		var hourPrefix = "h1a2s3aw3ufsdha_";

		var html = '';
		for(var i=0; i<orderList.length; i++){
			
			var id=hourPrefix + i;
			html += '<a href="javascript:void(0);" onClick="orderPicker.setOrder(' + "'" +  orderList[i] + "'" + ');"><div id="' + id + '" class="';
			
			if(_self.selectedOrder != null && _self.selectedOrder == orderList[i]){
				html += 'box box_selected';
				_self.scroll = parseInt((i+5)/5) * 22 - 22;
			}
			else{
				html += 'box box_sel';
			}
			html += '">' + orderList[i] + '</div></a>';
		}
		_self.order.append(html);
	}
	
	this.hide=function(obj){
		var _self=this;
		if(_self.target!=null){
			_self.target.unbind('keydown');
		}
		_self.frame.fadeOut('fast');
		_self.target=null;
	}
	
	this.setOrder=function(order){
		var _self=this;
		_self.target.val(order);
		_self.hide();
	}
	
	// 末尾の順番に１追加
	this.setOrderLastOne=function(){
		var _self=this;
		if(_self.matterId == null || _self.matterId == ''){
			_self.target.val(1);
			_self.hide();
		}
		else{
			var dataMap={};
			$.ajax({
				url: _self.url + '?matter_id=' + _self.matterId,
				type: 'POST',
				data: dataMap,
				async: false,
				cache: true,
				dataType:"html",
				error: function(XMLHttpRequest, textStatus, errorThrown){
					alert('データの取得に失敗しました。再度実行してください。');
					_self.hide();
				},
				success: function(html){
					// OK
					var lastDispalyOrder=0;
					eval(html);
					_self.target.val(lastDispalyOrder+1);
					_self.hide();
				}
			});
		}
	}

	// 末尾の順番に１桁追加
	this.setOrderLastDigit=function(){
		
		var _self=this;
		if(_self.matterId == null || _self.matterId == ''){
			_self.target.val(10);
			_self.hide();
		}
		else{
			var dataMap={};
			$.ajax({
				url: _self.url + '?matter_id=' + _self.matterId,
				type: 'POST',
				data: dataMap,
				async: false,
				cache: true,
				dataType:"html",
				error: function(XMLHttpRequest, textStatus, errorThrown){
					alert('データの取得に失敗しました。再度実行してください。');
					_self.hide();
				},
				success: function(html){
					// OK
					var lastDispalyOrder=0;
					eval(html);
					if(lastDispalyOrder == 0){
						_self.target.val(10);
					}
					else{
						_self.target.val((lastDispalyOrder - lastDispalyOrder % 10) + 10);
						_self.hide();
					}
				}
			});
		}
	}
	
	this.setLastOrderUrl=function(url){
		this.url=url;
	}


}
