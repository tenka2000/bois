var monthlyPicker=new function(){
	
	var _self=this;
	this.target=null;
	this.showTarget=null;
	this.top=null;
	this.left=null;
	this.pos=null;
	this.frame=null;

	this.top=null;
	this.body=null;
	this.bottom=null;

	this.navi=null;
	this.calendar=null;
	this.baseYear=null;
	this.baseMonth=null;
	this.imagePath="../img/monthly.picker/";
	this.dayBoxIdPrefix="acdgudf9e_";
	this.selectedYear=null;
	this.selectedMonth=null;
			
	$(function(){
		_self.pos = $('<div class="monthly_picker_pos" style=""><div>').prependTo('body');
		_self.frame = $('<div class="calendar_frame"></div>').appendTo(_self.pos);

		_self.top = $('<div class="calendar_top"></div>').appendTo(_self.frame);
		_self.body = $('<div class="calendar_body"></div>').appendTo(_self.frame);
		_self.bottom = $('<div class="calendar_bottom"></div>').appendTo(_self.frame);
		
		var html="";
		html += '<div class="calendar_navi">';
		html += '<div class="l_arrow" onclick="monthlyPicker.prevYear();">';
		html += '<img src="' + _self.imagePath + 'l_arrow.png" onmouseover="this.src=' + "'" + _self.imagePath + 'l_arrow_hover.png' + "'" + '" onmouseout="this.src=' + "'" + _self.imagePath + 'l_arrow.png' + "'" + '">';
		html += '</div>';
		html += '<div class="label"></div>';
		html += '<div class="r_arrow" onclick="monthlyPicker.nextYear();">';
		html += '<img src="' + _self.imagePath + 'r_arrow.png" onmouseover="this.src=' + "'" + _self.imagePath + 'r_arrow_hover.png' + "'" + '" onmouseout="this.src=' + "'" + _self.imagePath + 'r_arrow.png' + "'" + '">';
		html += '</div>';
		html += '</div>';

		_self.navi = $(html).appendTo(_self.body);
		_self.calendar = $('<div class="calendar_days"></div>').appendTo(_self.body);

		rainy3.bindDocumentMousedown("arasnfdsa", function(e){
			var parent = app.getParentJQueryObjectByClassName($(e.target), 'monthly_picker_pos');
			if(parent == null){
				_self.hide();
			}
		});
	});

	this.show=function(obj,showTarget, top, left){
		var _self=this;
		_self.frame.hide();
		/* 初期化 */
		_self.top=null;
		_self.left=null;
		_self.frame.css('top', "0px");
		_self.frame.css('left', "0px");
		_self.selectedYear=null;
		_self.selectedMonth=null;

		_self.showTarget=null;
		var year=null;
		var month=null;
		
		_self.target=$(obj);
		
		if(_self.target.val() != null && _self.target.val() != ""){
			var date = app.parseMonthlyString(_self.target.val());
			if(date != null){
				var _year = date.getFullYear();
				var _month = date.getMonth()+1;
				var _day = date.getDate();

				year = _year;

				_self.selectedYear=_year;
				_self.selectedMonth=_month;
			}
		}
		if(showTarget == null){
			_self.showTarget=_self.target;
		}
		else{
			_self.showTarget=$(showTarget);
		}
		_self.top=top;
		_self.left=left;
		_self._show(year);
		_self.showTarget.after(_self.pos);
		if(_self.top != null){
			_self.frame.css('top', _self.top + "px");
		}
		if(_self.left != null){
			_self.frame.css('left', _self.left + "px");
		}
		_self.target.keydown(function(e){
			if(e.keyCode == KEY.TAB){
				_self.hide();
			}
		})
		_self.frame.fadeIn('fast');
	}

	this._show=function(year){
		var _self=this;
		var now = new Date();
		var nowYear = now.getFullYear();
		var nowMonth = now.getMonth()+1;
		var nowDay = now.getDate();

		_self.calendar.empty();
		var label=_self.navi.children('.label');
		label.empty();

		if(year != null){
			_self.baseYear=year;
		}
		else{
			_self.baseYear=nowYear;
		}
		var calList = [1,2,3,4,5,6,7,8,9,10,11,12];

		var html = '';
		html += _self.baseYear;
		html += '年';
		label.append(html);

		var html = '';

		for(var i=0; i<calList.length; i++){

			html += '<a href="javascript:void(0);" onclick="monthlyPicker.set(' + _self.baseYear + ',' + calList[i] + ');return false;">';
			html += '<div class="';
			if(_self.selectedYear != null && _self.selectedYear == _self.baseYear && _self.selectedMonth == calList[i]){
				html += 'day_box day_box_selected';
			}
			else if(nowYear == _self.baseYear && nowMonth == calList[i]){
				html += 'day_box day_box_current';
			}
			else{
				html += 'day_box day_box_sel';
			}
			html += '">';
			html += calList[i];
			html += '</div></a>';
		}
		_self.calendar.append(html);
	}
	
	this.hide=function(obj){
		var _self=this;
		if(_self.target!=null){
			_self.target.unbind('keydown');
		}
		_self.frame.fadeOut('fast');
		_self.target=null;
	}
	
	this.set=function(year, month){
		var _self=this;
		var val = year + "/" + ("0"+ month).slice(-2);
		_self.target.val(val);
		_self.hide();
	}

	this.nextYear=function(){
		this._show(this.baseYear + 1);
	}

	this.prevYear=function(){
		this._show(this.baseYear - 1);
	}
}
