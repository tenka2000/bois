var datePicker=new function(){
	
	var _self=this;
	this.target=null;
	this.showTarget=null;
	this.top=null;
	this.left=null;
	this.pos=null;
	this.frame=null;

	this.top=null;
	this.body=null;
	this.bottom=null;

	this.navi=null;
	this.calendar=null;
	this.baseYear=null;
	this.baseMonth=null;
	this.imagePath="../img/date.picker/";
	this.dayBoxIdPrefix="fhasudf8e_";
	this.selectedYear=null;
	this.selectedMonth=null;
	this.selectedDay=null;
			
	$(function(){
		_self.pos = $('<div class="date_picker_pos" style=""><div>').prependTo('body');
		_self.frame = $('<div class="calendar_frame"></div>').appendTo(_self.pos);

		_self.top = $('<div class="calendar_top"></div>').appendTo(_self.frame);
		_self.body = $('<div class="calendar_body"></div>').appendTo(_self.frame);
		_self.bottom = $('<div class="calendar_bottom"></div>').appendTo(_self.frame);

		var html="";
		html += '<div class="calendar_navi">';
		html += '<div class="l_arrow" onclick="datePicker.prevMonth();">';
		html += '<img src="' + _self.imagePath + 'l_arrow.png" onmouseover="this.src=' + "'" + _self.imagePath + 'l_arrow_hover.png' + "'" + '" onmouseout="this.src=' + "'" + _self.imagePath + 'l_arrow.png' + "'" + '">';
		html += '</div>';
		html += '<div class="label"></div>';
		html += '<div class="r_arrow" onclick="datePicker.nextMonth();">';
		html += '<img src="' + _self.imagePath + 'r_arrow.png" onmouseover="this.src=' + "'" + _self.imagePath + 'r_arrow_hover.png' + "'" + '" onmouseout="this.src=' + "'" + _self.imagePath + 'r_arrow.png' + "'" + '">';
		html += '</div>';
		html += '</div>';

		_self.navi = $(html).appendTo(_self.body);

		_self.calendar = $('<div class="calendar_days"></div>').appendTo(_self.body);

		rainy3.bindDocumentMousedown("heraknfdsa", function(e){
			var parent = app.getParentJQueryObjectByClassName($(e.target), 'date_picker_pos');
			if(parent == null){
				_self.hide();
			}
		});
	});

	this.show=function(obj,showTarget, top, left){
		var _self=this;
		_self.frame.hide();
		/* 初期化 */
		_self.top=null;
		_self.left=null;
		_self.frame.css('top', "0px");
		_self.frame.css('left', "0px");
		_self.selectedYear=null;
		_self.selectedMonth=null;
		_self.selectedDay=null;

		_self.showTarget=null;
		var year=null;
		var month=null;
		
		_self.target=$(obj);
		
		if(_self.target.val() != null && _self.target.val() != ""){
			var date = app.parseDateString(_self.target.val());
			if(date != null){
				var _year = date.getFullYear();
				var _month = date.getMonth()+1;
				var _day = date.getDate();

				year = _year;
				month = _month;

				_self.selectedYear=_year;
				_self.selectedMonth=_month;
				_self.selectedDay=_day;
			}
		}
		if(showTarget == null){
			_self.showTarget=_self.target;
		}
		else{
			_self.showTarget=$(showTarget);
		}
		_self.top=top;
		_self.left=left;
		_self._show(year, month);
		_self.showTarget.after(_self.pos);
		if(_self.top != null){
			_self.frame.css('top', _self.top + "px");
		}
		if(_self.left != null){
			_self.frame.css('left', _self.left + "px");
		}
		_self.target.keydown(function(e){
			if(e.keyCode == KEY.TAB){
				_self.hide();
			}
		})
		_self.frame.fadeIn('fast');
	}

	this._show=function(year, month){
		var _self=this;
		var now = new Date();
		var nowYear = now.getFullYear();
		var nowMonth = now.getMonth()+1;
		var nowDay = now.getDate();

		_self.calendar.empty();
		var label=_self.navi.children('.label');
		label.empty();

		var calList = this.createDayList(year, month);

						
		var html = '';
		html += _self.baseYear;
		html += '年';
		html += _self.baseMonth;
		html += '月';
		label.append(html);

		var html = '';
		html += '<div class="week_label sunday">日</div>';
		html += '<div class="week_label">月</div>';
		html += '<div class="week_label">火</div>';
		html += '<div class="week_label">水</div>';
		html += '<div class="week_label">木</div>';
		html += '<div class="week_label">金</div>';
		html += '<div class="week_label saturday">土</div>';


		for(var i=0; i<calList.length; i++){
			if(calList[i]['year'] != null){
				
				var id = _self.dayBoxIdPrefix + calList[i]['year'] + ("0"+ calList[i]['month']).slice(-2) + ("0"+ calList[i]['day']).slice(-2);
				html += '<a href="javascript:void(0);" onclick="datePicker.set(' + calList[i]['year'] + ',' + calList[i]['month'] + ',' + calList[i]['day'] + ');return false;">';
				html += '<div id="' + id + '" class="';
				
				if(_self.selectedYear != null && _self.selectedYear == calList[i]['year'] && _self.selectedMonth == calList[i]['month'] && _self.selectedDay == calList[i]['day']){
					html += 'day_box day_box_selected';
				}
				else if(nowYear == calList[i]['year'] && nowMonth == calList[i]['month'] && nowDay == calList[i]['day']){
					html += 'day_box day_box_current';
				}
				else{
					html += 'day_box day_box_sel';
				}
				html += '">';
				html += calList[i]['day'];
				html += '</div></a>';
			}
			else{
				html += '<div class="day_box">&nbsp;';
				html += '</div>';
			}
		}
		_self.calendar.append(html);
		
	}
	
	this.hide=function(obj){
		var _self=this;
		if(_self.target!=null){
			_self.target.unbind('keydown');
		}
		_self.frame.fadeOut('fast');
		_self.target=null;
	}
	
	this.set=function(year, month, day){
		var _self=this;
		var val = year + "/" + ("0"+ month).slice(-2) + "/" + ("0"+ day).slice(-2);

		_self.target.val(val);
		_self.hide();
	}
	
	this.nextMonth=function(){
		var year=this.baseYear;
		var month=this.baseMonth+1;
		if(this.baseMonth == 12){
			year = this.baseYear+1;
			month = 1;
		}
		this._show(year, month);
	}

	this.prevMonth=function(){
		var year=this.baseYear;
		var month=this.baseMonth-1;
		if(this.baseMonth == 1){
			year = this.baseYear-1;
			month = 12;
		}
		this._show(year, month);
	}

	
	this.createDayList=function(year, month){

		var _self=this;
		var list=[];
		var now = new Date();
		if(year == null){
			year = now.getFullYear();
			month = now.getMonth()+1;
		}
		
		_self.baseYear=year;
		_self.baseMonth=month;
		
		var date = new Date(year, month-1, 1);
		if(date.getDay() != 0){
			var prev_year=year;
			var prev_month=month;
			if(month==1){
				prev_year -= 1;
				prev_month=12;
			}
			else{
				prev_month-=1;
			}
			
			var count = this.getDateCount(prev_year, prev_month);
			for(var i=count-date.getDay() + 1; i<=count; i++){
				list.push({year:null, month:null, day:null});
			}
		}
		var count = this.getDateCount(year, month);
		for(var i=1; i<= count; i++){
			list.push({year:year, month:month, day:i});
		}
		
		if(list.length%7 != 0){
			for(var i=list.length%7+1; i<= 7; i++){
				list.push({year:null, month:null, day:null});
			}
		}
		return list;
	}
	this.getDateCount=function(year, month){
		var date_count=[31,28,31,30,31,30,31,31,30,31,30,31]
		if((year % 4 == 0 && year % 100 != 0) || year % 400 == 0){
			date_count[1]=29;
		}
		return date_count[month-1];
	}
}
