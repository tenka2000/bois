var userMenu = new function(){

	this.logoutUrl="";
	this.accountUrl="";
	this.passwordUrl="";

	this.form="";
	this.form+="<a href='javascript:void(0);' onclick='userMenu.password();' class='user_menu_link'><div >パスワード変更</div></a>";
	this.form+="<a href='javascript:void(0);' onclick='userMenu.logout();' class='user_menu_link'><div >ログアウト</div></a>";
	this.userControl = new Rainy3Submenu(120, 50, this.form);
	this.userControl.clickTargetId = "setting_control";

	this.click = function(){
		var obj = $('#setting_control');
		var left = obj.get(0).offsetLeft - 60;
		var height = obj.height() + 2;
		this.userControl.setPosition('height', left, height, left, height);
		this.userControl.show();
	}	

	this.account=function(){
		location.href=this.accountUrl;
	}

	this.password=function(){
		location.href=this.passwordUrl;
	}
	
	this.logout = function(){
		
		var _self=this;
		
		var dataMap ={};
				// ログインリクエスト
		$.ajax({
			url: _self.logoutUrl,
			type: 'POST',
			data: dataMap,
			async: false,
			cache: true,
			dataType:"html",
			error: function(XMLHttpRequest, textStatus, errorThrown){
				alert("通信に失敗しました。管理者にお問い合わせください。");
			},
			success: function(html){
				var nextPath = "";
				eval(html)
				location.href=nextPath;
			}
		});
	}
}
