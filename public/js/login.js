var login=new function(){
	
	this.loginUrl="";
	
	var _self = this;
	$(function(){

		_self.resize();

		$(window).resize(_self.resize);

		$('input[name=login_id]').focus();

		
	})
	
	this.resize=function(){
		var height = app.getWindowHeight();
		var loginBox = $('div.login_box');
		var header = $('#header');
		var marginTop = (height / 2 - loginBox.height() / 2) - header.height();
		
		if(marginTop < 45){
			marginTop = 45;
		}
		
		loginBox.css("margin-top", "" + marginTop + "px");
	}
	
	this.doLogin=function(form){
		var _self = this;
		var messageArea = $('div.message');
		var loginId = form.login_id.value;
		var passwd = form.passwd.value;
		var autoLogin = form.auto_login.checked;
		var path = form.path.value;
		var header = $('#header');
		var footer = $('#footer');
		var contents = $('#contents');
		var loginBox = $('div.login_box');
		
		messageArea.empty();
		
		if(loginId == null || loginId == ''){
			messageArea.html("LOGIN IDを入力してください。");
			form.login_id.focus();
			return false;
		}
		if(passwd == null || passwd == ''){
			messageArea.html("PASSWORDを入力してください。");
			form.passwd.focus();
			return false;
		}

		dataMap = {
			login_id:loginId,
			passwd:passwd,
			auto_login:autoLogin,
			path:path
		}
		
		// ログインリクエスト
		$.ajax({
			url: _self.loginUrl,
			type: 'POST',
			data: dataMap,
			async: false,
			cache: true,
			dataType:"html",
			error: function(XMLHttpRequest, textStatus, errorThrown){
				messageArea.html("通信に失敗しました。管理者にお問い合わせください。");
			},
			success: function(html){
				var ret = false;
				var message = "";
				var nextPath = "";
				eval(html)
				if(ret){

//					contentsHeight = contents.height() + (header.height() -37) + (footer.height() -6)

					var marginTop = app.px2num(contents.css("margin-top")) + (footer.height() -6);
					var marginBottom = app.px2num(contents.css("margin-bottom")) + (header.height() -37);

//					contents.animate({height:"" + contentsHeight + "px"}, 600, null, function(){location.href=nextPath})
					contents.animate({"margin-top": "" + marginTop + "px", "margin-bottom" : "" + marginBottom + "px"}, 600, null, function(){})

					header.animate({height:"37px"}, 600, null, function(){location.href=nextPath});
					footer.animate({height:"6px"}, 600, null, function(){});
					loginBox.fadeOut(400);

				}
				else{
					messageArea.html(message);
					form.passwd.focus();
					return false;
				}
			}
		});
	}
}
