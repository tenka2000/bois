var ratePicker=new function(){
	
	var _self=this;
	this.target=null;
	this.showTarget=null;
	this.top=null;
	this.left=null;
	this.pos=null;
	this.frame=null;

	this.top=null;
	this.body=null;
	this.bottom=null;

	this.rate=null;
	this.imagePath="../img/tate.picker/";
	this.dayBoxIdPrefix="soy6u231o_";
	this.selectedRate=null;
	
	this.scroll=0;
			
	$(function(){
		_self.pos = $('<div class="rate_picker_pos" style=""><div>').prependTo('body');
		_self.frame = $('<div class="rate_frame"></div>').appendTo(_self.pos);


		_self.top = $('<div class="rate_top"></div>').appendTo(_self.frame);
		_self.body = $('<div class="rate_body"></div>').appendTo(_self.frame);
		_self.bottom = $('<div class="rate_bottom"></div>').appendTo(_self.frame);
		_self.rate = $('<div class="rate_rate"></div>').appendTo(_self.body);

		rainy3.bindDocumentMousedown("t4t2d1fdsq", function(e){
			var parent = app.getParentJQueryObjectByClassName($(e.target), 'rate_picker_pos');
			if(parent == null){
				_self.hide();
			}
		});
	});

	/**
	 * obj・・・対象のオブジェクト
	 * showTarget・・・追加先
	 * top・・・top
	 * left・・・left
	 * pos・・・追加位置（before, after(null)）
	 */
	this.show=function(obj,showTarget, top, left, pos){
		var _self=this;
		_self.frame.hide();
		/* 初期化 */
		_self.top=null;
		_self.left=null;
		_self.frame.css('top', "0px");
		_self.frame.css('left', "0px");
		_self.selectedRate=null;
		_self.scroll=0;
	
		_self.showTarget=null;
		var year=null;
		var month=null;
		_self.target=$(obj);
		if(_self.target.val() != null && _self.target.val() != ""){
			_self.selectedRate=_self.target.val() ;
		}
		if(showTarget == null){
			_self.showTarget=_self.target;
		}
		else{
			_self.showTarget=$(showTarget);
		}
		_self.top=top;
		_self.left=left;
		_self._show(year, month);
		if(pos != null && pos == 'before'){
			_self.showTarget.before(_self.pos);
		}
		else{
			_self.showTarget.after(_self.pos);
		}
		if(_self.top != null){
			_self.frame.css('top', _self.top + "px");
		}
		if(_self.left != null){
			_self.frame.css('left', _self.left + "px");
		}
		_self.target.keydown(function(e){
			if(e.keyCode == KEY.TAB){
				_self.hide();
			}
		})
		_self.frame.fadeIn('fast', function(){
																	_self.rate.scrollTop(_self.scroll - 30);
																});
	}

	this._show=function(){
		var _self=this;

		_self.rate.empty();

		var hourList = [];
		for(var i=0; i<10; i++){
			hourList.push(i * 10);
			hourList.push(i * 10 + 5);
		}
		hourList.push(100);
		var hourPrefix = "h1a2s3aw3ufsdha_";

		var html = '';
		for(var i=0; i<hourList.length; i++){
			
			var id=hourPrefix + i;
			html += '<a href="javascript:void(0);" onClick="ratePicker.setHour(' + "'" +  hourList[i] + "'" + ');"><div id="' + id + '" class="';
			
			if(_self.selectedRate != null && _self.selectedRate == hourList[i]){
				html += 'box box_selected';
				_self.scroll = parseInt((i+2)/2) * 22 - 22;
			}
			else{
				html += 'box box_sel';
			}
			html += '">' + hourList[i] + '</div></a>';
		}
		_self.rate.append(html);
	}
	
	this.hide=function(obj){
		var _self=this;
		if(_self.target!=null){
			_self.target.unbind('keydown');
		}
		_self.frame.fadeOut('fast');
		_self.target=null;
	}
	
	this.setHour=function(hour){
		var _self=this;
		_self.target.val(hour);
		_self.hide();
	}
}
