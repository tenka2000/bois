# encoding: utf-8
Bois::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # The test environment is used exclusively to run your application's
  # test suite. You never need to work with it otherwise. Remember that
  # your test database is "scratch space" for the test suite and is wiped
  # and recreated between test runs. Don't rely on the data there!
  config.cache_classes = true

  # Configure static asset server for tests with Cache-Control for performance
  config.serve_static_assets = true
  config.static_cache_control = "public, max-age=3600"

  # Log error messages when you accidentally call methods on nil
  config.whiny_nils = true

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Raise exceptions instead of rendering exception templates
  config.action_dispatch.show_exceptions = false

  # Disable request forgery protection in test environment
  config.action_controller.allow_forgery_protection    = false

  # Tell Action Mailer not to deliver emails to the real world.
  # The :test delivery method accumulates sent emails in the
  # ActionMailer::Base.deliveries array.
  config.action_mailer.delivery_method = :test

  # Raise exception on mass assignment protection for Active Record models
  config.active_record.mass_assignment_sanitizer = :strict

  # Print deprecation notices to the stderr
  config.active_support.deprecation = :stderr
  
  config.action_mailer.raise_delivery_errors = true

  ################################################
  # Local Setting
  config.logger = Logger.new("log/production.log")
	config.logger.formatter = Logger::Formatter.new
	config.logger.datetime_format = "%Y-%m-%d %H:%M:%S"
	# メール送信エラーの通知
  config.action_mailer.raise_delivery_errors = true
 
	# サイトURL
	config.system_site_url='http://localhost:3000'
	# サイト管理者TEL
	config.system_site_admin_tel='026-1111-2222'
	# サイト管理者EMAIL
	config.system_site_admin_email='fujisawa@smartsw.co.jp'
	# システム通知メールFromアドレス
  config.system_mailer_from_address='fujisawa@smartsw.co.jp'
	# システム通知メールFrom名
  config.system_mailer_from_name='Boisシステム管理'
  # 初期ログインID
  config.init_login_id='admin'
  # 処理パスワード
  config.init_password='p@ssw0rd'
  # メール送信先設定
	config.action_mailer.default_url_options = { :host => "localhost", :port => 3000 }
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    :address => "smtp.alpha-prm.jp",
    :domain => 'smtp.alpha-prm.jp',
    :port => 587,
    :user_name => "fujisawa%smartsw.co.jp",
    :password => "tenka2000",
    :authentication => 'plain',
    :enable_starttls_auto => true,
  }
  # メール送信先の変更設定（Off）
  config.debug_send_to=nil


end
