# encoding: utf-8
Bois::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Log error messages when you accidentally call methods on nil.
  config.whiny_nils = true

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = true

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin

  # Raise exception on mass assignment protection for Active Record models
  config.active_record.mass_assignment_sanitizer = :strict

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL)
  config.active_record.auto_explain_threshold_in_seconds = 0.5

  # Do not compress assets
  config.assets.compress = false

  # Expands the lines which load the assets
  config.assets.debug = true
  
  ################################################
  # Local Setting
  config.logger = Logger.new("log/development.log")
	config.logger.formatter = Logger::Formatter.new
	config.logger.datetime_format = "%Y-%m-%d %H:%M:%S"
	# メール送信エラーの通知
  config.action_mailer.raise_delivery_errors = true

	# サイトURL
	config.system_site_url='http://localhost:3000'
	# サイト管理者TEL
	config.system_site_admin_tel='026-1111-2222'
	# サイト管理者EMAIL
	config.system_site_admin_email='fujisawa@smartsw.co.jp'
	# システム通知メールFromアドレス
  config.system_mailer_from_address='fujisawa@smartsw.co.jp'
	# システム通知メールFrom名
  config.system_mailer_from_name='Bois.システム管理'

  # 初期ログインID
  config.init_login_id='admin'
  # 処理パスワード
  config.init_password='password'
  # メール送信先設定
	config.action_mailer.default_url_options = { :host => "localhost", :port => 3000 }
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    :address => "smtp.alpha-prm.jp",
    :domain => 'smtp.alpha-prm.jp',
    :port => 587,
    :user_name => "fujisawa%smartsw.co.jp",
    :password => "tenka2000",
    :authentication => 'plain',
    :enable_starttls_auto => true,
  }
  # メール送信先の変更設定（デバッグ時に不正なメールが届かないようにするため）
  config.debug_send_to='fujisawa@smartsw.onmicrosoft.com'

end
