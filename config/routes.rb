Bois::Application.routes.draw do
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
  
	match '' => 'top#index', :via => [:get], :as => :top_index
	match 'top' => 'top#index', :via => [:get]
	match 't:id' => 'top#topic', :via => [:get], :as => :top_topic

	match 'request' => 'request#index', :via => [:get,:post], :as => :request_index

	match 'power' => 'power#index', :via => [:get], :as => :power_index
	match 'power/list' => 'power#list', :via => [:post], :as => :power_list
	match 'power/graph' => 'power#graph', :via => [:get,:post], :as => :power_graph

	match 'children' => 'children#index', :via => [:get], :as => :children_index
	match 'children/list' => 'children#list', :via => [:post], :as => :children_list
	match 'children/form' => 'children#form', :via => [:post], :as => :children_form
	match 'children/create' => 'children#create', :via => [:post], :as => :children_create
	match 'children/edit' => 'children#edit', :via => [:post], :as => :children_edit
	match 'children/delete' => 'children#delete', :via => [:post], :as => :children_delete
	match 'children/passwd' => 'children#passwd', :via => [:post], :as => :children_passwd

end
